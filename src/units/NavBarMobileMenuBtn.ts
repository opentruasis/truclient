import {IconButton, CloseIcon, HamburgerIcon} from 'luix';
 

export default class NavBarMobileMenuBtn extends IconButton{
    constructor(props: any = null){
        super(props);
        this.addClass("selectable");
        this.style = {  overflow: 'hidden', borderWidth: "0px", borderRadius: "0px"}; 
        if (props != null) {
            this.onClick = props.onClick;
        }else{
            this.onClick = () =>{}
        }
        this.innerText = "";
 
        this.hamburgerIcon.visible = true;
        this.closeIcon.visible = false;


        this.children.push(this.hamburgerIcon);
        this.children.push(this.closeIcon);
 
    }
 
    public toggle: boolean = true;

    private hamburgerIcon = new HamburgerIcon({width: 50, height: 50});
    private closeIcon = new CloseIcon({width: 50, height: 50});


    public changed(props: any = null){
        if(this.toggle){
        this.hamburgerIcon.visible = true;
        this.closeIcon.visible = false;
    }else{
        this.hamburgerIcon.visible = false;
        this.closeIcon.visible = true;
    }
    super.changed(props);
    }
}