import {LUIX, Select, SelectOption} from 'luix';
 
import LocalSuperNodeURL from '../api/LocalSuperNodeURL';
 
export default class SuperSelect extends Select {

    constructor(props: any = null){
        super(props);
        this.style= {height: "35px"};
    }

  
    public onSelectedChanged():void{
        LUIX.post({'selectedSuper': this.Selected, 'selectedSuperIndex': this.SelectedIndex});
    }

    private fillOptions():void {
       var superUrls = LocalSuperNodeURL.GetUrls(); //LUIX.getStateAt('superNodeUrls'); 
       if(superUrls){
           if(superUrls.length != this.OptionList.length){
                let options  = [];
        
                for(var url of superUrls){
                    var newOption = new SelectOption();
                    newOption.innerText = url;
                    newOption.value = url;
                    options.push(newOption);
                }
            
                this.OptionList = options;
           }
        }
    }

    public changed(props: any){
        this.fillOptions(); 
        var isConnected = LUIX.getStateAt('isConnected');
        if(isConnected){
            this.disabled = true;
        }else{
            this.disabled = false;
        }
        var selectedIndexRuntime = LUIX.getStateAt('selectedSuperIndex')

        if(selectedIndexRuntime > -1){
            if(this.SelectedIndex != selectedIndexRuntime){
                setTimeout(()=>{
                  this.SelectedIndex = selectedIndexRuntime;
                }, 500);
            }
        }
         
        super.changed(props);
    }
 
  
}