import {Button, LUIX} from 'luix';
import StyledButton from './StyledButton';

export default class CreateTokenBtn extends StyledButton{
    constructor(props: any = null){
        super(props);
        LUIX.postData('showCreateTokenBtn', true);
    }

    public changed(props: any){
        
        var showCreateTokenBtn = LUIX.getStateAt('showCreateTokenBtn');
        
        showCreateTokenBtn? this.visible = true : this.visible = false;

        super.changed(props);
    }
}