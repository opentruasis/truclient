import TruWalletAPI from "../api/TruWalletAPI";
import { Entry, FlexBox, IconButton, LUIX}  from "luix";
import TruWallet from '../types/ITruWallet';
import CloseIconBtn from "./CloseIconBtn";
 
export default class  AppSearchBar extends FlexBox{
    constructor(props: any = null){
        super(props);
        const flexContainer = new FlexBox();
        flexContainer.style = {flexDirection: 'column', width: "100%", overflow:'auto' };
       

        const searchBoxContainer = new FlexBox();
        searchBoxContainer.style = {justifyContent: 'center', alignItems: 'center'};

        this.AppSearchEntry.EntryLabel.Text = "";
        this.AppSearchEntry.EntryInput.Elem.setAttribute("placeholder", "App Search");
        this.AppSearchEntry.EntryInput.style={maxWidth: "125px"};
        this.AppSearchEntry.style={maxWidth: "150px"};
        this.AppSearchEntry.EntryInput.onChange = this.searchTextChanged.bind(this);

        searchBoxContainer.push(this.AppSearchEntry);

        const clearSearch = new CloseIconBtn();
 
        clearSearch.onClick = ()=>{
            this.AppSearchEntry.EntryInput.Value = "";
            
            LUIX.postData('appSearchString', "");
            // this.SearchResultsBox.clear();
            // this.listOfWallets.splice(0, this.listOfWallets.length);
            // LUIX.removeData("sendTruTo");
        };
        
        searchBoxContainer.push(clearSearch);
        this.children.push(searchBoxContainer);


    }

    public AppSearchEntry = new Entry();
    
 
    private async searchTextChanged(e:any){
        var entryText =  this.AppSearchEntry.EntryInput.Value;
      
        //try and get wallets:
        if(entryText.length > 1){
            // let searchWalletResults = await TruWalletAPI.SearchWallet(entryText);
            
            // if(searchWalletResults.length > 0){
               
            //     var listOfWallets: TruWallet[] = [];
            //     searchWalletResults.forEach((w)=>{
            //          listOfWallets.push( w );
            //     });

            //     LUIX.postData('tokenSearchString', listOfWallets);
            //     this.TokenSearchEntry.EntryInput.focus();
            // }
        }
        
    }
 
}