import {Button} from 'luix';

export default class ChartViewBtn extends Button{
    constructor(props: any = null){
        super(props);
        this.addClass("selectable");
        this.addClass("navButton");
        this.style = {fontSize: "15px", fontWeight: 600, height: "40px", flexGrow:1 };
        if (props != null) {
            this.onClick = props.onClick;
        }
    }

    public render(): HTMLElement {
        return super.render();
    } 
}