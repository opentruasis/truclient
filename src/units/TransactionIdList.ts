﻿ 
import {Div, ElemList, LUIX} from 'luix';
 


interface ITransaction {
    ID: string;
}

export default class TransactionIdList extends ElemList {

    constructor(props: any = null) {
        super(props);
        this.style = {display: 'flex', flexWrap: 'wrap'};
    }

    public blockId: number = -1;



    public changed(props: any) {
 
        this.clear();
       
        //Transactions.
        try {
            let transactionData = props as ITransaction[];
 
            let txToAdd  = [];
            for (let txdata of transactionData) {
     
                let txIn: boolean = false;

                for (var txd in this.children) {
           
                    let childLabel = this.children[txd];
       
                    if (txdata.ID == txd) {
                        txIn = true;
                        break;
                    }
                }
                if (!txIn) {
  
                    var tx = new Div();
                    tx.onClick = ()=>{
                       LUIX.GoTo("/chain?transaction="+txdata.ID +"&block=" + this.blockId);
                       //LUIX.GoTo("/transaction");
                    };
                    tx.innerText = txdata.ID.substring(0,6);
                    tx.addClass('selectable');
                    tx.style={flex: 'left', margin: "2px", padding:"2px", border: '1px black solid'};
                    txToAdd.push(tx);
                }
            }
 
            for (var newTx of txToAdd) {
                this.push(newTx);
                
            }
 

        } catch (e) {
             
        }

        super.changed(props);
    }

}     
