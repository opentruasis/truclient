import {Button, LUIX} from 'luix';
 
import StyledButton from './StyledButton';

export default class ViewMyTokensBtn extends StyledButton{
    constructor(props: any = null){
        super(props);
        LUIX.postData('showMyTokenBtn', true);
    }

    public changed(props: any){
        var showMyTokenBtn = LUIX.getStateAt('showMyTokenBtn');
        var numTkns = LUIX.getStateAt("myTokenCount") ? LUIX.getStateAt("myTokenCount") : 0;
        if( numTkns < 1){
            showMyTokenBtn = false;
        }
        
        showMyTokenBtn ? this.visible = true : this.visible = false;
        super.changed(props);
    }
}