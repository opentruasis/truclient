
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import {FlexBox, LUIX, Text} from 'luix';
import LocalExchangeAPI from '../api/LocalExchangeAPI';
import TruExchangeAPI from '../api/TruExchangeAPI';
import ITruBuyOrder from '../types/ITruBuyOrder';
 
 
import ITruOrder from '../types/ITruOrder';
import OrderStatus from '../types/OrderStatus';
import OrderType from '../types/OrderType';
import StyledButton from './StyledButton';

export default class ExchangeOrderLine extends FlexBox {

    constructor(props: any = null){
        super(props);

            
        // return;
        this.style = {
            backgroundColor: "#efefef",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            overflowY: "auto" ,
            justifyContent:'center',
 
        };
 
 
    }
    


    public fillData(order: ITruOrder){
   
        // {
        //     "OrderType": 0,
        //     "BuyerTruID": "03c2f4af-2f15-447e-ae36-76e4fe39705d",
        //     "BuyerPubKey": "1Fea2sy2KhuR4ky8ZOC+PxhclO0ajGjuv8Ookibpw9egkW/+NhyVM7CmPRkm1tDlyOb+XEiCl59TtwPyPONdxRtraVKqcIZztRYaFHopKcxEiO63AtXaK+TH9elrngxc5Yu8QBaqkyAUG0bR/W5xGFjqsB5BhJhWBJGrgoShcVEBAAE=",
        //     "PaypalBuyURL": "https://www.sandbox.paypal.com/checkoutnow?token=1PA248339D0926410",
        //     "PaypalTx": "1PA248339D0926410",
        //     "PaypalBuyerEmail": null,
        //     "ID": 15,
        //     "CreatedOn": "2022-01-05T19:26:34.8784941+00:00",
        //     "UpdatedOn": "2022-01-05T19:26:34.8784978+00:00",
        //     "Qty": 100,
        //     "CumQty": 0,
        //     "LeavesQty": 100,
        //     "LimitPrice": 1.05,
        //     "OrderStatus": 1
        // }

        const tradeLineBox: FlexBox = new FlexBox();
        tradeLineBox.style = {display: 'flex', justifyContent: 'space-around', width: '100%', fontSize: "18px", alignItems: 'center'};
        const priceLabel: Text = new Text();
        priceLabel.Text = order.LimitPrice.toFixed(4);
        tradeLineBox.push(priceLabel);
        const qtyLabel: Text = new Text();
        qtyLabel.Text = order.CumQty + "/" + order.Qty;
        tradeLineBox.push(qtyLabel);

        const statusContainer: FlexBox = new FlexBox();
        statusContainer.style = {justifyContent: 'center', alignItems: 'center'};
        tradeLineBox.push(statusContainer);


        const statusLabel: Text = new Text();
        statusLabel.Text = OrderStatus[order.OrderStatus].toString();
     
        const executeButton : StyledButton = new StyledButton();
        executeButton.innerText = "Execute";
        executeButton.onClick = async ()=>{
            if(order.OrderType == OrderType.Buy){
                var uri = (order as ITruBuyOrder).PaypalBuyURL;
                console.log("URI:  " + uri);
                var newwindow = window.open(uri,'name' );
                if (window.focus) {newwindow.focus()}
            }
        };

        const deleteButton : StyledButton = new StyledButton();
        deleteButton.innerText = "Remove";
        deleteButton.onClick = async ()=>{
            console.log("MMMMMMMMMMM");
            console.log(order.OrderStatus);
            if(order.OrderStatus == OrderStatus.Initiated){
                await this.CancelOrder(order);
            }
            LocalExchangeAPI.DeleteOrder(order.ID);
            LUIX.GoTo("/mytruorders"); // reloads local list: works nice.
        };


        const cancelOrderButton: StyledButton = new StyledButton();
        cancelOrderButton.innerText = "Cancel";
        cancelOrderButton.onClick = async ()=>{
           await this.CancelOrder(order);
        };

        statusContainer.push(statusLabel);

        if (order.OrderStatus != OrderStatus.Initiated)
        {
            // exOrderLine.executeButton.Visibility = Visibility.Hidden;
            // exOrderLine.statusLabel.Visibility = Visibility.Visible;
            statusContainer.push(statusLabel);

        }
        else
        {
            // exOrderLine.executeButton.Visibility = Visibility.Visible;
            // exOrderLine.statusLabel.Visibility = Visibility.Hidden;
            statusContainer.push(executeButton);

        }

        if (order.OrderStatus ==  OrderStatus.Paid)
        {
            if (order.OrderType == OrderType.Sell)
            {
                //exOrderLine.cancelOrderButton.Visibility = Visibility.Visible;
                statusContainer.push(cancelOrderButton);
            }
            else
            {
                // exOrderLine.cancelOrderButton.Visibility = Visibility.Hidden;
                // exOrderLine.deleteButton.Visibility = Visibility.Visible;
                
                statusContainer.push(deleteButton);
            }
        }
        

        if (order.OrderStatus ==  OrderStatus.Fullfilled ||
            order.OrderStatus ==  OrderStatus.Canceled ||
            order.OrderStatus ==  OrderStatus.Initiated)
        {
           // exOrderLine.deleteButton.Visibility = Visibility.Visible;
           statusContainer.push(deleteButton);
        }
        
           
 
        this.children.push(tradeLineBox);

      
    }

     
    private async CancelOrder(order: ITruOrder): Promise<void>{
        if(order.OrderType == OrderType.Buy){
            var res =  await TruExchangeAPI.CancelBuyOrder(order.ID, LUIX.getStateAt('wallet').ID);
            console.log("Result from canceling buy order>>>");
            console.log(res);
        }

        if(order.OrderType == OrderType.Sell){
          var res =  await TruExchangeAPI.CancelSellOrder(order.ID, LUIX.getStateAt('wallet').ID);
          console.log("Result from canceling sell order>>>");
          console.log(res);
        }
        LUIX.GoTo("/mytruorders"); // reloads local list: works nice.
    }



    public changed(props: any = null){
         
        super.changed(props);
    }

}
