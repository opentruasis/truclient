import {Canvas} from 'luix';
import ITruPrice from '../types/ITruPrice';


interface IPlotPoint{
    X:number,
    Y:number
}
 
 
export default class PriceChart extends Canvas {
 
    constructor(props: any = null){
        super(props);

        const c = this.Elem;
 
        this.ctx = (c as HTMLCanvasElement).getContext("2d");

        this.backDate = this.GetBackDate();
        
        
        this.Elem.addEventListener('mouseleave', this.TruPriceChart_MouseLeave.bind(this));
        this.Elem.addEventListener('mousemove', this.TruPriceChart_MouseMove.bind(this));

        this.Elem.addEventListener('touchstart', this.TruPriceChart_TouchMove.bind(this));
        this.Elem.addEventListener('touchmove', this.TruPriceChart_TouchMove.bind(this));
        this.Elem.addEventListener('touchend', this.TruPriceChart_TouchEnd.bind(this));
 
    }

    

    private width: number = 0;
    private height: number = 0;
    private minTruPriceInRange: ITruPrice;
    private maxTruPriceInRange: ITruPrice;
 
    private minPoint: any;
    private maxPoint: any;

    private cursorPos: number;

    private snapToPrice: number;
    private snapToX: number = 0;
    private rangeMin: number;
    private rangeMax: number;

    private timeTrimmedPrices: ITruPrice[] = [];
    private optimizedPrices: ITruPrice[] = [];

    private chartView: number = 5;
    private isTouching: boolean = false;

    private optimizedOnce: boolean 
    private TruPrices: ITruPrice[] = [];

    private backDate: Date;
        
    private ctx : CanvasRenderingContext2D = null; 

 
    private  TruPriceChart_TouchEnd(e: Event)
    {
        this.isTouching = false;
        this.draw();
    }

    private  TruPriceChart_TouchMove(e: Event)
    {
        this.isTouching  = true;
        var rect = this.Elem.getBoundingClientRect();
        var x = (e as TouchEvent).touches[0].clientX - rect.left;
        // var y = (e as MouseEvent).y - rect.top;  //y position within the element.
        if(x < 0 || x > this.width){
            this.isTouching = false;
        }
        this.cursorPos =  x;
        this.draw();
    }


    private  TruPriceChart_MouseLeave(e: Event)
    {
        this.isTouching = false;
        this.draw();
    }

    private  TruPriceChart_MouseMove(e: Event)
    {
        this.isTouching  = true;
        var rect = this.Elem.getBoundingClientRect();
        var x = (e as MouseEvent).x - rect.left; //x position within the element.
        this.cursorPos =  x;
        this.draw();
    }

    private draw(){
 
        this.Elem.setAttribute("width", this.width.toString());
        this.Elem.setAttribute("height", this.height.toString());
    
        this.drawMinMaxLabels();
        this.drawPlot();

        if (this.isTouching)
        {
            this.drawSelectedDatePrice();
        }
    }


    private drawSelectedDatePrice(){
 
        var point0: IPlotPoint = {X: this.snapToX, Y: 0};
        var point1: IPlotPoint = {X: this.snapToX, Y: this.height};
        
        this.ctx.beginPath();
        this.ctx.strokeStyle = "rgba(0,0,0,0.5)";
        this.ctx.lineWidth = 5;
        this.ctx.moveTo(point0.X, point0.Y);
        this.ctx.lineTo(point1.X, point1.Y);
         this.ctx.stroke();

        var priceTxt = "$" +  this.snapToPrice.toFixed(4);

        var xPosForHoverLabel = this.snapToX + 5;
        if (xPosForHoverLabel > this.width - 100)
        {
            xPosForHoverLabel = this.snapToX - 105;
        }
  
        this.ctx.fillStyle = "rgba(0,0,0,0.5)";
        this.ctx.fillRect(xPosForHoverLabel, this.height/2 - 5, 100, 30);   
        this.ctx.fillStyle = "#fff";
        this.ctx.font = "20px Arial";
        this.ctx.fillText(priceTxt, xPosForHoverLabel + 15, this.height / 2 + 18);
 
    }

    private drawMinMaxLabels(){
        this.ctx.font = "20px Arial";
        if (this.minTruPriceInRange != null)
        {
            var minPoint = this.getPlotPoint(this.minTruPriceInRange);
            if (minPoint.X >= this.width - 90)
            {
                minPoint.X = this.width - 90;
            }
            this.ctx.fillText("$" + this.minTruPriceInRange.Price.toFixed(4), minPoint.X, this.height - 5);
        }
        else
        {
            this.ctx.fillText("$" + this.minTruPriceInRange.Price.toFixed(4),  8.0, this.height - 5);
        }

        if (this.maxTruPriceInRange != null)
        {
            var maxPoint = this.getPlotPoint(this.maxTruPriceInRange);

            if (maxPoint.X >= this.width - 90)
            {
                maxPoint.X = this.width - 90;
            }
 
            this.ctx.fillText("$" + this.maxTruPriceInRange.Price.toFixed(4),  maxPoint.X, 20);
        }
        else
        {
            this.ctx.fillText("$" + this.maxTruPriceInRange.Price.toFixed(4),  8, 20);
        }


    }


    private getPlotPoint(p: ITruPrice):IPlotPoint{
 
          var mYdiv = this.rangeMin - this.rangeMax;
  
          if(Math.abs(mYdiv) < 0.0000000000001){
            if(mYdiv < 0){
                mYdiv = -0.0000000000001;
              }else{
                mYdiv = 0.0000000000001;
              }
          }
 
          let mY = (this.height - 50) / mYdiv;
          let  bY = -mY * this.rangeMax + 25;
 
          //X Scaled..
          var backDateNum = Number(this.backDate);
          var offset = new Date().getTimezoneOffset();
          var nowDate = new Date();
          nowDate.setMinutes(nowDate.getMinutes() + offset);
          var nowNum = Number(nowDate);
 

         var bX = (this.width) / (1 - nowNum / backDateNum);
 
         var mX = -bX / backDateNum;
     
         var nextX =  mX * Number(p.TimeStamp) + bX;

         var nextY =  mY * p.Price + bY;
         
           
          
         return  {X: nextX, Y: nextY};

    }


    
    private drawPlot(){
        
        var initPrice =  this.getInitPlotTruPrice();
        
        var startPnt = this.getPlotPoint(initPrice);

        var nextPnt;

        this.ctx.beginPath();
        this.ctx.lineWidth = 2;
        this.ctx.moveTo(startPnt.X, startPnt.Y);
 
        for(var i=0; i<this.timeTrimmedPrices.length; i++)
        {
            var p = this.timeTrimmedPrices[i];
            nextPnt = this.getPlotPoint(p);
            if (this.isTouching)
            {
                if(Math.abs(this.cursorPos - this.snapToX) >= Math.abs(nextPnt.X - this.cursorPos)){
                    this.snapToX = nextPnt.X;
                    this.snapToPrice = p.Price;
                }
            }

            this.ctx.lineTo(nextPnt.X, nextPnt.Y);
        }

        this.ctx.stroke();
    }


    private GetBackDate():Date{

        var backTime: Date  = new Date(-8640000000000000);
 
        var offset = new Date().getTimezoneOffset();
 
        var now = new Date();
        now.setMinutes(now.getMinutes() + offset);
 
        switch (this.chartView)
        {
            case 0:
                if (this.optimizedPrices.length > 0)
                {
                    backTime = this.optimizedPrices[0].TimeStamp;
                } 
                break;
            case 1:
                backTime = new Date(now.setFullYear(now.getFullYear() - 1));
                break;
            case 2:
                backTime = new Date(now.setMonth(now.getMonth() - 1));
                break;
            case 3:
                backTime = new Date(now.setDate(now.getDate() - 7));
                break;
            case 4:
                backTime = new Date(now.setDate(now.getDate() - 1));
                break;
            case 5:
                backTime = new Date(now.setHours(now.getHours() - 1));
                break;
        }
 
        return backTime;

    }


    
    private  getInitPlotTruPrice() :ITruPrice
    {
        var initPrice = 0.0;
        if( this.timeTrimmedPrices.length > 0){
            initPrice = this.timeTrimmedPrices[0].Price;
        } 
    
        return  { Price: initPrice, TimeStamp: this.backDate};
    }


   private calculateRangeMinMax():void {
        
        let minPrice = 999999999999.0;
        let minPriceIndex = 0;

        for (var i = 0; i < this.timeTrimmedPrices.length; i++)
        {
            if (minPrice >= this.timeTrimmedPrices[i].Price)
            {
                minPrice = this.timeTrimmedPrices[i].Price;
                minPriceIndex = i;
            }
        }
        this.rangeMin = minPrice;

        this.minTruPriceInRange = this.timeTrimmedPrices[minPriceIndex];

        let maxPrice = -1;
        let maxPriceIndex = 0;
        for (var i = 0; i < this.timeTrimmedPrices.length; i++)
        {
            if (maxPrice < this.timeTrimmedPrices[i].Price)
            {
                maxPrice = this.timeTrimmedPrices[i].Price;
                maxPriceIndex = i;
            }
        }
        this.rangeMax = maxPrice;

        this.maxTruPriceInRange = this.timeTrimmedPrices[maxPriceIndex];

 
        // var overallRange = this.rangeMax - this.rangeMin;

        // if (overallRange < 0.00001)
        // {
        //     this.rangeMin = 0;
        // }
        // var tenPercent = 0.1 * overallRange;

        // this.rangeMin = this.rangeMin - tenPercent;
        // if (this.rangeMin < 0) this.rangeMin = 0;
 
    }

    private getPricesForView(): ITruPrice[]
    {
 
        var filteredPrices: ITruPrice[] = [];
 
        var firstEntry = true;
       
        for (var i = 0; i < this.optimizedPrices.length; i++)
        {
            var price =  this.optimizedPrices[i];
            var lastPrice: ITruPrice;

       
            if (Number(price.TimeStamp) >= Number(this.backDate))
            {
                if (!firstEntry)
                {
                    if (i > 0)
                    {
                        lastPrice =  this.optimizedPrices[i - 1];
                    }
                    else
                    {
                        lastPrice = price;
                        filteredPrices.push(lastPrice);
                        continue;
                    }
                    filteredPrices.push(lastPrice);
                }
                firstEntry = false;
                filteredPrices.push(price);
            }
        }

 
        return  filteredPrices;
    }


    private  createOptimizedPriceList():void
    {
        var optimizedPrices: ITruPrice[] = [];
  
        for (var k = 0; k < this.TruPrices.length; k++)
        {
            
            if (k == 0)
            {
                optimizedPrices.push(this.TruPrices[k]);
                continue;
            }

            //did direction change ie. key point?

            var directionChange = false;

            if (k + 1 < this.TruPrices.length) //Trying to capture the key point.
            {
                var dirOne = this.TruPrices[k].Price - this.TruPrices[k - 1].Price;
                var dirTwo = this.TruPrices[k + 1].Price - this.TruPrices[k].Price;
                if (dirOne > 0 && dirTwo < 0)
                {
                    directionChange = true;
                }

                if (dirOne < 0 && dirTwo > 0)
                {
                    directionChange = true;
                }
            }
            else
            {
                directionChange = true;
            }

            var denominator = this.TruPrices[k].Price;
            if (denominator < 0.000001)
            {
                denominator = 0.000001;
            }
 
            //use last
            if (((this.TruPrices[k].Price - this.TruPrices[k-1].Price) / denominator) > 0.01 ||  
            // Add to optimized prices if the percent change is greater than one.
                 directionChange  //Direction change.
            )
            {
                optimizedPrices.push(this.TruPrices[k]);
            }

        }

        if (optimizedPrices.length > 0) // add a point to the end for plotting to end of chart..
        {
            var offset = new Date().getTimezoneOffset();
            var nowDate = new Date();
            nowDate.setMinutes(nowDate.getMinutes() + offset);
            optimizedPrices.push({ Price : optimizedPrices[optimizedPrices.length - 1].Price, TimeStamp : nowDate });
        }

        this.optimizedPrices = optimizedPrices;
    }



    public changed(props: any = null){
     
        const {screenWidth = 0, screenHeight = 0, chartView = 0, TruPrices = []} = props ||{};
        this.chartView = chartView;
        this.backDate = this.GetBackDate();
        this.width = screenWidth-20;
        this.height = 0.4 * screenHeight;
 
        if(this.height > this.width * 0.5){
            this.height = this.width * 0.5;
        }

        var OldPriceLength = this.TruPrices.length;

        this.TruPrices = TruPrices;

        if (this.optimizedOnce)
        {
            if (this.TruPrices.length == (OldPriceLength + 1))
            {
                this.optimizedPrices.push(this.TruPrices[this.TruPrices.length - 1]);
            }

            this.createOptimizedPriceList();
            
            this.timeTrimmedPrices = this.getPricesForView();
            if(this.timeTrimmedPrices.length == 1){
                this.timeTrimmedPrices.unshift({Price: this.timeTrimmedPrices[0].Price, TimeStamp: this.backDate});
            }
            if (this.timeTrimmedPrices.length > 0)
            {
                this.calculateRangeMinMax();
            }

        }
        else(this.TruPrices.length > 0 )
        {
            
            this.optimizedOnce = true;
            this.createOptimizedPriceList();

            this.timeTrimmedPrices = this.getPricesForView();
            if(this.timeTrimmedPrices.length == 1){
                this.timeTrimmedPrices.unshift({Price: this.timeTrimmedPrices[0].Price, TimeStamp: this.backDate});
            }
            if (this.timeTrimmedPrices.length > 0)
            {
                this.calculateRangeMinMax();
            }
        }
        
        if(this.timeTrimmedPrices.length > 0){        
            this.draw();
        }

           //super.changed(props);
    }
 
    
}     
