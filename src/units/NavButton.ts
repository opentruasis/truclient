import {Button} from 'luix';

export default class  NavButton extends Button{
    constructor(props: any = null){
        super(props);
        this.addClass("selectable");
        this.addClass("navButton");
        this.style = {fontSize: "25px", height: "50px",  marginBottom: "25px" };
        if (props != null) {
            this.onClick = props.onClick;
        }
    }
}