import {IconButton} from "luix";

export default class SocketConnectionBtn extends IconButton{

    constructor(props:any = null){
        super(props);
        this.style={backgroundColor: "red", borderRadius: "20px", width: "40px", height: "40px"};
        this.innerText = "";
    }


    public changed(props:any = null){        
        const {isConnected = false} = props||{};
        this.style={backgroundColor:  isConnected ? "green" : "red"};
 
        super.changed(props);
    }
  
}