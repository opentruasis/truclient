import { Button, LUIX} from 'luix';
 
import StyledButton from './StyledButton';

export default class CreateAppBtn extends StyledButton{
    constructor(props: any = null){
        super(props);
        LUIX.postData('showCreateAppBtn', true);
    }

    public changed(props: any){
        
        var showCreateAppBtn = LUIX.getStateAt('showCreateAppBtn');
        
        showCreateAppBtn? this.visible = true : this.visible = false;

        super.changed(props);
    }
}