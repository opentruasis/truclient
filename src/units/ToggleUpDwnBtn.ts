import {IconButton, ArrowDown, ArrowUp} from 'luix';
 
export default class ToggleUpDwnBtn extends IconButton{
    constructor(props: any = null){
        super(props);
        this.addClass("selectable");
        this.style = {maxWidth: "40px", maxHeight: "40px", overflow: 'hidden'};
        if (props != null) {
            this.onClick = props.onClick;
        }else{
            this.onClick = () =>{}
        }
        this.innerText = "";
        this.iconDwn = new ArrowDown({width: 50, height: 50});
        this.iconUp = new  ArrowUp({width: 50, height: 50});

        this.iconDwn.style = {position: 'relative', left: "-12.75px", top: "-7px"};
        this.iconUp.style = {position: 'relative', left: "-12.75px", top: "-7px"};
        
        const {initDown=true} = props || {};

        if(initDown){
            this.iconDwn.visible = true;
            this.iconUp.visible = false;
        }else{
            this.iconDwn.visible = false;
            this.iconUp.visible = true;
            this.toggle = true;
        }

        
         this.children.push(this.iconDwn);
        this.children.push(this.iconUp);
        
    }

    public set onClick(event: ()=>void){

        this.Elem.onclick = ()=>{
            if(this.toggle){
                this.iconDwn.visible = true;
                this.iconUp.visible = false;
           }else{
               this.iconDwn.visible = false;
               this.iconUp.visible = true;
           }
           this.toggle = !this.toggle;
            event(); 
        }
      }


    
      public toggle: boolean = false;

      private iconDwn;
      private iconUp;
  

      public changed(props: any = null){

        if(this.toggle){
            this.iconDwn.visible = true;
            this.iconUp.visible = false;
        }else{
            this.iconDwn.visible = false;
            this.iconUp.visible = true;
        }

        super.changed(props);
      }


}