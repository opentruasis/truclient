import {Image} from 'luix';

export default class WalletImage extends Image{
    constructor(props: any = null){
        super(props);
        this.style = { borderRadius: "25px", margin: "10px", minHeight: "50px", maxHeight: "200px", minWidth: "50px", width:"200px", maxWidth: "200px" };
    }

    public render(): HTMLElement {
        return super.render();
    } 
}