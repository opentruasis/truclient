import TruWalletAPI from "../api/TruWalletAPI";
import {Entry, FlexBox, LUIX} from "luix";
 
import TruWallet from '../types/ITruWallet';
import CloseIconBtn from "./CloseIconBtn";
 
export default class  UserSearchBar extends FlexBox{
    constructor(props: any = null){
        super(props);
        const flexContainer = new FlexBox();
        flexContainer.style = {flexDirection: 'column', width: "100%", overflow:'auto' };
       

        const searchBoxContainer = new FlexBox();
        searchBoxContainer.style = {marginBottom: "10px", justifyContent: 'center', alignItems: 'center'};

        this.UserSearchEntry.EntryLabel.Text = "Search Wallets: ";
        this.UserSearchEntry.EntryInput.onChange = this.searchTextChanged.bind(this);

        searchBoxContainer.push(this.UserSearchEntry);

        const clearSearch = new CloseIconBtn();
 
        clearSearch.onClick = ()=>{
            this.UserSearchEntry.EntryInput.Value = "";
            
            LUIX.postData('searchedListOfWallets', []);
            // this.SearchResultsBox.clear();
            // this.listOfWallets.splice(0, this.listOfWallets.length);
            // LUIX.removeData("sendTruTo");
        };
        
        searchBoxContainer.push(clearSearch);
        this.children.push(searchBoxContainer);


    }

    public UserSearchEntry  = new Entry();
    
 
    private async searchTextChanged(e:any){
        var entryText =  this.UserSearchEntry.EntryInput.Value;
      
        //try and get wallets:
        if(entryText.length > 1){
            let searchWalletResults = await TruWalletAPI.SearchWallet(entryText);
            
            if(searchWalletResults.length > 0){
               
                var listOfWallets: TruWallet[] = [];
                searchWalletResults.forEach((w)=>{
                     listOfWallets.push( w );
                });

                LUIX.postData('searchedListOfWallets', listOfWallets);
                this.UserSearchEntry.EntryInput.focus();
            }
        }
        
    }

    public changed(props: any = null){
        
        super.changed(props);
    }
 
}