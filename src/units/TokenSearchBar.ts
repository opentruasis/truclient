import TruWalletAPI from "../api/TruWalletAPI";
import {Entry, FlexBox,LUIX} from "luix";
 
import CloseIconBtn from "./CloseIconBtn";
 
export default class  TokenSearchBar extends FlexBox{
    constructor(props: any = null){
        super(props);
        const flexContainer = new FlexBox();
        flexContainer.style = {flexDirection: 'column', width: "100%", overflow:'auto' };
       

        const searchBoxContainer = new FlexBox();
        searchBoxContainer.style = {justifyContent: 'center', alignItems: 'center'};
        this.TokenSearchEntry.EntryInput.Elem.setAttribute("placeholder", "Search Tokens");
        this.TokenSearchEntry.EntryInput.style={maxWidth: "125px"};
        this.TokenSearchEntry.style={maxWidth: "150px"};
        this.TokenSearchEntry.EntryLabel.Text = "";
        this.TokenSearchEntry.EntryInput.onChange = this.searchTextChanged.bind(this);

        searchBoxContainer.push(this.TokenSearchEntry);

        const clearSearch = new CloseIconBtn();
 
        clearSearch.onClick = ()=>{
            this.TokenSearchEntry.EntryInput.Value = "";
            
            LUIX.postData('tokenSearchString', "");
            // this.SearchResultsBox.clear();
            // this.listOfWallets.splice(0, this.listOfWallets.length);
            // LUIX.removeData("sendTruTo");
        };
 
        searchBoxContainer.push(clearSearch);
        this.children.push(searchBoxContainer);


    }

    public TokenSearchEntry = new Entry();
    
 
    private async searchTextChanged(e:any){
        var entryText =  this.TokenSearchEntry.EntryInput.Value;
      
        //try and get wallets:
        if(entryText.length > 1){
            // let searchWalletResults = await TruWalletAPI.SearchWallet(entryText);
            
            // if(searchWalletResults.length > 0){
               
            //     var listOfWallets: TruWallet[] = [];
            //     searchWalletResults.forEach((w)=>{
            //          listOfWallets.push( w );
            //     });

            //     LUIX.postData('tokenSearchString', listOfWallets);
            //     this.TokenSearchEntry.EntryInput.focus();
            // }
        }
        
    }
 
}