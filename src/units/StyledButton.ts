import {Button} from 'luix';

export default class StyledButton extends Button{
    constructor(props: any = null){
        super(props);
        this.addClass("selectable");
        this.style = {fontSize: "15px", fontWeight: 600,  borderRadius: "5px", borderWidth: "3px", margin: "10px" };
        if (props != null) {
            this.onClick = props.onClick;
        }
    }

    public render(): HTMLElement {
        return super.render();
    } 
}