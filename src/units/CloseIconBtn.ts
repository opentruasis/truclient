import {IconButton, CloseIcon } from 'luix';

export default class CloseIconBtn extends IconButton{
    constructor(props: any = null){
        super(props);
        this.addClass("selectable");
        this.style = {maxWidth: "40px", maxHeight: "40px", overflow: 'hidden'};
        if (props != null) {
            this.onClick = props.onClick;
        }else{
            this.onClick = () =>{}
        }
        this.innerText = "";
        var xIcon: CloseIcon = new CloseIcon({width: "40px", height: "40px"});
        xIcon.style = {position: 'relative', left: "-7.75px", top:"-2.75px"};
        this.children.push(xIcon);
    }

    public changed(props: any = null){
        
        
        super.changed(props);
    }
 
}