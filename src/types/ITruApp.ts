export default interface ITruApp{
    ID?: string,
    Name: string,
    Description: string,
    Program: string,
    ShowcaseFiles: any[],
    GitRepoURL: string,
    Hash: string,
    CreatedBy: string
};
