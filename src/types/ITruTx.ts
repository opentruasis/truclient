import WalletKeys from './IWalletKeys';
import ISignedMsg from './ISignedMsg';

export default interface ITruTx extends ISignedMsg{
    ID: string,
    TruAmount: number,
    ReceiverId?: string,
    ReceiverPubKey?: string,
    isVerified: boolean,
    CreatedOn: string,
    SignerId: string
};
