import OrderStatus from "./OrderStatus";
import OrderType from "./OrderType";

export default interface ITruOrder{
    ID : number,
    OrderType: OrderType,
    CreatedOn: string,
    UpdatedOn: string
    Qty: number,
    CumQty: number,
    LeavesQty: number,
    LimitPrice: number
    OrderStatus: OrderStatus
}