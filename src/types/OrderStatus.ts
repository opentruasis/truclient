enum OrderStatus { 
    None = 0,
    Initiated,
    Paid,
    Fullfilled,
    Canceled
}

export default OrderStatus; 
