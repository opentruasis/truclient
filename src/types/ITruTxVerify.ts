import ISignedMsg from './ISignedMsg';

export default interface ITruTxVerify extends ISignedMsg{
    SenderBalance: number,
    TxID: string
};
