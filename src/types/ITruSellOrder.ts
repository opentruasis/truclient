import ITruOrder from "./ITruOrder";

export default interface ITruSellOrder extends ITruOrder{
    SellerPayPalEmail: string
    PaypalTx: string
    SendTruMsg: string // Send Tru Msg.  Signed by the seller to MasterWallet..
}