export default interface ITruPrice{
      ID?: number,
      TimeStamp: Date,
      Price: number 
}