import ITruOrder from "./ITruOrder";

export default interface ITruBuyOrder extends ITruOrder{
    BuyerTruID: string,
    BuyerPubKey: string,
    PaypalBuyURL: string,
    PaypalTx: string,
    PaypalBuyerEmail: string
}