export default interface  SignedMsg{
    SignerId?: string,
    Hash?: string,
    Signature?: string,
    stringifyMsg: ()=>string
};
