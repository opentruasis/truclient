import WalletKeys from './IWalletKeys';
import ITruTx from './ITruTx';
import TruCrypto from '../api/TruCrypto';
import { v4 as uuidv4 } from 'uuid';



export default class TruTxMsg implements ITruTx {
    constructor(){
        this.ID = uuidv4(); 
    }
    public ID: string;
    public TruAmount: number;
    public ReceiverId?: string;
    public ReceiverPubKey?: string;
    public CreatedOn: string;
    public isVerified: boolean;
    public SignerId: string;
    public stringifyMsg() :string {
            
            var encodedMsgString = JSON.stringify({
                ID: this.ID, TruAmount: this.TruAmount.toFixed(6), SignerId:this.SignerId, ReceiverId:this.ReceiverId, ReceiverPubKey:this.ReceiverPubKey
            });
 
    return encodedMsgString;
}

};
