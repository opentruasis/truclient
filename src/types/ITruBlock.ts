import WalletKeys from './IWalletKeys';
import ITruTx from './ITruTx';

export default interface ITruBlock{
    ID: number,
    PreviousHash: string,
    Transactions: ITruTx[],
    BlockHash: string
};
