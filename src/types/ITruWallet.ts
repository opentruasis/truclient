import WalletKeys from './IWalletKeys';

export default interface ITruWallet{
    ID?: string,
    Keys: WalletKeys,
    Handle: string,
    FullName: string,
    PushNotifToken: string,
    ProfilePicBase64: string,
    Balance: number
};
