export default interface IWalletKeys{
    PubKey: string,
    PrivKey: string
}