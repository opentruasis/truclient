 
import ITruTxVerify from './ITruTxVerify';

export default class TruTxVerifyMsg implements ITruTxVerify {
    public SenderBalance: number;
    public TxID: string;
    public CurrentBlockHash: string;
    public stringifyMsg() :string {
            var encodedMsgString = JSON.stringify({
                SenderBalance: this.SenderBalance.toFixed(6).toString(), TxID:this.TxID
            });
 
            return encodedMsgString;
        }

};
