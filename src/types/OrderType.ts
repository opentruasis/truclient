enum OrderType {
    Buy = 0,
    Sell  = 1
}

export default OrderType;