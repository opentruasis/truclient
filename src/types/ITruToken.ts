export default interface ITruToken{
    ID?: string,
    Name: string,
    FileExt: string,
    ForSale: boolean,
    SalePrice: number,
    CreatedBy: string
};
