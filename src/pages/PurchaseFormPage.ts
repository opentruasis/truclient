import {LUIX, Elem, FlexBox, Text} from 'luix';
 
import StyledButton from '../units/StyledButton';
import AppLoading from '../components/AppLoading';
 
export default class PurchaseFormPage extends Elem {

    constructor(props: any = null){
        super("div", props);

            
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "space-between",
            overflowY: "auto" 
        };

   
        const label = new Text("Purchase Form.");
        this.children.push(label);
  
        //Get params...

        LUIX.postData("showLoader", true);

        setTimeout(()=>{

            var url = LUIX.getStateAt("currentRoute");

            
            
            

            label.Text = url.search.h;

            this.loaded = true;
            this.changed(props);


 
        }, 100);

        this.children.push(new AppLoading());
 
    }

    private loaded: boolean = false;

    public changed(props: any = null){
        
        
        
        if(this.loaded){
            if(LUIX.getStateAt("showLoader")){
                LUIX.postData("showLoader", false);
            }
        }else{
            if(!LUIX.getStateAt("showLoader")){
                LUIX.postData("showLoader", true);
            }
        }

        super.changed(props); // Gotta call children..
    }

}
