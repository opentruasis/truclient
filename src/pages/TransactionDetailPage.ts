import {LUIX, Elem, FlexBox, Text} from 'luix';
 
import ITruTx from '../types/ITruTx';
 
import LocalChainV2 from '../api/LocalChainV2';
 
export default class TransactionDetailPage extends Elem {

    constructor(props: any = null){
        super("div", props);

        // return;
        this.style = {
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignItems: "center",
            marginTop: "30px",
            overflowY: "auto" 
        };

        const txDetailBox = new FlexBox();
        txDetailBox.style = { flexDirection: 'column',  maxWidth: "980px",  justifyContent: 'space-around', height: '100%', width: '100%', overflowWrap:'break-word'};
        txDetailBox.push(this.transactionHeader);
        txDetailBox.push(this.txID);
        txDetailBox.push(this.txCreatedOn);
        txDetailBox.push(this.txTruAmount);
        txDetailBox.push(this.txSenderId);
        txDetailBox.push(this.txReceiverId);
        txDetailBox.push(this.txReceiverPubKey);
        txDetailBox.push(this.txisVerified);
        txDetailBox.push(this.txSignerId);
        txDetailBox.push(this.txSignature);
        txDetailBox.push(this.txHash);
        

 
        this.children.push(txDetailBox);
    }

    private transactionHeader  =  new Text("", {style: {margin: "5px", fontSize: "20px"}});

    private txTruAmount  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txSenderId  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txReceiverId  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txReceiverPubKey  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txID =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txisVerified  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txCreatedOn  =  new Text("", {style: {margin: "5px"}});
    private txSignature  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txHash  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});
    private txSignerId  =  new Text("", {style: {margin: "5px", wordBreak: 'break-all'}});


    public async changed(props:any){

        var routeParams = LUIX.getStateAt("currentRoute");
        console.log("Params>>>");
        console.log(routeParams);
        if(routeParams.search.transaction){
            this.visible = true;
            this.transactionHeader.Text = "TRU Transaction: " + routeParams.search.transaction + " --- @ Block: " + routeParams.search.block;
     
            var truTx: ITruTx =  await LocalChainV2.GetTxAt(routeParams.search.block,  routeParams.search.transaction)
         
            this.txID.Text = "Tx ID: " + truTx.ID.toString();
            this.txTruAmount.Text ="Amount: " + truTx.TruAmount.toString() + " TRU";

            this.txSenderId.Text = "Sender ID: " + truTx.SignerId.toString();
            this.txReceiverId.Text = "Receiver ID: " +  truTx.ReceiverId.toString();
            this.txReceiverPubKey.Text =  "Receiver PubKey: " +  truTx.ReceiverPubKey.toString();
            this.txisVerified.Text = "Tx Verified: " + truTx.isVerified.toString();
            this.txCreatedOn.Text = "Created On: " + new Date( parseInt(truTx.CreatedOn) * 1000  ).toString();
            this.txSignature.Text = "Tx Signature: " + truTx.Signature.toString();
            this.txHash.Text = "Tx Hash: " + truTx.Hash.toString();
            this.txSignerId.Text = "Tx Signer ID: " +  truTx.SignerId?.toString();
            
      
        }else{

            this.visible = false;
 
        }
        super.changed(props);
    }


}
