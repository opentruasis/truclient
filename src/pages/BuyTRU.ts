import {LUIX, Elem, FlexBox, ArrowLeft, Text, Entry, Input} from 'luix';
import LocalExchangeAPI from '../api/LocalExchangeAPI';
import TruExchangeAPI from '../api/TruExchangeAPI';
import ITruWallet from '../types/ITruWallet';
import StyledButton from '../units/StyledButton';
 
export default class BuyTRU extends Elem {

    constructor(props: any = null){
        super("DIV", props);
 
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            overflowY: "auto" ,
            justifyContent:'flex-start',
            width: '100%'
        };

       
        const backButton: StyledButton = new StyledButton();
        backButton.style = {
            display: 'flex',
            alignItems: 'center',
            alignSelf: 'flex-start', 
            margin: "20px", 
            padding: '20px',
            paddingTop: "5px",  
            paddingBottom: "5px", 
            fontSize: "20px"
        };
        backButton.innerText = '';
        backButton.children.push(new ArrowLeft());
        var backBtnTxt = new Text();
        backBtnTxt.Text = "Back";
        backButton.children.push(backBtnTxt);
        
        backButton.onClick = ()=>{
            LUIX.GoTo("/exchange");
        };

        this.children.push(backButton)

        const formContainer = new FlexBox();
        formContainer.style={flexDirection: 'column', justifyContent:'center', height: '100%'};

        this.limitPriceEntry.style={margin: "20px"};
        this.limitPriceEntry.EntryLabel.Text = "Enter Limit Price ($): ";
        this.limitPriceEntry.EntryInput.InputType = "number";
        this.limitPriceEntry.EntryInput.onChange = (e)=>{
            console.log(e);
            var limitPriceinput = parseFloat(((e as InputEvent).target as HTMLInputElement).value);
            //validate price input
            console.log('limit price input: ' + limitPriceinput);
            if(limitPriceinput.toString() != 'NaN' && limitPriceinput >= 0){
                this.limitPrice = limitPriceinput;
            }else{
                this.limitPriceEntry.EntryInput.value = "";
            }
        };
        formContainer.push(this.limitPriceEntry);


        this.buyQty.style={margin: "20px"};
        this.buyQty.EntryLabel.Text = 'Enter Buy QTY: ';
        this.buyQty.EntryInput.InputType = "number";
        this.buyQty.EntryInput.onChange = (e)=>{
            console.log((e as InputEvent).data);
       
            var qtyinput = parseFloat(((e as InputEvent).target as HTMLInputElement).value);
            //validate qty.
            
            console.log('qty input: ' + qtyinput);
            if(qtyinput.toString() != 'NaN' && qtyinput > 0.00001){
                this.qty = qtyinput;
            }else{
                this.buyQty.EntryInput.value = "";
            }
        
        };
        formContainer.push(this.buyQty);


        const termsText: Text = new Text();
        termsText.style={margin: '20px'};
        termsText.Text = "Order good until canceled/fullfilled. $5 Paypal Fee on fullfillment/cancellation.";
        formContainer.push(termsText);
        

        const buyBtn: StyledButton = new StyledButton();
        buyBtn.style={margin: "20px", fontSize: "20px", padding: '20px', alignSelf: 'center'};
        buyBtn.innerText = "BUY TRU";
        buyBtn.onClick = async ()=>{
            LUIX.postData('showLoader', true);
            var myWallet : ITruWallet = LUIX.getStateAt('wallet');
            var newTx = { 
               BuyerTruID: myWallet.ID,
               BuyerPubKey: myWallet.Keys.PubKey,
               Qty : this.qty, 
               LimitPrice: this.limitPrice
            };
           var buyResult = await TruExchangeAPI.PostBuyOrder(newTx);
           console.log("BUY RESULT>>>");
           console.log(buyResult);
           LocalExchangeAPI.SaveOrder(buyResult);
            //open window with link..

            var uri = buyResult.PaypalBuyURL;
            console.log("URI:  " + uri);

            var newwindow = window.open(uri,'name' );
	        if (window.focus) {newwindow.focus()}
            LUIX.postData('showLoader', false);
            LUIX.GoTo('/exchange');
        };
        
        formContainer.push(buyBtn);

        this.children.push(formContainer);
 
    }

    private limitPriceEntry: Entry = new Entry();

    private buyQty: Entry = new Entry();

    private qty: number = 0;
    private limitPrice: number = 0;

    public changed(props: any = null){

        super.changed(props);
    }

}
