import {LUIX, Elem,FlexBox, ArrowLeft, Text, Entry, ElemList} from 'luix';
import LocalExchangeAPI from '../api/LocalExchangeAPI';
import TruExchangeAPI from '../api/TruExchangeAPI';
import ITruOrder from '../types/ITruOrder';
import OrderType from '../types/OrderType';
import ExchangeOrderLine from '../units/ExchangeOrderLine';
 
import StyledButton from '../units/StyledButton';
 
export default class MyTruOrders extends Elem {

    constructor(props: any = null){
        super("DIV", props);
 
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            overflowY: "auto" ,
            justifyContent:'flex-start',
            width: '100%'
        };

 
        setTimeout(async ()=>{
            var orderList = LocalExchangeAPI.GetOrders();
            console.log("Order LIST>>>>>>");
            console.log(orderList);
            if(orderList){
                for(var o of orderList)
                {
                    var orderStatusFromExchange = await TruExchangeAPI.GetOrderStatus(o.ID, o.OrderType);
                    console.log("oooooooooooooooooooo");
                    console.log(orderStatusFromExchange);
                    if (orderStatusFromExchange)
                    {
                    LocalExchangeAPI.SaveOrder(orderStatusFromExchange);
                    }
                }
            }
        });
       
        const backButton: StyledButton = new StyledButton();
        backButton.style = {
            display: 'flex',
            alignItems: 'center',
            alignSelf: 'flex-start', 
            margin: "20px", 
            padding: '20px',
            paddingTop: "5px",  
            paddingBottom: "5px", 
            fontSize: "20px"
        };
        
        backButton.innerText = '';
        backButton.children.push(new ArrowLeft());
        var backBtnTxt = new Text();
        backBtnTxt.Text = "Back";
        backButton.children.push(backBtnTxt);
        
        backButton.onClick = ()=>{
            LUIX.GoTo("/exchange");
        };

        this.children.push(backButton)


        const headerLabels: FlexBox = new FlexBox();
        headerLabels.style = {justifyContent: 'space-around', width: '100%', fontSize: "18px", borderBottom: "2px solid #333"};
        const priceLabel: Text = new Text();
        priceLabel.Text = "Price ($): ";
        headerLabels.push(priceLabel);
        const qtyLabel: Text = new Text();
        qtyLabel.Text = "Qty: ";
        headerLabels.push(qtyLabel);
        const statusLabel: Text = new Text();
        statusLabel.Text = "Status: ";
        headerLabels.push(statusLabel);
        this.children.push(headerLabels);

        this.ordersList.style = {width: '100%', fontSize: "18px", overflow: 'auto'};
        this.children.push(this.ordersList);

       
     

        const tabsContainer = new FlexBox();
        tabsContainer.style={flexDirection: 'row', width: '100%', justifyContent:'space-between'};
 
        const myBidsToggleBtn: StyledButton = new StyledButton();
        myBidsToggleBtn.style={margin: "20px", fontSize: "20px", padding: '10px', width: '100%'};
        myBidsToggleBtn.innerText = "My Bids";
        myBidsToggleBtn.onClick = ()=>{
            this.viewBids = true;
            this.changed(this.props);
        }

        tabsContainer.push(myBidsToggleBtn);

     
        const myAsksToggleBtn: StyledButton = new StyledButton();
        myAsksToggleBtn.style={margin: "20px", fontSize: "20px", padding: '10px', width: '100%' };
        myAsksToggleBtn.innerText = "My Asks";

        myAsksToggleBtn.onClick = ()=>{
           this.viewBids = false;
           this.changed(this.props);
        };

        tabsContainer.push(myAsksToggleBtn);

        this.children.push(tabsContainer);
 
    }

    private viewBids: boolean = true;
  
    private ordersList: ElemList = new ElemList();
 

    public changed(props: any = null){

        this.ordersList.clear();

        var myOrders = LocalExchangeAPI.GetOrders();

        var screenHeight =  LUIX.getStateAt('screenHeight');
       
        this.ordersList.style = {height: screenHeight - 320 + "px"};

        console.log("My Orders? ");
        console.log(myOrders);

        if(myOrders){
            myOrders.forEach((o: ITruOrder)=>{
                if(o){
                    if(o.OrderType == OrderType.Buy && this.viewBids){
                        var exchangeLine  = new ExchangeOrderLine(o);
                        exchangeLine.fillData(o);
                        this.ordersList.children.push(exchangeLine);
                    }else if(o.OrderType == OrderType.Sell && !this.viewBids){
                        var exchangeLine  = new ExchangeOrderLine(o);
                        exchangeLine.fillData(o);
                        this.ordersList.children.push(exchangeLine);
                    }
                }
            });
        }
        super.changed(props);
    }

}
