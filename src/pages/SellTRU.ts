import {LUIX, Elem, FlexBox, ArrowLeft, Text, Entry} from 'luix';
import LocalExchangeAPI from '../api/LocalExchangeAPI';
import LocalWalletAPI from '../api/LocalWalletAPI';
import TruCrypto from '../api/TruCrypto';
import TruExchangeAPI from '../api/TruExchangeAPI';
import ITruSellOrder from '../types/ITruSellOrder';
import ITruWallet from '../types/ITruWallet';
import TruTxMsg from '../types/TruTxMsg';
 
import StyledButton from '../units/StyledButton';
 
export default class SellTRU extends Elem {

    constructor(props: any = null){
        super("DIV", props);
 
       
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            overflowY: "auto" ,
            justifyContent:'flex-start',
            width: '100%'
        };
  
        const backButton: StyledButton = new StyledButton();
        backButton.style = {
            display: 'flex',
            alignItems: 'center',
            alignSelf: 'flex-start', 
            margin: "20px", 
            padding: '20px',
            paddingTop: "5px",  
            paddingBottom: "5px", 
            fontSize: "20px"
        };
        backButton.innerText = '';
        backButton.children.push(new ArrowLeft());
        var backBtnTxt = new Text();
        backBtnTxt.Text = "Back";
        backButton.children.push(backBtnTxt);
        
        backButton.onClick = ()=>{
            LUIX.GoTo("/exchange");
        };

        this.children.push(backButton)

        const formContainer = new FlexBox();
        formContainer.style={flexDirection: 'column', justifyContent:'center', height: '100%'};

        this.limitPriceEntry.style={margin: "20px"};
        this.limitPriceEntry.EntryLabel.Text = "Enter Limit Price ($): ";
        this.limitPriceEntry.EntryInput.onChange = (e)=>{
            console.log(e);
            var limitPriceinput = parseFloat(((e as InputEvent).target as HTMLInputElement).value);
            //validate price input
            console.log('limit price input: ' + limitPriceinput);
            if(limitPriceinput.toString() != 'NaN' && limitPriceinput >= 0){
                this.limitPrice = limitPriceinput;
            }else{
                this.limitPriceEntry.EntryInput.value = "";
            }
        };

        formContainer.push(this.limitPriceEntry);
 
        this.sellQty.style={margin: "20px"};
        this.sellQty.EntryLabel.Text = 'Enter Sell QTY: ';
        this.sellQty.EntryInput.onChange = (e)=>{
            console.log(e);
            var qtyInput = parseFloat(((e as InputEvent).target as HTMLInputElement).value);
            //validate price input
            console.log('qty input: ' + qtyInput);
            if(qtyInput.toString() != 'NaN' && qtyInput > 0.000001){
                this.qty = qtyInput;
            }else{
                this.sellQty.EntryInput.value = "0";
            }
        };
        formContainer.push(this.sellQty);
  
        this.paypalEmail.style={margin: "20px"};
        this.paypalEmail.EntryLabel.Text = 'Enter Paypal Email: ';
        this.paypalEmail.EntryInput.onChange = (e)=>{
            console.log(e);
            var emailInput = ((e as InputEvent).target as HTMLInputElement).value;
            //validate price input
            console.log('qty input: ' + emailInput);
            if(this.validEmail(emailInput)){
                this.email = emailInput;
            } 
        };
        formContainer.push(this.paypalEmail);
  

        const termsText: Text = new Text();
        termsText.style={margin: '20px'};
        termsText.Text = "TRU held in escrow while awaiting match. Receive $ via Paypal after order fully fulfilled.  Order good until canceled/fulfilled. $5 Paypal Fee on fulfillment or on cancellation.";
        formContainer.push(termsText);
         
        const sellBtn: StyledButton = new StyledButton();
        sellBtn.style={margin: "20px", fontSize: "20px", padding: '20px', alignSelf: 'center'};
        sellBtn.innerText = "SELL TRU";
        sellBtn.onClick = async ()=>{

            var masterWalletInfo = await TruExchangeAPI.GetMasterWalletInfo();

            if (masterWalletInfo == null)
            {
                alert("Something went wrong with posting your Sell Order. Try again later.");
                return;
            }

            var masterPubKey = masterWalletInfo["pubKey"];
            var masterWalletId = masterWalletInfo["walletId"];
            var myWallet : ITruWallet = LUIX.getStateAt('wallet');
            
            const newTx : TruTxMsg = new TruTxMsg();
            newTx.TruAmount =   this.qty;
            newTx.ReceiverId = masterWalletId;
            newTx.ReceiverPubKey = masterPubKey;
            newTx.SignerId = myWallet.ID;
            console.log("THIS WALLET SENDING TRU????");
            console.log(myWallet);
            var localWallet = LocalWalletAPI.GetWallet(); // Get the local wallet private key! the data in LUIX is potentially null
            const paymentMsg = TruCrypto.SignMsg(newTx, localWallet.Keys.PrivKey)  as TruTxMsg;
            
         
            var truSellOrder: any =  
            {
                SellerPayPalEmail: this.email,
                SendTruMsg:  JSON.stringify(paymentMsg),
                Qty: this.qty,
                LimitPrice:  this.limitPrice
            };

            try
            {
                this.sellQty.EntryInput.value = "0";
                this.qty = 0;

                var initializedSellOrder = await TruExchangeAPI.PostSellOrder(truSellOrder);
                LocalExchangeAPI.SaveOrder(initializedSellOrder);

                alert("Your Sell Order has been Registered.");
    
                LUIX.GoTo('/exchange');
            }catch(e){

            }
        }; 
        
        formContainer.push(sellBtn);

 
        this.children.push(formContainer);
 
    }

    private  validEmail(emailString: string): boolean{
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   
         return re.test(emailString);
    }
  

    private limitPriceEntry: Entry = new Entry();

    private sellQty: Entry = new Entry();

    private paypalEmail: Entry = new Entry();

    private qty: number = 0;
    private limitPrice: number = 0;
    private email:string = "";

    public changed(props: any = null){

        super.changed(props);
    }

}
