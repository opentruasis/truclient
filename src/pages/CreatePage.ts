 
import {Elem, FlexBox} from 'luix';
 
 
import CreateTokens from '../components/CreateTokens';
import CreateTRUApp from '../components/CreateTRUApp';

export default class DeveloperPage extends Elem {

    constructor(props: any = null){
        super("div", props);

            
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            overflowY: "auto" ,
            justifyContent:'center'
        };

 
       this.DevContainer.style = {width: '100%', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around'};
 
      
       this.DevContainer.push(new CreateTokens());

       this.DevContainer.push(new CreateTRUApp());


       this.children.push(this.DevContainer);

 
 
    }

    public DevContainer: FlexBox = new FlexBox();

    public changed(props: any = null){
        var {isConnected = false} = props || {};
        if(isConnected){
            this.DevContainer.visible = true;
          
        }else{
            this.DevContainer.visible = false;
        }

        super.changed(props);
    }

}
