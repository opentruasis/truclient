 
import {Elem, Text} from 'luix';
 
import ToggleUpDwnBtn from '../units/ToggleUpDwnBtn';
 
export default class PurchaseFormBuilderPage extends Elem {

    constructor(props: any = null){
        super("div", props);

            
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "space-between",
            overflowY: "auto" 
        };
 
        const label = new Text("Purchase Form Builder:");
        this.children.push(label);

        const toggleUpDwnBtn = new ToggleUpDwnBtn({initDown: false});
 
        this.children.push(toggleUpDwnBtn);
    
    }
 

}
