import AvailApps from '../components/AvailApps';
import AvailTruasisTokens from '../components/AvailTruasisTokens';
import {Elem} from 'luix';
 
 
export default class TruasisPage extends Elem {

    constructor(props: any = null){
        super("div", props);

        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "flex-start",
            overflowY: "auto"

        };
 
        this.children.push(this.availApps);
        this.children.push(this.availTruasisTokens);
    }


    public availApps: AvailApps = new AvailApps();
    public availTruasisTokens: AvailTruasisTokens = new AvailTruasisTokens();


    public changed(props: any = null){
        var {isConnected = false} = props || {};
        if(isConnected){
            this.visible = true;
        }else{
            this.visible = false;
        }
        
        super.changed(props);
    }
}
