import {LUIX, Elem, FlexBox, Text} from 'luix';
 

import StyledButton from '../units/StyledButton';
import ChartViewBtn from '../units/ChartViewBtn';
import PriceChart from '../units/PriceChart';
import Paper from '../units/Paper';
import TruExchangeAPI from '../api/TruExchangeAPI';

export default class ExchangePage extends FlexBox {

    constructor(props: any = null){
        super(props);
 
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "space-between",
            overflowY: "auto" 
        };
 
         setTimeout(async ()=>{
            LUIX.postData("TruPrices", await TruExchangeAPI.GetTruPrices());
         });

         LUIX.postData("chartView", 5, false);
       
        
        const ChartFlexBox = new FlexBox();
        ChartFlexBox.style = {flexDirection: 'column', width: '100%'};
        
        
        const ChartContainer:Paper = new Paper();
        this.PriceChart.style={marginLeft: "10px", marginRight: "10px"};
        ChartContainer.push(this.PriceChart);

        ChartFlexBox.push(ChartContainer);
        const ChartBtnFlexBox = new FlexBox();
        ChartBtnFlexBox.style = {justifyContent: 'space-between', marginLeft: "10px", marginRight: "10px"};

 
       this.AllChartBtn.innerText = "ALL";
       this.AllChartBtn.onClick = ()=>{
            this.activateChartViewBtn("all");
            this.changed(LUIX.postData("chartView", 0, false));
        };
        this.HourChartBtn.addClass('active');
        ChartBtnFlexBox.push(this.AllChartBtn);

 
        this.YearChartBtn.innerText = "YEAR";
        this.YearChartBtn.onClick = ()=>{
            this.activateChartViewBtn("year");
            this.changed(LUIX.postData("chartView", 1, false));
        };
        ChartBtnFlexBox.push(this.YearChartBtn);

    
        this.MonthChartBtn.innerText = "MONTH";
        this.MonthChartBtn.onClick = ()=>{
            this.activateChartViewBtn("month");
            this.changed(LUIX.postData("chartView", 2, false));
        };
        ChartBtnFlexBox.push(this.MonthChartBtn);

 
        this.WeekChartBtn.innerText = "WEEK";
        this.WeekChartBtn.onClick = ()=>{
            this.activateChartViewBtn("week");
            this.changed(LUIX.postData("chartView", 3, false));
        };
        ChartBtnFlexBox.push(this.WeekChartBtn);

     
        this.DayChartBtn.innerText = "DAY";
        this.DayChartBtn.onClick = ()=>{
            this.activateChartViewBtn("day");
            this.changed(LUIX.postData("chartView", 4, false));
        };
        ChartBtnFlexBox.push(this.DayChartBtn);

  
        this.HourChartBtn.innerText = "HOUR";
        this.HourChartBtn.onClick = ()=>{
            this.activateChartViewBtn("hour");
            this.changed(LUIX.postData("chartView", 5, false));
        };
        ChartBtnFlexBox.push(this.HourChartBtn);

        ChartFlexBox.push(ChartBtnFlexBox);
 
        this.children.push(ChartFlexBox);
 
        const LastPriceContainer = new FlexBox();
        LastPriceContainer.style = {justifyContent: 'center', marginTop: '20px', marginBottom: '20px'};
        const priceLabel = new Text("1 TRU = $");
        priceLabel.style = {fontSize: "20px"};
        LastPriceContainer.push(priceLabel);
        this.LastPriceLabel.style = {marginLeft: "5px",fontSize: "20px"};
        LastPriceContainer.push(this.LastPriceLabel);
        this.children.push(LastPriceContainer);


        const SellBuyFlexBox  = new FlexBox();
        SellBuyFlexBox.style = {width: '100%', justifyContent: 'space-around'};

        const SellBtn: StyledButton = new StyledButton();
        SellBtn.innerText = "Sell TRU";
        SellBtn.style = {height: "50px", width:"150px", fontSize: "20px"};
        SellBtn.onClick = ()=>{
            LUIX.GoTo("/selltru");
        };


        const BuyBtn: StyledButton = new StyledButton();
        BuyBtn.innerText = "Buy TRU";
        BuyBtn.style = {height: "50px", width:"150px", fontSize: "20px"};
        BuyBtn.onClick = ()=>{
            LUIX.GoTo("/buytru");
        };
        SellBuyFlexBox.push(SellBtn);
        SellBuyFlexBox.push(BuyBtn);
    
        this.children.push(SellBuyFlexBox);


        const MyOrdersBtnContainer  = new FlexBox();
        SellBuyFlexBox.style = {width: '100%', justifyContent: 'space-around'};

        const MyOrdersBtn: StyledButton = new StyledButton();
        MyOrdersBtn.innerText = "My Orders";
        MyOrdersBtn.style = {height: "50px", width:"150px", fontSize: "20px"};
        MyOrdersBtn.onClick = ()=>{
            LUIX.GoTo("/mytruorders");
        };
        MyOrdersBtnContainer.push(MyOrdersBtn)

        this.children.push(MyOrdersBtnContainer);

        const filler = new FlexBox();
        filler.style = {flexDirection: 'column', flexGrow: 1};
        this.children.push(filler);

        const walletInfoContainer  = new FlexBox();
        walletInfoContainer.style = {justifyContent: 'space-around', width: '100%', flexWrap: 'wrap'};

        const WalletTRUValueContainer  = new FlexBox();
        WalletTRUValueContainer.style = {margin: "10px"};
        const yourTruLabel = new Text("Your TRU: ");
        yourTruLabel.style = {fontSize: "20px"};
        WalletTRUValueContainer.push(yourTruLabel);

        this.YourTRU.style = {marginLeft: "5px",fontSize: "20px"};
        WalletTRUValueContainer.push(this.YourTRU);

        const WalletDollarValueContainer  = new FlexBox();
        WalletDollarValueContainer.style = {margin: "10px"};
        const dollarTruValueLabel = new Text("TRU Value ($): ");
        dollarTruValueLabel.style = {fontSize: "20px"};
        WalletDollarValueContainer.push(dollarTruValueLabel);

        this.YourTRUDollarValue.style = {marginLeft: "5px",fontSize: "20px"};
        WalletDollarValueContainer.push(this.YourTRUDollarValue);

        walletInfoContainer.push(WalletTRUValueContainer);
        walletInfoContainer.push(WalletDollarValueContainer);
 
        this.children.push(walletInfoContainer);
  
    }


    private activateChartViewBtn(view: string){
        switch(view){
            case "all":
                this.AllChartBtn.addClass('active');
                this.YearChartBtn.removeClass('active');
                this.MonthChartBtn.removeClass('active');
                this.WeekChartBtn.removeClass('active');
                this.DayChartBtn.removeClass('active');
                this.HourChartBtn.removeClass('active');
            break;
            case "year":
                this.AllChartBtn.removeClass('active');
                this.YearChartBtn.addClass('active');
                this.MonthChartBtn.removeClass('active');
                this.WeekChartBtn.removeClass('active');
                this.DayChartBtn.removeClass('active');
                this.HourChartBtn.removeClass('active');

            break;
            case "month":
                this.AllChartBtn.removeClass('active');
                this.YearChartBtn.removeClass('active');
                this.MonthChartBtn.addClass('active');
                this.WeekChartBtn.removeClass('active');
                this.DayChartBtn.removeClass('active');
                this.HourChartBtn.removeClass('active');
            break;
            case "week":
                this.AllChartBtn.removeClass('active');
                this.YearChartBtn.removeClass('active');
                this.MonthChartBtn.removeClass('active');
                this.WeekChartBtn.addClass('active');
                this.DayChartBtn.removeClass('active');
                this.HourChartBtn.removeClass('active');
            break;
            case "day":
                this.AllChartBtn.removeClass('active');
                this.YearChartBtn.removeClass('active');
                this.MonthChartBtn.removeClass('active');
                this.WeekChartBtn.removeClass('active');
                this.DayChartBtn.addClass('active');
                this.HourChartBtn.removeClass('active');
            break;
            case "hour":
                this.AllChartBtn.removeClass('active');
                this.YearChartBtn.removeClass('active');
                this.MonthChartBtn.removeClass('active');
                this.WeekChartBtn.removeClass('active');
                this.DayChartBtn.removeClass('active');
                this.HourChartBtn.addClass('active');
            break;
        }
    }

    private PriceChart: PriceChart = new PriceChart();

    private  LastPriceLabel  = new Text();

    private AllChartBtn: ChartViewBtn = new ChartViewBtn();

    private YearChartBtn: ChartViewBtn = new ChartViewBtn();
    
    private MonthChartBtn: ChartViewBtn = new ChartViewBtn();
     
    private WeekChartBtn: ChartViewBtn = new ChartViewBtn();
     
    private DayChartBtn: ChartViewBtn = new ChartViewBtn();
   
    private HourChartBtn: ChartViewBtn = new ChartViewBtn();

    private YourTRU  = new Text();

    private YourTRUDollarValue  = new Text();
    
    public changed(props: any = null){

        var {isConnected = false} = props || {};
        if(isConnected){
            this.visible = true;
        }else{
            this.visible = false;
        }

        var {currentTRUPrice = 0.0000001, wallet = null} = props || {};

        this.LastPriceLabel.Text = currentTRUPrice.toFixed(4);
         
        if( wallet){
            this.YourTRU.Text = wallet.Balance.toFixed(4);
            this.YourTRUDollarValue.Text = (wallet.Balance * currentTRUPrice).toString();
        }

        super.changed(props);
    }   

}
