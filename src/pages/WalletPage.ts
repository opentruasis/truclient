import {Elem, Text, FlexBox, LUIX} from 'luix';

import AppLoading from '../components/AppLoading';
 

import NewWalletForm from '../components/NewWalletForm';
import WalletImportForm from '../components/WalletImportForm';
import UserSearch from '../components/UserSearch';

import TruWalletAPI from '../api/TruWalletAPI';
import LocalWalletAPI from '../api/LocalWalletAPI';
 
import SocketConnection from '../SocketConnection';

import WalletInfo from '../components/WalletInfo';
import SendTruView from '../components/SendTruView';
  
import ITruWallet from '../types/ITruWallet';
  
import MyTokens from '../components/MyTokens';
import UserSearchBar from '../units/UserSearchBar';
 
export default class WalletPage extends Elem {

    constructor(props: any = null){
        super("div", props);

       // TruCrypto.TestSign();
       // TruCrypto.TestRSACompare();

      // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "space-between",
            overflowY: "auto",
            width: "100%"
        };

     
       var hasLocalWallet = this.loadMyWallet();

 
 
       const newWalletForm = new NewWalletForm();
       const walletImport = new WalletImportForm();

 
        if(hasLocalWallet){
            SocketConnection.onConnectedInvokes.push(()=>{
                newWalletForm.visible = false;
                walletImport.visible = false;
                TruWalletAPI.WalletById(hasLocalWallet.ID);
            });
          
        } 
     

       this.walletLoading = new FlexBox();
       this.walletLoading.style = {flexDirection: 'column', flexGrow: 1};
       this.walletLoading.push(newWalletForm);
       this.walletLoading.push(walletImport);
       this.children.push(this.walletLoading);

       this.walletInfo = new WalletInfo();
       this.children.push(this.walletInfo);
    
           
        /////////////////////////////////////////////////////

        this.loggedInStuff = new FlexBox();
        this.loggedInStuff.style = {width: '100%', flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center'};
 
       

       const myTokens = new MyTokens();
       this.loggedInStuff.push(myTokens);

    
       const userSearchBar  = new UserSearchBar();

       this.loggedInStuff.push(userSearchBar);

      

 
       const userSearch = new UserSearch();
       this.loggedInStuff.push(userSearch);
   

        ////////////////////////////////////////////////

       const sendTruView =   new SendTruView();
       this.loggedInStuff.push(sendTruView);
  
 

       //////////////////////////////////////////////////


       this.children.push( this.loggedInStuff);
    
    }   

  
    private walletLoading: FlexBox = new FlexBox();
     
    public walletInfo: WalletInfo; 
 
    private loggedInStuff: FlexBox = new FlexBox();
 
    private loadMyWallet(): ITruWallet {
        LUIX.postData("showLoader", true);
        
        try {
            var wallet:ITruWallet =  LocalWalletAPI.GetWallet();
 
            if (wallet != null) {
       
                LUIX.post({"wallet":wallet, "showLoader": false});
                return wallet;
            }else{
                return null;
            }
        } catch(e) {
             
        }
    } 

  
    public changed(props: any){    
        var {isConnected = false} = props || {};
        if(isConnected){
            this.visible = true;
        }else{
            this.visible = false;
        }

        if(LUIX.getStateAt("wallet")){
            this.walletLoading.visible = false;
            this.loggedInStuff.visible = true;
        }else{
            this.walletLoading.visible = true;
            this.loggedInStuff.visible = false;
        }
        
        super.changed(props);
    }

 
}
