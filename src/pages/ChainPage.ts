import {LUIX, Elem,FlexBox} from 'luix';
 
import TransactionDetailPage from './TransactionDetailPage';
import TruChain from '../components/TruChain';
import LocalChainV2 from '../api/LocalChainV2';
import ITruBlock from '../types/ITruBlock';
 
export default class ChainPage extends Elem {

    constructor(props: any = null){
        super("DIV", props);
 
        // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center',
            overflowY: "auto" 
        };


  
        this.children.push(this.TruChain);

        this.children.push(this.TransactionDetailPage);
     
    }

    public TruChain: TruChain = new TruChain();

    public TransactionDetailPage: TransactionDetailPage = new TransactionDetailPage();


    public changed(props: any = null){
        console.log("Chain Page Changed..");
        console.log(props);
        var {isConnected = false} = props || {};
        if(isConnected){
            this.visible = true;
        }else{
            this.visible = false;
        }

        super.changed(props);
    }



}
