import * as signalR from "@microsoft/signalr";
import { LUIX } from "luix";
import ITruWallet from './types/ITruWallet'; 
import TruTxVerifyMsg from "./types/TruTxVerifyMsg";
 
import ITruTx from './types/ITruTx';
import TruCrypto from "./api/TruCrypto";
import LocalSuperNodeURL from "./api/LocalSuperNodeURL";
import LocalChainV2 from "./api/LocalChainV2";
import ITruBlock from "./types/ITruBlock";
import ChainLoader from "./api/ChainLoader";
class SocketConnection {
 
    public static onConnected: ()=>void = ()=>{};

    public static onDisconnected: ()=>void = ()=>{};
      
    private static hubCon: any;

    public static onConnectedInvokes:  { (): void; }[] = [];

    public static async TryConnect(url:string) : Promise<void>{
        console.log("Try to Connect: " + url);
        LUIX.postData("showLoader", true);
        try{
            SocketConnection.canTryAgain = false;
            SocketConnection.hubCon = new signalR.HubConnectionBuilder().withUrl(url + "/ws/truclient").configureLogging(signalR.LogLevel.Error).build();
       
            var connectResults =  await SocketConnection.StartSocketConnection();
 
            if(connectResults){
                SocketConnection.isConnected = true;
                SocketConnection.onConnected();
                SocketConnection.SetConnectionCallbacks();

                for(var invokable of  SocketConnection.onConnectedInvokes){
                    invokable();
                }
              
                SocketConnection.SendHeartBeat();
                SocketConnection.canTryAgain = true;
            }else{
                SocketConnection.isConnected = false;
                SocketConnection.canTryAgain = true;
            }

        }catch(e){
            
            SocketConnection.isConnected = false;
            SocketConnection.canTryAgain = true;
            throw e;
        }
        LUIX.postData("showLoader", false);
    }

    public static disconnect(){
        SocketConnection.isConnected = false;
        if(SocketConnection.hubCon.connectionState == "Connected"){
            SocketConnection.hubCon.invoke("ClientDisconnect", SocketConnection.hubCon.connection.connectionId );
        }
        SocketConnection.onDisconnected();
    }
 
    public static isConnected: boolean = false;
    public static canTryAgain: boolean = true;
  
    public static async  StartSocketConnection(): Promise<any>{
        //maxtgimeout..
        return new Promise((resolve, reject)=>{
            SocketConnection.hubCon.start({ withCredentials: false }).then((success:any)=>{
                resolve("success");
            }, (err:any)=>{
                reject(null);
            });
        });
    }


    private  static SendHeartBeat(){
        setTimeout(async ()=>{    
       
          
            if(SocketConnection.hubCon.connectionState == "Connected"){
      
                var SocketConnectionWallet = LUIX.getStateAt('wallet');

                if(SocketConnectionWallet){
         
                    var localSuperNodeURLs = LocalSuperNodeURL.GetUrls();
                    var localChainBlockCount = await LocalChainV2.GetBlockCount();
                    console.log("Local Chain?.....", localChainBlockCount)
                    if(localChainBlockCount){
                        SocketConnection.myLastBlockID = localChainBlockCount-1; // ID is less one the count..
                    }else{
                        SocketConnection.myLastBlockID = 0;
                    }
                  
                    SocketConnection.hubCon.invoke("HeartBeat", 
                    JSON.stringify(
                        { 
                        walletId: SocketConnectionWallet.ID,
                        heartbeat  : SocketConnection.heartBeatCount, 
                        endblockID : SocketConnection.myLastBlockID, 
                        connectionId: SocketConnection.hubCon.connection.connectionId,
                        superNodeCount: localSuperNodeURLs.length
                    }));

                    // SocketConnection.hubCon.invoke("Heartbeat",   SocketConnection.heartBeatCount );
                    if(SocketConnection.isConnected){
                        SocketConnection.SendHeartBeat();
                    }
                }
            }else{
                SocketConnection.isConnected = false;
                SocketConnection.heartBeatCount = 0;
            }
        },5000);
    }

    public static GetChainData(blockNum: number ){
        console.log("GEt Chain Data??????");
        console.log(blockNum);
        if(SocketConnection.hubCon.connectionState == "Connected"){
            SocketConnection.hubCon.invoke("LoadChainBlocks", JSON.stringify({HubConnectionId: SocketConnection.hubCon.connection.connectionId, blockNum: blockNum})); 
        }
    }

    public static myLastBlockID: number = 0;

    private static heartBeatCount: number = 0;

    private static listOfTxValidations: {[id:string]: number} = {};
  
    private static SetConnectionCallbacks():void {
 
        SocketConnection.hubCon.on('HeartbeatResponse', (msg: any) =>{
  
            let resp = JSON.parse(msg);
            
            if(resp.heartbeat as number != SocketConnection.heartBeatCount){
                SocketConnection.hubCon.stop();
            }
                       
            SocketConnection.heartBeatCount = resp.heartbeat + 1;
        });




        SocketConnection.hubCon.on('NewTxToValidate', async (newTxToValidateJson:any) => {
         
            if(!ChainLoader.GetChainLoader()) return;

            if(!(await LocalChainV2.ValidChain())) return;
                let newTxToValidate: ITruTx = JSON.parse(newTxToValidateJson);
        
                if(newTxToValidate.ID in SocketConnection.listOfTxValidations){
                    return;
                }else{
                    SocketConnection.listOfTxValidations[newTxToValidate.ID] = 0;
                }
            
                console.log("Trying to validate a tx: ");
                console.log(newTxToValidate);
                var senderBalanceAsISeeIt: number = await LocalChainV2.BalanceAt(newTxToValidate.SignerId);
                console.log(senderBalanceAsISeeIt);
                console.log("-----")
                var verificationMsg: TruTxVerifyMsg = new TruTxVerifyMsg();
                verificationMsg.TxID = newTxToValidate.ID;
                verificationMsg.SenderBalance = senderBalanceAsISeeIt;
                var lastBlock = await LocalChainV2.GetLastBlock();
                verificationMsg.CurrentBlockHash = lastBlock.PreviousHash;
                var localWallet:ITruWallet = LUIX.getStateAt("wallet");
    
                const signedMsg = TruCrypto.SignMsg(verificationMsg, localWallet.Keys.PrivKey)  as TruTxVerifyMsg;
    
                var signedMsgJson = JSON.stringify(signedMsg);

                SocketConnection.hubCon.invoke("TxValidation", signedMsgJson);
 
        });




        SocketConnection.hubCon.on('ChainData', async (chainData:any) => {
           let latestChain = JSON.parse(chainData);
            console.log("Got Chain Data in socket connection..");
            console.log(latestChain);
            var lstBlock : number = await LocalChainV2.GetBlockCount();
            Object.values (latestChain).forEach(async (block: ITruBlock)=>{
                if(block.ID <= lstBlock-1){
                    console.log("Update Bloc: " + block.ID);
                  var updateRes =  await LocalChainV2.UpdateLatestBlock( block  );

                  console.log("UPdate Res", updateRes);
                }else{
                    LocalChainV2.AddToChain( block  );
                }
            });
           console.log("==========================================================");
        });

      


        SocketConnection.hubCon.on('SuperNodeList', (superNodeListData:any) => {
           
            var data = JSON.parse(superNodeListData);
            //Add url to SuperNodeAPI..
            for(var d of data){
                LocalSuperNodeURL.AddUrl(d);
            }
        });

        SocketConnection.hubCon.on('Send', (msg:any) => {
             
             
        });

        SocketConnection.hubCon.onclose(() => {
              
             
            SocketConnection.onDisconnected();
        });
         
        
         
    }
 
}





export default SocketConnection;

