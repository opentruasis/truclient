import {Elem, FlexBox, Image} from 'luix';
 
 
export default class AppModal extends Elem {

    constructor(props: any = null) {
        super("div", props);

        this.style = {
            display:'flex',
            position: 'fixed',
            width: '100%',
            zIndex: 1275,
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.8)',
            alignItems: 'center',
            justifyContent: 'center',
        };
        this.isFlexDisplay = true;
 
        this.box.style={ 
            boxShadow: '1px 1px 10px 2px rgba(119, 119, 119, 0.75)',
            margin: '0 10px',
            backgroundColor:'#fff',
            minWidth: '200px',
            padding:'10px',
            flex: 1,
            maxWidth: '800px',
            flexDirection: 'column'
         };

 
        this.children.push(this.box);
 
        this.visible = false;
   
    }   

    private box: FlexBox = new FlexBox();
   
    public changed(props: any) {
        
        var { showModal = false, modalContent = null} = props || {}; 
        if(modalContent){
            this.box.clear();
            this.box.children.push(modalContent);
            this.visible = showModal;
        }else{
            if(!showModal){
                this.box.clear();
                this.visible = showModal;
            }
        }
        super.changed(props);  //Do this if you don't have 
    }

 


}