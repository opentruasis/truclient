
import {LUIX, FlexBox, Entry} from 'luix';
 
 
import StyledButton from '../units/StyledButton';
import TruCrypto from '../api/TruCrypto';

import TruPushNotifications from '../api/TruPushNotifications';
import TruWalletAPI from '../api/TruWalletAPI';
import TruWallet from '../types/ITruWallet';
 
import LocalWalletAPI from '../api/LocalWalletAPI';
import WalletKeys from '../types/IWalletKeys';
  
import LocalChainV2 from '../api/LocalChainV2';
 
export default class WalletImportForm extends FlexBox {

    constructor(props: any = null) {
        super(props);
       
        this.style = { display: 'flex', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', overflowY: "auto", width:"100%"};
        
        this.PrivateKeyEntry.EntryLabel.Text = "Import Wallet Key:";
        this.children.push(this.PrivateKeyEntry);
    
        this.PrivateKeyEntry.EntryInput.onChange = this.onDataChanged.bind(this);
       
        this.GetImportWalletBtn.innerText = "Import Wallet";
        this.GetImportWalletBtn.disabled = true;
        this.children.push(this.GetImportWalletBtn);

    }
     
  
    public PrivateKeyEntry  = new Entry();
 

    private GetImportWalletBtn : StyledButton = new StyledButton({ onClick: this.newWalletSubmit.bind(this) });
    

    public changed(props: any): void {
        let _loadedWallet = LUIX.getStateAt('wallet');
        if(!_loadedWallet){
            this.visible = true;
        }else{
            this.visible = false;
        }

        if(this.PrivateKeyEntry.EntryInput.Value.length < 1 ){
            this.GetImportWalletBtn.disabled = true;
        }
       
        super.changed(props);
    }


    private onDataChanged(e:Event){
        if(this.PrivateKeyEntry.EntryInput.Value.length > 0){
            this.GetImportWalletBtn.disabled = false;
        }
    }

    private async newWalletSubmit(){
        
       var privateKey = this.PrivateKeyEntry.EntryInput.Value;
       var walletKeys: WalletKeys = await TruCrypto.ImportKey(privateKey);
     
        var result = await TruWalletAPI.GetWalletByPubKey(walletKeys.PubKey);

        let loadedTruWallet: TruWallet;
        var newPushNotifToken = TruPushNotifications.GetNewPushToken();
        
        
        
        if(!result){
            loadedTruWallet = {
                Balance: 0,
                FullName: "",
                Handle: "",
                ID: "",
                Keys: walletKeys,
                ProfilePicBase64: "",
                PushNotifToken: newPushNotifToken
            };
  
        }else{
            loadedTruWallet = {
                Balance: result.Balance,
                FullName:  result.FullName,
                Handle: result.Handle,
                ID: result.ID,
                Keys: walletKeys,
                ProfilePicBase64: result.ProfilePicBase64,
                PushNotifToken: newPushNotifToken
            };
 
        }
 
        loadedTruWallet.Balance = await LocalChainV2.BalanceAt(loadedTruWallet.ID);
        LocalWalletAPI.SetWallet(loadedTruWallet);
        this.visible = false;
       
        this.PrivateKeyEntry.EntryInput.Value = "";
        LUIX.postData("wallet", loadedTruWallet);
    }


    

}