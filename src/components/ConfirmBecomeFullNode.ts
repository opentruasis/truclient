import {Elem, FlexBox, Image, Text, LUIX} from 'luix';
import ChainLoader from '../api/ChainLoader';
import CloseIconBtn from '../units/CloseIconBtn';
import StyledButton from '../units/StyledButton';
 
 
export default class ConfirmBecomeFullNode extends FlexBox {

    constructor(props: any = null) {
        super(props);

        this.style = {flexDirection: 'column', justifyContent: 'cetner'};

        const becomeFullNodeHeader = new FlexBox();
        becomeFullNodeHeader.style={width: "100%", justifyContent: 'space-between'};
        const headerLabel = new Text();
        headerLabel.Text = "Turn Full Node Mode On: "
        headerLabel.style = { "fontSize": "20px" , marginLeft: "10px"};

        const clearSearch = new CloseIconBtn();
        clearSearch.onClick = ()=>{
            ChainLoader.SetChainLoader(false);
            LUIX.post({'showModal': false, 'modalContent': null }, true);
        };

        becomeFullNodeHeader.push(headerLabel);
        becomeFullNodeHeader.push(clearSearch);

        this.children.push(becomeFullNodeHeader);

  
   
        this.notificationText.Text = `
        Turning on the Full Node mode helps the Truasis Network become more secure and decentralized. 
        By Turrning on Full Node mode you will download the entire TRU Block Chain.
        The current block count is ${ LUIX.getStateAt('blockCount') } and is approximately ${ LUIX.getStateAt('chainSize')}.
        Thanks for participating in our Network!
        `;

        this.children.push(this.notificationText);


        const commitBtn: StyledButton = new StyledButton();
        commitBtn.onClick = ()=>{
            ChainLoader.SetChainLoader(true);
            LUIX.post({'showModal': false, 'modalContent': null }, true);
        };
        commitBtn.style={alignSelf: 'flex-end', padding: "10px", marginTop: "20px"};
        commitBtn.innerText = "Start Sync";
 
        this.children.push(commitBtn);
 
    }   

 

    private notificationText: Text = new Text();

 
    public changed(props: any) {
        console.log("Become Full Node Changed..");

        this.notificationText.Text = `
        Turning on the Full Node mode helps the Truasis Network become more secure and decentralized. 
        By Turrning on Full Node mode you will download the entire TRU Block Chain.
        The current block count is ${ LUIX.getStateAt('blockCount') } and is approximately ${LUIX.getStateAt('chainSize')}.
        Thanks for participating in our Network!
        `;

        super.changed(props);  //Do this if you don't have 
    }

 


}