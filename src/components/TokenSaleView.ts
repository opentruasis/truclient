 
import  {FlexBox, Text, Image} from "luix";


import ITruToken from "../types/ITruToken";
import Paper from "../units/Paper";
import StyledButton from "../units/StyledButton";
import BaseURL from '../api/BaseURL';
 
export default class TokenSaleView extends Paper {

    constructor(props: any = null ) {
        super(props);
 
        this.style = {flexDirection: 'column', backgroundColor: "#eee", justifyContent: 'space-between',   flexShrink: 0,  padding: "5px"};

        var tkn = props as ITruToken;
        console.log("tkn sale view;::");
        console.log(tkn);
        const tknNameLabel = new Text(tkn.Name);
        tknNameLabel.style = {fontSize: "18px", fontWeight: 600};
        this.push(tknNameLabel);

        if(tkn.FileExt === "png" || tkn.FileExt === "PNG" || 
        tkn.FileExt === "jpg" || tkn.FileExt === "jpeg" || 
        tkn.FileExt === "JPG" || tkn.FileExt === "JPEG" || 
        tkn.FileExt === "gif" || tkn.FileExt === "GIF"){

            var firstImage = new Image();
            firstImage.style = {maxWidth: "200px"};
            firstImage.imgSource = BaseURL() + "/tokens/" +  tkn.ID + "." + tkn.FileExt;
            this.push(firstImage);
     
        }else{
 
            this.push(new Text(tkn.ID + "." + tkn.FileExt));
 
        }
        
        const btmContainer = new FlexBox();
        btmContainer.style = {flexDirection: 'column', marginTop: "15px"};
        const tknPriceBox = new  FlexBox();
        tknPriceBox.push(new Text("PRICE (TRU): "))
        tknPriceBox.push(new Text(tkn.SalePrice.toString()));
        btmContainer.push(tknPriceBox);

         
        var sellBtn = new StyledButton();
        sellBtn.innerText = "Buy Now";
        sellBtn.style = {padding: "2px", height: "none"};
        btmContainer.push(sellBtn);
        this.push(btmContainer);
 


    }

 
}     
