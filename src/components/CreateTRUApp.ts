import {LUIX }from 'luix';
 
 
import StyledButton from '../units/StyledButton';
 
import Paper from '../units/Paper';
 
import TruAppAPI from '../api/TruAppAPI';
import TruCrypto from '../api/TruCrypto';
import ShowCaseFileView from './ShowCaseFileView';
 
import ITruWallet from '../types/ITruWallet';
import {LongEntry, ProgressBar, Text, Label, Switch, Entry, IconButton, FlexBox}  from 'luix';



function buf2hex(buffer:any) { // buffer is an ArrayBuffer
    // create a byte array (Uint8Array) that we can use to read the array buffer
    const byteArray = new Uint8Array(buffer);
    
    // for each element, we want to get its two-digit hexadecimal representation
    const hexParts = [];
    for(let i = 0; i < byteArray.length; i++) {
      // convert value to hexadecimal
      const hex = byteArray[i].toString(16);
      
      // pad with zeros to length 2
      const paddedHex = ('00' + hex).slice(-2);
      
      // push to array
      hexParts.push(paddedHex);
    }
    
    // join all the hex values of the elements into a single string
    return hexParts.join('');
  }

 
  function _arrayBufferToBase64( buffer: any ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
   
    var len = bytes.byteLength;
 
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
  
    }

    return window.btoa( binary );
}


export default class CreateTRUApp extends Paper {

    constructor(props: any = null){
        super(props);
        this.style= {
            minHeight: "250px", 
            backgroundColor:"#eee",
             flexDirection: 'column',  
             justifyContent: 'space-between', 
             alignItems: 'center',
            padding: '5px'};
       

        const createHeader = new FlexBox();
        createHeader.style = {width: '100%', margin: "5px", justifyContent: 'space-between', alignItems: 'center'};
        var title = new Label("Create TRU App: ");
        title.style={fontSize: "18px"};
        createHeader.push(title);
  

        this.children.push(createHeader);

        
        this.appName.EntryLabel.Text = "App Name: ";
        this.appName.style = {margin: '5px'};
        this.push(this.appName)

        
        this.TruAppDescription.EntryLabel.Text = "Description: ";
        this.TruAppDescription.style = {margin: '5px'};
        this.push(this.TruAppDescription);

        
        this.gitRepoURLInputBox.EntryLabel.Text = "Git Public Repo URL: ";
        this.gitRepoURLInputBox.style = {margin: '5px'};
        this.push(this.gitRepoURLInputBox);


        

      this.ShowCaseFileEntry.EntryInput.onChange = (ev: Event) =>{
        console.log("ShowCase Files: ");
        console.log(LUIX.getStateAt("showCaseFiles") || []);
          
        var files = (ev.target  as HTMLInputElement).files;
        let showCaseFiles:any = [];
        for(var  i = 0; i< files.length; i++){
          showCaseFiles.push(files[i]);
        }

        var listOfFiles = LUIX.getStateAt("showCaseFiles") || [];
        listOfFiles.push(...showCaseFiles);
        console.log(showCaseFiles);
        this.changed(LUIX.postData("showCaseFiles", listOfFiles, false));
       
      }
      
 

      this.ShowCaseFileEntry.EntryInput.InputType = "file";
      this.ShowCaseFileEntry.EntryInput.Elem.setAttribute("accept", "image/*");
      this.ShowCaseFileEntry.EntryInput.Elem.setAttribute("multiple", "true");
      this.ShowCaseFileEntry.EntryLabel.Text = "Showcase Files:";
      this.ShowCaseFileEntry.style = {margin: '5px'};
      this.push(this.ShowCaseFileEntry);

      this.push(this.showCaseImages);




        this.JSFileInput.EntryInput.onChange = (ev: Event) =>{

            
            var files = (ev.target  as HTMLInputElement).files;
            
           
            this.programFile = files[0];

            var fr  = new FileReader();
             fr.onload=()=>{
                
               try{
                 this.TruAppSOURCECode =  _arrayBufferToBase64(fr.result as ArrayBuffer); 
                 this.FileHash.innerText = "HASH: " +  TruCrypto.HashBase64String(this.TruAppSOURCECode);
                 this.hash =  TruCrypto.HashBase64String(this.TruAppSOURCECode);
               }catch(e){
                 alert("Bad File format: ");
               }
            }
             fr.readAsArrayBuffer(this.programFile);
        };


        this.JSFileInput.EntryInput.InputType = "file";
        this.JSFileInput.EntryLabel.Text = "Program:";
        this.push(this.JSFileInput);

        this.push(this.FileHash);
 
        const priceReadout = new FlexBox();
        priceReadout.push(new Text("Publish Price (TRU): "));
        this.tokenTRUPriceLabel.style = {marginLeft: '5px'};
        priceReadout.push(this.tokenTRUPriceLabel);
        this.push(priceReadout);


        this.GenerateDAP.innerText = "Genenerate";
        this.GenerateDAP.onClick = async () =>{
            
           this.appGenUploadProgress.visible = true;

           console.log(LUIX.getStateAt("showCaseFiles") || [])
           var res =  await TruAppAPI.NewApp(
             {
              Program: this.programFile, 
              Name: this.appName.EntryInput.Value,
              Description: this.TruAppDescription.EntryText.value,
              ShowcaseFiles: LUIX.getStateAt("showCaseFiles") || [],
              GitRepoURL: this.gitRepoURLInputBox.EntryInput.Value,
              Hash: this.hash,
              CreatedBy: (LUIX.getStateAt('wallet') as ITruWallet).ID
            });

            this.appGenUploadProgress.visible = false;
           
        };



        this.push(this.GenerateDAP);
 

    }

    private NFTSwitch  = new Switch();

    
    private ShowCaseFileEntry  = new Entry();
 
    private showCaseImages: ShowCaseFileView = new ShowCaseFileView();

    private JSFileInput  = new Entry();

    private programFile: any = null;

    private FileHash  = new Text();

    private TruAppDescription  = new LongEntry();

    private qtyInputBox = new Entry();
    
    private appName = new Entry();

    private gitRepoURLInputBox = new Entry();
    
    private tokenTRUPriceLabel = new Text();

    private GenerateDAP: StyledButton = new StyledButton();

    private TruAppSOURCECode: string = null;
    
    private hash: string = "";

    private appGenUploadProgress = new ProgressBar();


    public changed(props: any = null){
       
        this.qtyInputBox.visible = !this.NFTSwitch.isOn;

        this.tokenTRUPriceLabel.Text = (1/LUIX.getStateAt("currentTRUPrice")).toFixed(4).toString();
 
        super.changed(props);
    }
 
  
}