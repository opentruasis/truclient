﻿import {Elem, Image} from 'luix';
 
 
export default class AppLoading extends Elem {

    constructor(props: any = null) {
        super("div", props);

    
        var loader = new Image();
        loader.imgSource = "/imgs/loading.gif";
        this.children.push(loader);
        this.visible = true;
        this.isFlexDisplay = true;
        this.style = {
            backgroundColor: "rgba(0,0,0,0.5)",
            height: "100%",
            width: "100%",
            display: "flex",
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "center",
            position: 'fixed'
        };
    }   

   
    public changed(props: any) {
        
        var { showLoader = false } = props || {}; 
        this.visible = showLoader;
        super.changed(props);  //Do this if you don't have 
    }

 


}