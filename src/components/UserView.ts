import {LUIX, FlexBox, Entry, Text} from 'luix';
 
 
 
import StyledButton from '../units/StyledButton';
import TruCrypto from '../api/TruCrypto';

import TruPushNotifications from '../api/TruPushNotifications';
import TruWalletAPI from '../api/TruWalletAPI';
import TruWallet from '../types/ITruWallet';

 
import LocalWalletAPI from '../api/LocalWalletAPI';
 
 import WalletImage from '../units/WalletImage';

export default class UserView extends FlexBox {

    constructor(props: any = null) {
        super(props);
       
        this.style = {
            display: 'flex', 
            flexDirection: 'row', 
            justifyContent: 'space-between',
            border: '1px black solid',
            borderRadius: '5px',
            padding: '5px',
            width:"250px",
            minWidth:"250px",
            margin : "5px",
        };

  

        const walletDetailContainer = new FlexBox();
        walletDetailContainer.style = {flexDirection: 'column', justifyContent:'space-around'};
        
        walletDetailContainer.push(this.WalletImage);
        walletDetailContainer.push(this.FullName);
        walletDetailContainer.push(this.Handle);

        this.children.push(walletDetailContainer);


        const actionContainer = new FlexBox();
        actionContainer.style = {flexDirection: 'column', alignItems:'flex-end'};
        
        this.SendTruBtn.style = {margin:"5px", width: "100px"};
        this.SendTruBtn.innerText = "Send TRU";
        this.SendTruBtn.onClick = this.SendTruTo.bind(this);
        actionContainer.push(this.SendTruBtn);

        // this.SendMsgBtn.style = {margin:"5px", width: "100px"};
        // this.SendMsgBtn.innerText = "Send Message";
        // actionContainer.push(this.SendMsgBtn);

        this.children.push(actionContainer);


        
        if(!props) return;

        this.truWallet = {...props["truWallet"]};

        if(!this.truWallet) return;

        this.FullName.Text = this.truWallet.FullName;
        this.Handle.Text = "@" + this.truWallet.Handle;
        this.WalletImage.src = this.truWallet.ProfilePicBase64;
        this.WalletImage.style = {maxWidth: "75px", maxHeight: "75px"};
      
 
    }
   
    private FullName  = new Text();
    private Handle  = new Text();

    private truWallet: TruWallet;

    private WalletImage: WalletImage = new WalletImage();
 
    private SendTruBtn: StyledButton = new StyledButton();
    private SendMsgBtn: StyledButton = new StyledButton();


    private SendTruTo(e: Event){
    
        //Send Tru To
        LUIX.post({'showSendTru': true, "sendTruTo": this.truWallet});
 
    }

    
}