 
import {LUIX, HamburgerIcon, Image, IconButton, FlexBox, Text} from 'luix';
import LocalChainV2 from '../api/LocalChainV2';
import LocalSuperNodeURL from '../api/LocalSuperNodeURL';
import SocketConnection from '../SocketConnection';
import NavBarMobileMenuBtn from '../units/NavBarMobileMenuBtn';
import NavButton from '../units/NavButton';
import Paper from '../units/Paper';
import SocketConnectionBtn from '../units/SocketConnectionBtn';
import SuperSelect from '../units/SuperSelect';
import ConnectivityError from './ConnectivityError';

export default class  Connectivity extends FlexBox{
    constructor(props: any = null){
        super(props);
 
        this.style={justifyContent: 'space-around', width: "100%", borderTop: "1px solid rgba(51,51,51,0.5)" };

     

       const topBar = new FlexBox();
       topBar.style = {justifyContent: 'space-around', width: "100%", alignItems: 'center', flexWrap:'wrap'};
       const socketConnectionBtn = new SocketConnectionBtn();
       socketConnectionBtn.onClick = this.connectToNetwork.bind(this);
       topBar.push(socketConnectionBtn);
       topBar.push(new SuperSelect());
       
       topBar.push(this.selectedSuper);
 
       this.children.push(topBar);


       this.connectToNetwork();

    }
 
 
    public ConnectedNodeUrl: string;
 
    private selectedSuper  = new Text();
   

    private async connectToNetwork() {
        console.log("COnnect to Network!!??");
       // LocalSuperNodeURL.AddUrl(this.MasterNodeUrl);
      //  LocalSuperNodeURL.AddUrl(this.MasterNodeUrlDev);
        
        
        if( SocketConnection ){
      
            if( SocketConnection.isConnected){
           
                SocketConnection.disconnect();

                return;
            }
        }

        let truasisNodes = LocalSuperNodeURL.GetUrls();
        console.log("Truasis Nodes..////");
        console.log(truasisNodes);
    


        SocketConnection.onConnected=()=>{
            LUIX.postData("isConnected", true);
            LocalChainV2.Init();
        };
        SocketConnection.onDisconnected =()=>{
            
            
           LUIX.postData("isConnected", false);
        };

        let trycount = 0;
  
        LUIX.postData("showLoader", true);
   
        let tryConnectURL: string;
        var selectedSuper = LUIX.getStateAt('selectedSuper');
        var randomIndex =  LUIX.getStateAt('selectedSuperIndex');
        while(!SocketConnection.isConnected){
            if(SocketConnection.canTryAgain){
                try{
                    //tryConnectURL = truasisNodes[Math.floor(Math.random()*truasisNodes.length)];
                     
                 
                    if(!selectedSuper){
                        randomIndex = Math.floor(Math.random()*truasisNodes.length);
                    }

                    tryConnectURL = truasisNodes[randomIndex];
                    
                    
                    console.log("Try Connect URL");
                    console.log(tryConnectURL);
                    
                    await SocketConnection.TryConnect(tryConnectURL);
                    trycount++;
                }catch(e){
                    
                   if(tryConnectURL != "https://localhost:44380" && tryConnectURL != "http://18.144.168.127:5001" ){
                       // Remove from list...
                       LocalSuperNodeURL.DeleteUrl(tryConnectURL);
                   }
                   trycount++;
                }
                if(trycount > 3){
                    break;
                }
            }
        }

        
  
         
        LUIX.postData("showLoader", false);
        if(SocketConnection.isConnected){
            this.ConnectedNodeUrl = tryConnectURL;
            LUIX.post({"selectedSuper": tryConnectURL, "selectedSuperIndex": randomIndex,  "superNodeUrls": truasisNodes});
        }else{
            LUIX.post({"selectedSuper": truasisNodes[randomIndex], "selectedSuperIndex": randomIndex, "superNodeUrls": truasisNodes});

            LUIX.post({'showModal': true, 'modalContent': new ConnectivityError()});

        }
        LUIX.postData("isConnected", SocketConnection.isConnected);
    }

  
    public changed(props:any = null){
        var  { selectedSuper = ""} = props || {};
        this.selectedSuper.Text = selectedSuper;
       
        super.changed(props);
    }

}