import LUIX from 'luix';
 
import Paper from '../units/Paper';
import {FlexBox, Label, ArrowDown, IconButton} from 'luix';
import TokenSaleView from './TokenSaleView';

import StyledButton from '../units/StyledButton';

import TokenSearchBar from '../units/TokenSearchBar';
import ToggleUpDwnBtn from '../units/ToggleUpDwnBtn';
import TruTokenAPI from '../api/TruTokenAPI';
import ITruToken from '../types/ITruToken';
import SocketConnection from '../SocketConnection';

export default class AvailTruasisTokens extends Paper {

    constructor(props: any = null){
        super(props);
        this.style= {justifyContent:'flex-start',  flexDirection: 'column', alignItems: 'center', maxHeight: "800px", overflowX: 'hidden'};
      
        const createHeader = new FlexBox();
        createHeader.style = {width: '100%', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap'};
        var title = new Label("TRU Tokens: ");

        title.style={fontSize: "18px", marginLeft: "5px"};
        createHeader.push(title);

        createHeader.push(new TokenSearchBar());

        const toggleAvailTokenView = new ToggleUpDwnBtn({initDown: false});
        toggleAvailTokenView.onClick = ()=>{
             this.ForSaleTokenContainer.visible = !this.ForSaleTokenContainer.visible;
        };
        createHeader.push(toggleAvailTokenView);
        

        this.push(createHeader);

        this.ForSaleTokenContainer.style = { position: 'relative', marginTop: "15", flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center'};
  
        this.push( this.ForSaleTokenContainer);

        SocketConnection.onConnectedInvokes.push(async ()=>{
            var tokens : ITruToken[] = await TruTokenAPI.TokensForSale();
            if(tokens){
                tokens.forEach(tkn => {
                    
                    
                    var tkncard = new TokenSaleView(tkn);
            
                    this.ForSaleTokenContainer.push(tkncard)
                });
            
                this.changed(props);
            }
        });
       

    }

    private ForSaleTokenContainer  = new FlexBox();

 
  
}