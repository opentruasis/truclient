import {LUIX, FlexBox, Entry }  from 'luix';
 
import StyledButton from '../units/StyledButton';
import TruCrypto from '../api/TruCrypto';

import TruPushNotifications from '../api/TruPushNotifications';
import TruWalletAPI from '../api/TruWalletAPI';
import TruWallet from '../types/ITruWallet';
 
import LocalWalletAPI from '../api/LocalWalletAPI';
 
export default class NewWalletForm extends FlexBox {

    constructor(props: any = null) {
        super(props);
       
        this.style = { display: 'flex', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', overflowY: "auto", width:"100%"};
        
        this.FullNameEntry.EntryLabel.Text = "Enter Full Name:";
        this.FullNameEntry.style = {margin: "5px"};
        this.children.push(this.FullNameEntry);
        this.HandleEntry.EntryLabel.Text = "Enter Handle: @";
        this.HandleEntry.style = {margin: "5px"};
        this.children.push(this.HandleEntry);

        this.FullNameEntry.EntryInput.onChange = this.onDataChanged.bind(this);
        this.HandleEntry.EntryInput.onChange = this.onDataChanged.bind(this);
        
        this.GetNewWalletBtn.innerText = "Open New Wallet";
        this.GetNewWalletBtn.disabled = true;
        this.GetNewWalletBtn.onClick = async ()=>{
           await this.newWalletSubmit();
        }
        this.children.push(this.GetNewWalletBtn);

    }
   
    public FullNameEntry = new Entry();

    public HandleEntry  = new Entry();

    private GetNewWalletBtn : StyledButton = new StyledButton();
    

    public changed(props: any): void {
        let _loadedWallet = LUIX.getStateAt('wallet');
 
        if(!_loadedWallet){
            this.visible = true;
        }else{
            this.visible = false;
        }

        if(this.FullNameEntry.EntryInput.Value.length < 1 && this.HandleEntry.EntryInput.Value.length  < 1 ){
            this.GetNewWalletBtn.disabled = true;
        }
       
        super.changed(props);
    }


    private onDataChanged(e:Event){
 
        if(this.FullNameEntry.EntryInput.Value.length > 0 && this.HandleEntry.EntryInput.Value.length  > 0){
            this.GetNewWalletBtn.disabled = false;
        }

    }

    private async newWalletSubmit(){
       try{
        var newWalletKeys = await TruCrypto.CreateNewWalletKeys();
 
        var enteredName = this.FullNameEntry.EntryInput.Value;
        var enteredHandle =  this.HandleEntry.EntryInput.Value [0] == "@" ? this.HandleEntry.EntryInput.Value.substring(1): this.HandleEntry.EntryInput.Value;
        var newPushNotifToken = TruPushNotifications.GetNewPushToken();
        this.FullNameEntry.EntryInput.Value = "";
        this.HandleEntry.EntryInput.Value = "";

        var newWallet: TruWallet = {
            Keys: newWalletKeys,
            FullName: enteredName,
            Handle: enteredHandle,
            PushNotifToken: newPushNotifToken,
            Balance: 0,
            ProfilePicBase64: ""
        };

        var result = await TruWalletAPI.NewWallet(newWallet);
        
        if(result){
            newWallet.ID = result.ID;
            
            LocalWalletAPI.SetWallet(newWallet);
            this.visible = false;

            LUIX.postData("wallet", newWallet);
        
        }
        //
    }catch(e){
        alert(e);
    }
      
    }


    

}