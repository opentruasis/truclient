import { LUIX, FlexBox, Entry, Text, IconButton, RefreshIcon} from 'luix';
  
 
import StyledButton from '../units/StyledButton';
import WalletImage from '../units/WalletImage';
 
import TruWalletAPI from '../api/TruWalletAPI';
import TruWallet from '../types/ITruWallet';
 
import LocalWalletAPI from '../api/LocalWalletAPI';
import ViewMyTokensBtn from '../units/ViewMyTokensBtn';
 
export default class WalletInfo extends FlexBox {

    constructor(props: any = null) {
        super(props);
   
        this.style = { display: 'flex', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center',   width:"100%"};
        
        this._loadedWallet = LUIX.getStateAt('wallet');
 
        this.visible = false;

        {
            this.WalletImage.src = "/imgs/loading.gif";
            this.children.push(this.WalletImage);
        }
 
        {
            this.walletDataList = new FlexBox();
            this.walletDataList.style = {fontSize: "25px", flexWrap: 'wrap'};
 
            this.walletDataList.style = {flexDirection: 'column', alignItems: 'flex-start'};

            this.refreshButton.onClick = ()=>{this.getMyWallet()}  
            this.refreshButton.innerText = "";
            this.refreshButton.children.push(new RefreshIcon());
            this.refreshButton.style = {height: '40px'}

            this.walletDataList.push(this.refreshButton);

            {
                const nameData = new FlexBox();
                nameData.style = {margin: "10px"};
                nameData.push(new Text("Name:"));
                this.WalletFullName.style = {marginLeft: "10px"};
                nameData.push(this.WalletFullName);
                this.walletDataList.push(nameData);
            }
            {
                
                this.walletDataList.push( this.WalletHandel );
            }
            {
                const balanceData = new FlexBox();
                balanceData.style = {margin: "10px"};
                balanceData.push(new Text("TRU:"));
                this.WalletBalance.style = {marginLeft: "10px", fontWeight: 600};
                balanceData.push(this.WalletBalance);
                this.walletDataList.push(balanceData);
            }

            {

                const viewMyTokensBtn = new ViewMyTokensBtn();
                viewMyTokensBtn.innerText = "My Tokens";
                viewMyTokensBtn.style={width: "120px"};
                
                viewMyTokensBtn.onClick = ()=>{
                    LUIX.post({'showMyTokenBtn':false, 'showMyTokens': true});
                };
                this.walletDataList.push(viewMyTokensBtn);
            }
            this.children.push(this.walletDataList);
        }


        
        {
            this.editDataList = new FlexBox();
            this.editDataList.visible = false;
            this.editDataList.style = {flexDirection: 'column', alignItems: 'flex-start'};
            {
                this.FullNameEntry.EntryLabel.Text = "Name:";
                this.FullNameEntry.style = {margin: "5px"};
                this.editDataList.push(this.FullNameEntry);
            }

            {
                this.HandleEntry.EntryLabel.Text = "@";
                this.HandleEntry.style = {margin: "5px"};
                this.editDataList.push(this.HandleEntry);
            }

            {
                this.ImageFileInput.EntryInput.InputType = "file";
                this.ImageFileInput.EntryLabel.Text = "Profile Pic:";
                this.ImageFileInput.style = {margin: "5px"};
                this.editDataList.push(this.ImageFileInput);
            }
           
            this.children.push(this.editDataList);
        }

        this.FullNameEntry.EntryInput.onChange = this.onDataChanged.bind(this);
        this.HandleEntry.EntryInput.onChange = this.onDataChanged.bind(this);
        this.ImageFileInput.EntryInput.onChange = this.fileDataChanged.bind(this);
 
        const actionBtns = new FlexBox();
        actionBtns.style = {flexDirection: 'column'};

        this.editTruasisWalletBtn = new StyledButton({ onClick: this.editWallet.bind(this) });
        this.editTruasisWalletBtn.innerText = "Edit Wallet";
        actionBtns.push(this.editTruasisWalletBtn);

        const walletExptBtns = new FlexBox();
        walletExptBtns.style = {flexDirection: 'column', alignItems: 'center'};
       
        
        
        const exportPrivKeyBtn = new StyledButton({ onClick: this.exportPrivKey.bind(this) });
        exportPrivKeyBtn.innerText = "PrivKey";


        const exportPubKeyBtn = new StyledButton({ onClick: this.exportPubKey.bind(this) });
        exportPubKeyBtn.innerText = "PubKey";


        const btnRowsBtns = new FlexBox();
        btnRowsBtns.push(exportPubKeyBtn);
        btnRowsBtns.push(exportPrivKeyBtn);

     
        walletExptBtns.push(btnRowsBtns);
     
        actionBtns.push(walletExptBtns)

        const exportSuperWalletJsonBtn = new StyledButton({ onClick: this.exportSuperWalletJSON.bind(this) });
        exportSuperWalletJsonBtn.innerText = "SuperNode.json";

        actionBtns.push(exportSuperWalletJsonBtn);

        const loadNewWallet = new StyledButton({ onClick: this.loadNewWallet.bind(this) });
        loadNewWallet.innerText = "Close Wallet";
        actionBtns.push(loadNewWallet);


        this.children.push(actionBtns);
 
    }
    

         
    private _loadedWallet : TruWallet;

    public WalletImage: WalletImage = new WalletImage();

    public WalletFullName  = new Text();

    public WalletHandel  = new Text();

    public WalletBalance  = new Text();

    public FullNameEntry  = new Entry();

    public HandleEntry  = new Entry();

    public ImageFileInput  = new Entry();

    private refreshButton : IconButton = new IconButton();

    private profilePicUpdateBase64Enc: string = "";
 
    public walletDataList  = new FlexBox();

    public editDataList  = new FlexBox();
 
    public editTruasisWalletBtn: StyledButton;


    private async getMyWallet(){
        var myLocalWallet = LocalWalletAPI.GetWallet();
        console.log("My Local Wallet: ");
        console.log(myLocalWallet);
        var myWallet =  await TruWalletAPI.WalletById(myLocalWallet.ID);
        console.log("Get My Wallet?....");
        console.log(myWallet);
        myWallet.Keys.PrivKey = myLocalWallet.Keys.PrivKey;
        LUIX.postData('wallet', myWallet);
        LocalWalletAPI.SetWallet(myWallet);
    }

    public changed(props: any): void {
 
        this._loadedWallet = LUIX.getStateAt('wallet');
 
        if(this._loadedWallet){
            
            this.visible = true;
            if(this._loadedWallet.ProfilePicBase64){
                this.WalletImage.src =  this._loadedWallet.ProfilePicBase64;
                this.profilePicUpdateBase64Enc =  this._loadedWallet.ProfilePicBase64;
            }else{
                this.WalletImage.src =  "";
                this.profilePicUpdateBase64Enc =  "";
            }
            this.WalletFullName.Text = this._loadedWallet.FullName;
            this.WalletHandel.Text = "@" + this._loadedWallet.Handle;
            this.WalletBalance.Text = this._loadedWallet.Balance.toFixed(4);
        }else{
            this.visible = false;
        }

        if(this.editMode){
            this.editTruasisWalletBtn.innerText = "Save Info";
        }else{
            this.editTruasisWalletBtn.innerText = "Edit Wallet";
        }

        super.changed(props);
    }


    private editMode: boolean = false;

    private async editWallet(){

        this.editMode = !this.editMode;


        this.walletDataList.visible = !this.editMode;
        this.editDataList.visible = this.editMode;
        this.WalletImage.visible = !this.editMode;

        if(this.editMode){
            this.FullNameEntry.EntryInput.Value =  this.WalletFullName.Text;
            this.HandleEntry.EntryInput.Value = this.WalletHandel.Text[0] == "@" ? this.WalletHandel.Text.substring(1) : this.WalletHandel.Text;

            if(this.FullNameEntry.EntryInput.Value.length > 0 && this.HandleEntry.EntryInput.Value.length  > 0){
                this.editTruasisWalletBtn.disabled = false;
            }else{
                this.editTruasisWalletBtn.disabled = true;
            }
  
        }else{

            ///Save the data..
            var saveWallet : TruWallet = LUIX.getStateAt('wallet');
            saveWallet.FullName = this.FullNameEntry.EntryInput.Value;
            saveWallet.Handle =  this.HandleEntry.EntryInput.Value [0] == "@" ? this.HandleEntry.EntryInput.Value.substring(1): this.HandleEntry.EntryInput.Value ;
            saveWallet.ProfilePicBase64 = this.profilePicUpdateBase64Enc;
            
            this.ImageFileInput.EntryInput.Value = null;
            try{
                var savedWallet =  await TruWalletAPI.UpdateWallet(saveWallet);
                savedWallet.Keys.PrivKey = saveWallet.Keys.PrivKey;
                LUIX.postData('wallet', savedWallet);
                LocalWalletAPI.SetWallet(savedWallet);
   
            }catch(e){
                 
        
            }
        }
        this.changed(this.props);

    }

    private fileDataChanged(e:Event){
        var newImage = this.ImageFileInput.EntryInput.files[0];

        var reader = new FileReader();
        reader.onload = (event) => {
               //Trim image..
            const img = new Image();
            img.src = event.target.result.toString();
            img.onload = (imgdata: Event)=>{
 
                const canvas = document.createElement('canvas');
                const ctx = canvas.getContext('2d');
                // Set width and height
                canvas.width = 200;
                canvas.height = canvas.width*img.naturalHeight/img.naturalWidth;
                // Draw the image
                ctx.drawImage(img, 0, 0,  img.naturalWidth, img.naturalHeight, 0, 0, canvas.width, canvas.height );

                //Compare compression!!::))))
                const origImageEnc = event.target.result.toString();
                const compressedImgEnc = canvas.toDataURL('image/jpeg');
 
                if(compressedImgEnc > origImageEnc){
                    this.profilePicUpdateBase64Enc = origImageEnc;
                }else{
                    this.profilePicUpdateBase64Enc = compressedImgEnc;
                }
   
            };
 
        };

         reader.readAsDataURL(newImage);
    }

    private onDataChanged(e:Event){
 
        if(this.FullNameEntry.EntryInput.Value.length > 0 && this.HandleEntry.EntryInput.Value.length  > 0){
            this.editTruasisWalletBtn.disabled = false;
        }else{
            this.editTruasisWalletBtn.disabled = true;
        }

    }

    private exportPubKey(){
        let textContent = "data:text/plain;charset=utf-8,";
        textContent += this._loadedWallet.Keys.PubKey;
        
        var encodedUri = encodeURI(textContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri); 
        link.setAttribute("download", "TruWalletPubKey-" + Date.now() + ".txt");
        document.body.appendChild(link); // Required for FF

        link.click(); // This will download the data file named "my_data.csv".
        document.body.removeChild(link);
        
    }

    private exportPrivKey(){
 
        let textContent = "data:text/plain;charset=utf-8,";
        textContent += this._loadedWallet.Keys.PrivKey;
        
        var encodedUri = encodeURI(textContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri); 
        link.setAttribute("download", "TruWalletPrivKey-" + Date.now() + ".txt");
        document.body.appendChild(link); // Required for FF

        link.click(); // This will download the data file named "my_data.csv".
        document.body.removeChild(link);
    }


    private exportSuperWalletJSON(){
        let textContent = "data:text/plain;charset=utf-8,";
        textContent += JSON.stringify(this._loadedWallet);
        
        var encodedUri = encodeURI(textContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri); 
        link.setAttribute("download", "SuperNode.json");
        document.body.appendChild(link); // Required for FF

        link.click(); // This will download the data file named "my_data.csv".
        document.body.removeChild(link);

    }


    private loadNewWallet(){
        this.walletDataList.visible = true;
        this.editDataList.visible = false;
        this.editMode = false;
        LUIX.removeData("wallet");
        LocalWalletAPI.DeleteKey("wallet");
    }
 

}