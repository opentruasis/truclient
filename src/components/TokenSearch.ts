
import {LUIX, FlexBox} from 'luix';
 
 

import TruWallet from '../types/ITruWallet';

import UserView from './UserView';

export default class TokenSearch extends FlexBox {

    constructor(props: any = null) {
        super(props);
 
        this.style = { overflow: "hidden", width:"100%"};
        
       
        const searchView = new FlexBox();
        searchView.style = {flexDirection: 'column', justifyContent: 'space-around',  width: "100%", overflow:'auto' };
 
        this.SearchResultsBox.style = {flexDirection: 'row',  flexWrap: 'nowrap', justifyContent: 'flex-start'};
        searchView.push( this.SearchResultsBox);
        searchView.onWheel = (ev: WheelEvent)=>{
            searchView.scrollLeft(ev.deltaY);
            ev.stopPropagation();
            ev.preventDefault();
        };
 
        this.children.push(searchView);
 
    }
   
 
    public SearchResultsBox = new FlexBox();
 
   
    public changed(props: any = null){
 
        this.SearchResultsBox.clear();

        var listWalletsFound: TruWallet[] = LUIX.getStateAt('searchedListOfWallets');
        
        
        if(listWalletsFound){
            listWalletsFound.forEach((w)=>{
                
                
                const userView = new UserView({truWallet: w})
                this.SearchResultsBox.push(userView);
            });
        }

        if(this.SearchResultsBox.children.length > 0){
            this.visible = true;
        }else{
            this.visible = false;
        }
        
        
        super.changed(props);
    }

}