import {FlexBox, Image,Text, Entry, LUIX} from 'luix';

 
import Paper from "../units/Paper";
import StyledButton from "../units/StyledButton";
import BaseURL from '../api/BaseURL';
import ITruToken from "../types/ITruToken";
import TruTokenAPI from "../api/TruTokenAPI";

export default class TokenView extends Paper {

    constructor(props: any = null ) {
        super(props);
 
        this.style = {flexDirection: 'column', backgroundColor: "#eee",   flexShrink: 0, padding: "5px", justifyContent: 'space-between', maxWidth: "210px"};

   
        var tkn = props as ITruToken;

        const tknNameLabel = new Text(tkn.Name);
        tknNameLabel.style = {fontSize: "18px", fontWeight: 600};
        this.push(tknNameLabel);

        if(tkn.FileExt === "png" || tkn.FileExt === "PNG" || 
            tkn.FileExt === "jpg" || tkn.FileExt === "jpeg" || 
            tkn.FileExt === "JPG" || tkn.FileExt === "JPEG" || 
            tkn.FileExt === "gif" || tkn.FileExt === "GIF"){

            var firstImage = new Image();
            firstImage.style = {maxWidth: "200px"};
            firstImage.imgSource = BaseURL() + "/tokens/" +  tkn.ID + "." + tkn.FileExt;
            this.push(firstImage);

        }else{
            this.push(new Text(tkn.ID + "." + tkn.FileExt));
        }
        
        const btnContainer = new FlexBox();

        btnContainer.style = {flexDirection: 'column'};
        
        var downloadBtn = new StyledButton();
        downloadBtn.innerText = "Download";
        downloadBtn.style = {padding: "2px", height: "none"};
        downloadBtn.onClick = async ()=>{
           await TruTokenAPI.DownloadToken(tkn);
        };

        btnContainer.push(downloadBtn)
          
        const sellPriceEntry  = new Entry();
        sellPriceEntry.visible = false;
        sellPriceEntry.EntryLabel.Text = "Set Price (TRU): ";
        sellPriceEntry.EntryInput.InputType = 'number';
        sellPriceEntry.EntryInput.style = {width: "75px"};
        btnContainer.push(sellPriceEntry);
 
        if(!tkn.ForSale){

            const doSellOrCancelContainer = new FlexBox();
            var cancelSellBtn = new StyledButton();
            var doSellBtn = new StyledButton();

            var sellBtn = new StyledButton();
            sellBtn.innerText = "Sell Token";
            sellBtn.style = {padding: "2px", height: "none"};
            sellBtn.onClick = async () => {
                if(!sellPriceEntry.visible) {
                    sellPriceEntry.visible = true;
                    cancelSellBtn.visible = true;
                    doSellOrCancelContainer.visible = true;
                    sellBtn.visible = false;
                    this.changed(props);
                } 
            };
    
            btnContainer.push(sellBtn)

        
            doSellOrCancelContainer.style = {justifyContent: 'space-around'};
            doSellOrCancelContainer.visible = false;

            cancelSellBtn.innerText = "Cancel";
            cancelSellBtn.style = { margin: "5px", height: "none", width: "40%"};
            cancelSellBtn.onClick = async ()=>{
                    sellPriceEntry.visible = false;
                    sellBtn.visible = true;
                    doSellOrCancelContainer.visible = false;
        
                    this.changed(props);
            };
    
            doSellOrCancelContainer.push(cancelSellBtn);

            doSellBtn.innerText = "Sell";
            doSellBtn.style = { margin: "5px", height: "none", width: "40%"};
            doSellBtn.onClick = async ()=>{
                    sellPriceEntry.visible = false;
                    sellBtn.visible = true;
                    doSellOrCancelContainer.visible = false;
             
                    tkn.SalePrice = parseFloat(sellPriceEntry.EntryInput.Value);
                    console.log("Sale PRICE::::::");
                    console.log(tkn.SalePrice);

                    var saleTkn = await TruTokenAPI.SellToken(tkn) as ITruToken;
                    if(saleTkn != null){
                        tkn.ForSale = true;
                        sellBtn.visible = false;
                        this.changed(props);
                    }
            };
            
            doSellOrCancelContainer.push(doSellBtn);

            btnContainer.push(doSellOrCancelContainer)
        }
 
        this.push(btnContainer);
    }
    

    public changed(props: any = null)
    {
        
        console.log("Token View changed..");

        super.changed(props);
    }
     
}     
