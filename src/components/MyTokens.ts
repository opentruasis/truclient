 
import {FlexBox, IconButton, LUIX, RefreshIcon} from 'luix';
 
import TokenView from './TokenView';
import CloseIconBtn from '../units/CloseIconBtn';
import ITruToken from '../types/ITruToken';
import TruTokenAPI from '../api/TruTokenAPI';
import ITruWallet from '../types/ITruWallet';
import SocketConnection from '../SocketConnection';

export default class MyTokens extends FlexBox {

    constructor(props: any = null){
        super(props);
        this.style= {width: '100%',   flexDirection: 'column', borderBottom: "1px solid black"};
        LUIX.postData('showMyTokens', false);
        this.visible = false;
        

        const containerHeader = new FlexBox();
        containerHeader.style = {flexDirection: 'row', justifyContent: 'space-between',  width: "100%"};
 
  
        const refreshMyTokens: IconButton = new IconButton();
        refreshMyTokens.innerText = "";
        refreshMyTokens.children.push(new RefreshIcon());
        refreshMyTokens.style = {height: '40px'}

        refreshMyTokens.onClick = async ()=>{
           await this.loadMyTokens();
        };
 
        containerHeader.push(refreshMyTokens);


        const closeMyTokenContainer = new CloseIconBtn();
  
        closeMyTokenContainer.onClick = ()=>{
            LUIX.post({'showMyTokenBtn':true, 'showMyTokens': false});
        };
    

        containerHeader.push(closeMyTokenContainer);

       
        this.children.push(containerHeader);

        const myTokensContainerOuter = new FlexBox();
        myTokensContainerOuter.style = {flexDirection: 'column', justifyContent: 'space-around',  width: "100%", overflow:'auto' };
 
        this.MyTokenContainer.style = {flexDirection: 'row',  flexWrap: 'nowrap', justifyContent: 'flex-start'};

        myTokensContainerOuter.push( this.MyTokenContainer);
        myTokensContainerOuter.onWheel = (ev: WheelEvent)=>{
            myTokensContainerOuter.scrollLeft(ev.deltaY);
            ev.stopPropagation();
            ev.preventDefault();
        };

      
        this.children.push(myTokensContainerOuter);

        SocketConnection.onConnectedInvokes.push(()=>{
            this.loadMyTokens();
        });
    
    }

    private MyTokenContainer = new FlexBox();
 
    private async loadMyTokens(){
        var wallet = LUIX.getStateAt("wallet") as ITruWallet;
        if(wallet){
 
            var tokens : {[key: string]: ITruToken} = await TruTokenAPI.WalletTokens(wallet.ID);
            LUIX.postData("myTokenCount", Object.keys(tokens).length);
            Object.values(tokens).forEach(tkn => {
              
                var tkncard = new TokenView(tkn);
        
                this.MyTokenContainer.push(tkncard)
                this.MyTokensList[tkn.ID] = tkncard;
               
            });

           
        }
    }

    private MyTokensList: {[id: string]: TokenView} = {};


    public changed(props: any){
        console.log("MY TOKENS >>>>")
        
        var showMyTokens = LUIX.getStateAt('showMyTokens');
        
        showMyTokens? this.visible = true : this.visible = false;
 
     
        super.changed(props);
    }
 
  
}