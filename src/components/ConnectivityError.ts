import {Elem, FlexBox, Image, Text, LUIX} from 'luix';
import ChainLoader from '../api/ChainLoader';
import CloseIconBtn from '../units/CloseIconBtn';
import StyledButton from '../units/StyledButton';
 
 
export default class ConnectivityError extends FlexBox {

    constructor(props: any = null) {
        super(props);

        this.style = {flexDirection: 'column', justifyContent: 'cetner'};

        const connectivityError = new FlexBox();
        connectivityError.style={width: "100%", justifyContent: 'space-between'};
        const headerLabel = new Text();
        headerLabel.Text = "Truasis Network Failure: "
        headerLabel.style = { "fontSize": "20px" , marginLeft: "10px"};

        const clearSearch = new CloseIconBtn();
        clearSearch.onClick = ()=>{
            ChainLoader.SetChainLoader(false);
            LUIX.post({'showModal': false, 'modalContent': null }, true);
        };

        connectivityError.push(headerLabel);
        connectivityError.push(clearSearch);

        this.children.push(connectivityError);

  
        var notificationText: Text = new Text();
        notificationText.Text = `
        Couln't connect right now. Try again later.\r\n\r\nMake sure you're connected to the internet.
        `;

        this.children.push(notificationText);
 
    }   
 
 
    public changed(props: any) {
 
        super.changed(props);  //Do this if you don't have 
    }

 


}