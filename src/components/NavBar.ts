 
import {LUIX, HamburgerIcon, Image, IconButton, FlexBox, Div} from 'luix';
import NavBarMobileMenuBtn from '../units/NavBarMobileMenuBtn';
import NavButton from '../units/NavButton';
import Paper from '../units/Paper';

export default class  NavBar extends FlexBox{
    constructor(props: any = null){
        super(props);
 
        this.style={justifyContent: 'space-around', width: "100%", };

        const mainBar  = new FlexBox();
        mainBar.style = {width: '100%', justifyContent: 'space-between', alignItems:'center'};

        const logo = new Image();
        logo.onClick = ()=>{LUIX.GoTo("/");};
        logo.imgSource = "/imgs/truIcon1.svg";
        logo.style = {width: "50px", height: "50px", marginLeft: "5px"};
        mainBar.push(logo);

 
        this.desktopLinkContainer.style={flexGrow: 1};
        this.WalletNavButton.innerText = "Wallet";
        this.WalletNavButton.style = {width: "150px"};
        this.WalletNavButton.onClick = ()=>{LUIX.GoTo("/");};
        this.desktopLinkContainer.push( this.WalletNavButton);

      
        this.ChainNavButton.innerText = "Chain";
        this.ChainNavButton.style = {width: "150px"};
        this.ChainNavButton.onClick = ()=>{LUIX.GoTo("/chain");};
        this.desktopLinkContainer.push( this.ChainNavButton);

        this.ExchangeNavButton.innerText = "Exchange";
        this.ExchangeNavButton.style = {width: "150px"};
        this.ExchangeNavButton.onClick = ()=>{LUIX.GoTo("/exchange");};
        this.desktopLinkContainer.push( this.ExchangeNavButton);

        this.CatalogNavButton.innerText = "Truasis";
        this.CatalogNavButton.style = {width: "150px"};
        this.CatalogNavButton.onClick = ()=>{LUIX.GoTo("/truasis");};
        this.desktopLinkContainer.push( this.CatalogNavButton);

        this.DevNavButton.innerText = "Create";
        this.DevNavButton.style = {width: "150px"};
        this.DevNavButton.onClick = ()=>{LUIX.GoTo("/create");};
        this.desktopLinkContainer.push( this.DevNavButton);

        mainBar.push(this.desktopLinkContainer);

 
        this.mobileNaveBtn.onClick =  this.toggleMobileMenu.bind(this);  

        mainBar.push(this.mobileNaveBtn);

        this.WalletNavButtonMobile.innerText = "Wallet";
        this.WalletNavButtonMobile.style = {width: "150px", marginTop: "25px"};
        this.WalletNavButtonMobile.onClick = ()=>{LUIX.GoTo("/");};
      
        this.ChainNavButtonMobile.innerText = "Chain";
        this.ChainNavButtonMobile.style = {width: "150px"};
        this.ChainNavButtonMobile.onClick = ()=>{LUIX.GoTo("/chain");};
       

        this.ExchangeNavButtonMobile.innerText = "Exchange";
        this.ExchangeNavButtonMobile.style = {width: "150px"};
        this.ExchangeNavButtonMobile.onClick = ()=>{LUIX.GoTo("/exchange"); };
       

        this.CatalogNavButtonMobile.innerText = "Truasis";
        this.CatalogNavButtonMobile.style = {width: "150px"};
        this.CatalogNavButtonMobile.onClick = ()=>{LUIX.GoTo("/truasis");};
      

        this.DevNavButtonMobile.innerText = "Create";
        this.DevNavButtonMobile.style = {width: "150px"};
        this.DevNavButtonMobile.onClick = ()=>{LUIX.GoTo("/create"); };
      

        this.mobileMenu.push(this.WalletNavButtonMobile);
        this.mobileMenu.push(this.ChainNavButtonMobile);
        this.mobileMenu.push(this.ExchangeNavButtonMobile);
        this.mobileMenu.push(this.CatalogNavButtonMobile);
        this.mobileMenu.push(this.DevNavButtonMobile);
        this.mobileMenu.Elem.onclick = (e: any)=>{  
            ///Dont allow click through.
            
            
            e.stopPropagation();
        };
        this.mobileMenu.style = {
            position: 'absolute',
            flexDirection: 'column',
            alignItems: 'center',
            top: "70px",
            right:"5px",
            width: "200px",
            backgroundColor: "#eee",
            zIndex: 999
        };

        this.mobileMenuBox.visible = false;
        this.mobileMenuBox.style = {position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,  backgroundColor: "rgba(0,0,0,0.5)", zIndex: 998};
        this.mobileMenuBox.onClick = this.toggleMobileMenu.bind(this);
        this.mobileMenuBox.push(this.mobileMenu);

        mainBar.push(this.mobileMenuBox);

        this.push(mainBar);

    }

    private desktopLinkContainer = new FlexBox();

    private WalletNavButton:NavButton = new NavButton();
    private ChainNavButton:NavButton = new NavButton();
    private ExchangeNavButton:NavButton = new NavButton();
    private CatalogNavButton:NavButton = new NavButton();
    private DevNavButton:NavButton = new NavButton();

   

    private mobileNaveBtn: NavBarMobileMenuBtn = new NavBarMobileMenuBtn();
    private mobileMenu: Paper = new Paper();

    private mobileMenuBox = new Div();

    private WalletNavButtonMobile:NavButton = new NavButton();
    private ChainNavButtonMobile:NavButton = new NavButton();
    private ExchangeNavButtonMobile:NavButton = new NavButton();
    private CatalogNavButtonMobile:NavButton = new NavButton();
    private DevNavButtonMobile:NavButton = new NavButton();


    private toggleMobileMenu(e: Event){
    
        var showMobileMenu = false;
        if(this.mobileMenuBox.visible ){
            this.mobileNaveBtn.toggle = true;
        }else{
            showMobileMenu = true;
            this.mobileNaveBtn.toggle = false;
        }
        LUIX.postData("showMobileMenu", showMobileMenu);
    }

    public changed(props:any = null){
        var { currentRoute = {}, screenWidth=0, screenHeight=0} = props||{};
 
        if(screenWidth<830){
            this.desktopLinkContainer.visible = false;
            this.mobileNaveBtn.visible = true;
        }else{
            this.desktopLinkContainer.visible = true;
            this.mobileNaveBtn.visible = false;
        }

        if(LUIX.getStateAt("showMobileMenu")){
            this.mobileMenuBox.visible = true;
        }else{
            this.mobileMenuBox.visible = false;
        }


       
        switch(currentRoute.path){
            case "/":
                this.WalletNavButton.addClass("active");
                this.ChainNavButton.removeClass("active");
                this.ExchangeNavButton.removeClass("active");
                this.CatalogNavButton.removeClass("active");
                this.DevNavButton.removeClass("active");
                this.WalletNavButtonMobile.addClass("active");
                this.ChainNavButtonMobile.removeClass("active");
                this.ExchangeNavButtonMobile.removeClass("active");
                this.CatalogNavButtonMobile.removeClass("active");
                this.DevNavButtonMobile.removeClass("active");
            break;
            case "/chain":
                this.WalletNavButton.removeClass("active");
                this.ChainNavButton.addClass("active");
                this.ExchangeNavButton.removeClass("active");
                this.CatalogNavButton.removeClass("active");
                this.DevNavButton.removeClass("active");
                this.WalletNavButtonMobile.removeClass("active");
                this.ChainNavButtonMobile.addClass("active");
                this.ExchangeNavButtonMobile.removeClass("active");
                this.CatalogNavButtonMobile.removeClass("active");
                this.DevNavButtonMobile.removeClass("active");
            break;
            case "/exchange":
                this.WalletNavButton.removeClass("active");
                this.ChainNavButton.removeClass("active");
                this.ExchangeNavButton.addClass("active");
                this.CatalogNavButton.removeClass("active");
                this.DevNavButton.removeClass("active");
                this.WalletNavButtonMobile.removeClass("active");
                this.ChainNavButtonMobile.removeClass("active");
                this.ExchangeNavButtonMobile.addClass("active");
                this.CatalogNavButtonMobile.removeClass("active");
                this.DevNavButtonMobile.removeClass("active");
            break;
            case "/truasis":
                this.WalletNavButton.removeClass("active");
                this.ChainNavButton.removeClass("active");
                this.ExchangeNavButton.removeClass("active");
                this.CatalogNavButton.addClass("active");
                this.DevNavButton.removeClass("active");
                this.WalletNavButtonMobile.removeClass("active");
                this.ChainNavButtonMobile.removeClass("active");
                this.ExchangeNavButtonMobile.removeClass("active");
                this.CatalogNavButtonMobile.addClass("active");
                this.DevNavButtonMobile.removeClass("active");
            break;
            case "/create":
                this.WalletNavButton.removeClass("active");
                this.ChainNavButton.removeClass("active");
                this.ExchangeNavButton.removeClass("active");
                this.CatalogNavButton.removeClass("active");
                this.DevNavButton.addClass("active");
                this.WalletNavButtonMobile.removeClass("active");
                this.ChainNavButtonMobile.removeClass("active");
                this.ExchangeNavButtonMobile.removeClass("active");
                this.CatalogNavButtonMobile.removeClass("active");
                this.DevNavButtonMobile.addClass("active");
            break;
        }


        super.changed(props);
    }

}