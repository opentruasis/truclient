﻿import {Elem,Text, LUIX, FlexBox, IconButton, Switch, RefreshIcon} from 'luix';
 
import TruBlock from './TruBlock';
 
import ITruWallet from '../types/ITruWallet';
 
import ToggleUpDwnBtn from '../units/ToggleUpDwnBtn';
import ChainLoader from '../api/ChainLoader';
import ConfirmBecomeFullNode from './ConfirmBecomeFullNode';
import LocalChainV2 from '../api/LocalChainV2';
import ConfirmBecomeLightNode from './ConfirmBecomeLightNode';
import TruLedgerAPI from '../api/TruLedgerAPI';
 

export default class TruChain extends Elem {

    constructor(props: any = null) {
        super("div", props);
      
        this.style = {display: 'flex', width: '100%'};

        const ledgerHeader  = new FlexBox();
        ledgerHeader.style = {width: '100%', justifyContent: 'space-between', alignItems: 'center', marginBottom: "10px"};
       
        const LedgerHeaderLabel = new Text();
        LedgerHeaderLabel.Text = "Chain: "
        LedgerHeaderLabel.style = { "fontSize": "20px" , marginRight: "10px"};
        this.toggleLedgerButton.onClick = this.toggleLedgerView.bind(this);  
        this.toggleLedgerButton.style = {marginRight: "15px"};
        this.toggleFlexBox.push(LedgerHeaderLabel)
        this.toggleFlexBox.push(this.toggleLedgerButton);

       
        this.fullNodeSwitch.style={marginLeft: '20px'};
        console.log("Full node a switch");
        const isFullNode = ChainLoader.GetChainLoader();
         
        this.fullNodeSwitch.isOn = isFullNode;
        this.fullNodeSwitch.LabelText = this.fullNodeSwitch.isOn ? "Full Node: " : "Light Wallet: ";
   
        this.fullNodeSwitch.onSwitch = async ()=>{
            const isFullNode = ChainLoader.GetChainLoader();
            
            if(isFullNode){
                LUIX.post({'showModal': true, 'modalContent': new ConfirmBecomeLightNode()});
            }else{
                var blockCount = 0;
                var chainSize = "0MB";
                try{
                var promises = await Promise.all([TruLedgerAPI.BlockCount(), TruLedgerAPI.Size()])
                 blockCount = promises[0];
                 chainSize = promises[1];
                }catch(e){
                    
                }  

                LUIX.post({'showModal': true, 'modalContent': new ConfirmBecomeFullNode(), 'chainSize': chainSize, 'blockCount': blockCount });
            }
        };


 
        this.refreshButton.onClick = ()=>{this.getLocalTruLedger()}  
        this.refreshButton.innerText = "";
        this.refreshButton.children.push(new RefreshIcon());
        this.refreshButton.style = {height: '40px'}


        ledgerHeader.push(this.fullNodeSwitch);
        ledgerHeader.push(this.refreshButton);
        ledgerHeader.push(this.toggleFlexBox);

        const chainContainer = new FlexBox();
        chainContainer.style = {position: 'relative', flexDirection: 'column', width: "100%"};

        //this.chainView.push(LedgerHeader);
        this.chainView   = new FlexBox();
        this.chainView.style = {flexDirection: 'row', justifyContent: 'flex-start', width: "100%", overflow:'auto' };
        this.chainView.visible = false;
     
  
        chainContainer.push(ledgerHeader);
 
        chainContainer.push(this.chainView);

        this.children.push(chainContainer);

        this.chainView.onWheel = (ev: WheelEvent)=>{
            this.chainView.scrollLeft(ev.deltaY);
            ev.stopPropagation();
            ev.preventDefault();
        };

        this.chainView.onScroll = (ev: Event)=>{
           console.log("ONS CROLL>>>>")
       
        };

        this.getLocalTruLedger();
    }
     

    private toggleFlexBox : FlexBox = new FlexBox();

    private refreshButton : IconButton = new IconButton();
 
    public chainView: FlexBox;
    private toggleLedgerButton: ToggleUpDwnBtn = new ToggleUpDwnBtn({initDown: true});
 
    private fullNodeSwitch: Switch = new Switch();
   
    private toggleLedgerView(){
         
        this.getLocalTruLedger();
        this.showLedger = !this.showLedger;
    }


    private showLedger: boolean = false;


    private async getLocalTruLedger(): Promise<void> {
      

        var totalBlocks = await LocalChainV2.GetBlockCount();
        var localChain =  await LocalChainV2.GetChainInRange(totalBlocks-20, totalBlocks);
 
        var localWallet:ITruWallet = LUIX.getStateAt("wallet");
        if(localWallet){
            var latestCalculatedBalance = await LocalChainV2.BalanceAt(localWallet.ID);

            if(localWallet.Balance != latestCalculatedBalance){
                localWallet.Balance = latestCalculatedBalance;
                LUIX.postData("wallet", localWallet);
            }
        }

        try {
            if(localChain){
              
                this.chainView.clear();
      
                for (var block of  localChain ) {

                    let tb = new TruBlock(block);
                    tb.previousHash = block.PreviousHash;
                    tb.blockHash = block.BlockHash;
                    this.chainView.children.unshift(tb);
           
                }

                this.changed(this.props);
 
            }
        } catch(e) {
             
        }
 
    }


    public async changed(props: any): Promise<void> {
 
        var isFullNode:boolean = ChainLoader.GetChainLoader();

        var valid = await LocalChainV2.ValidChain();
        console.log("Tru chain.. is Full Node?");
        console.log(isFullNode);
        if(!isFullNode){
            this.toggleLedgerButton.toggle = true;
            this.chainView.clear();
        } 
 
 
        this.fullNodeSwitch.isOn = isFullNode;
        this.fullNodeSwitch.LabelText = isFullNode ? "Full Node: " : "Light Wallet: ";
 
        this.toggleFlexBox.visible = isFullNode;
        this.chainView.visible = this.showLedger && isFullNode;
        this.refreshButton.visible = this.showLedger && isFullNode;
  
        super.changed(props);
    }


}