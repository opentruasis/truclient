import TruAppAPI from "../api/TruAppAPI";
import {FlexBox, Text, Image, LUIX} from "luix";
 
import ITruApp from "../types/ITruApp";
import Paper from "../units/Paper";
import StyledButton from "../units/StyledButton";
import BaseURL from '../api/BaseURL';

export default class AppCard extends Paper {

    constructor(props: any = null ) {
        super(props);
        var app = props as ITruApp;

        this.style = {flexDirection: 'column', backgroundColor: "#eee",   flexShrink: 0, padding: "5px"};

        this.push(new Text(app.Name));
        
        this.push(new Text(app.Description))

        //Showcase:

        var firstImage = new Image();
        firstImage.style = {maxWidth: "200px"};
        firstImage.imgSource = BaseURL() + "/apps/" +  app.ShowcaseFiles[0];
        this.push(firstImage);

        const btnContainer  = new FlexBox();


        var learnMoreBtn = new StyledButton();
        learnMoreBtn.innerText = "Learn More";
        learnMoreBtn.style = {padding: "2px", height: "none"};
        learnMoreBtn.onClick = ()=>{
            
           window.open(app.GitRepoURL);
        };
        btnContainer.push(learnMoreBtn)

        var downloadBtn = new StyledButton();
        downloadBtn.innerText = "Open App";
        downloadBtn.style = {padding: "2px", height: "none"};
        downloadBtn.onClick = ()=>{
            TruAppAPI.OpenApp(app);
        };

        btnContainer.push(downloadBtn);
        this.push(btnContainer);
 
    }

    public changed(props: any = null)
    {
        


    }
        
        
}     
