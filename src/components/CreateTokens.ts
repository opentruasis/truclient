import {LUIX, FlexBox, Entry, Switch, Label, Text} from 'luix';
 

import LocalSuperNodeURL from '../api/LocalSuperNodeURL';
 
import StyledButton from '../units/StyledButton';
 
import Paper from '../units/Paper';
 
import TruExchangeAPI from '../api/TruExchangeAPI';
import CloseIconBtn from '../units/CloseIconBtn';
import TruTokenAPI from '../api/TruTokenAPI';
import ITruWallet from '../types/ITruWallet';

export default class CreateTokens extends Paper {

    constructor(props: any = null){
        super(props);
        this.style= {
            minHeight: "250px", 
            backgroundColor:"#eee",
             flexDirection: 'column',  
             justifyContent: 'space-between', 
             alignItems: 'center',
            padding: '5px'};
      
        const createHeader = new FlexBox();
        createHeader.style = {width: '100%', margin: "5px", justifyContent: 'space-between', alignItems: 'center'};
        var title = new Label("Create TRU Token: ");
        title.style={fontSize: "18px"};
        createHeader.push(title);


        this.children.push(createHeader);
 
        this.tknName.EntryLabel.Text = "Token Name: ";
        this.push(this.tknName)


        this.tokenFileInput.EntryInput.InputType = "file";
        this.tokenFileInput.EntryLabel.Text = "Token File:";
        this.push(this.tokenFileInput);


        const priceReadout = new FlexBox();
        priceReadout.push(new Text("Mint Price (TRU): "));
        this.tokenTRUPriceLabel.style = {marginLeft: '5px'};
        priceReadout.push(this.tokenTRUPriceLabel);
        this.push(priceReadout);


        this.buyTokenBtn.innerText = "Mint Token";

        this.buyTokenBtn.onClick = async ()=>{

            LUIX.postData('showLoader', true);

            var selectedFile = this.tokenFileInput.EntryInput.files[0];
            var newTokenData = {
                tknFile: selectedFile,
                Name: this.tknName.EntryInput.Value,
                Qty: this.qtyInputBox.EntryInput.Value || 1,
                isNFT: this.NFTSwitch.isOn,
                CreatedBy: (LUIX.getStateAt("wallet") as ITruWallet).ID
              };

              
              
           var res =  await TruTokenAPI.NewToken(newTokenData);
           LUIX.postData('showLoader', false);
           
        }

        this.push(this.buyTokenBtn);
 
    }

    public NFTSwitch  = new Switch();

    public tokenFileInput = new Entry();

    public qtyInputBox  = new Entry();
    
    public tknName  = new Entry();

    public tokenTRUPriceLabel  = new Text();

    public buyTokenBtn: StyledButton = new StyledButton();
    
    
    public changed(props: any = null){

        this.qtyInputBox.visible = !this.NFTSwitch.isOn;

        this.tokenTRUPriceLabel.Text = (1/LUIX.getStateAt("currentTRUPrice")).toFixed(4).toString();
                
        
        super.changed(props);
    }
 
  
}