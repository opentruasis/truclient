﻿import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import {Elem,Text, FlexBox} from 'luix';
import ITruBlock from '../types/ITruBlock';

import TransactionIdList from '../units/TransactionIdList';

interface ITransaction {
    ID: string;
}
export default class TruBlock extends Elem {

    constructor(block:ITruBlock, props: any = null) {
        super("DIV", props);
        this.block = block;

        this.style = {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            border: '1px black solid',
            borderRadius: '5px',
            padding: '5px',
            minWidth: "450px",
            minHeight: "200px",
            maxWidth: "500px",
            margin : "5px",
            float: "left",
        };

        this.addClass("selectable");


        this.onClick = () => {  };

        this.BlockHeader.style = { display: "flex", justifyContent: 'space-between' };
        this.BlockHeader.push(this.BlockIdLabel);
        this.BlockHeader.push(this.PreviousHash);
        this.BlockIdLabel.style = { fontSize: "20px", margin: "5px" };
 
        this.children.push(this.BlockHeader);
        this.children.push(this.ListOfTransactions);

        this.ListOfTransactions.blockId = this.block.ID;
 
        this.BlockHashLabel.style = {alignSelf: 'flex-end'};
        this.children.push(this.BlockHashLabel);

    }

    private BlockHeader = new FlexBox();


    private PreviousHash = new Text();
    public previousHash: string = "";

    private BlockIdLabel = new Text();
    public block : ITruBlock = null;

    private ListOfTransactions: TransactionIdList = new TransactionIdList(); 

  
    private BlockHashLabel = new Text();
    public blockHash: string = "";

    private formatHash(hash: string) : string{
        var firstPart = hash.substring(0, 10);
        var secondPart = hash.substring(hash.length-10);

        return firstPart +"...."+secondPart;
    }


    public changed(props: any) {
 
        this.BlockIdLabel.Text = this.block.ID.toString();

        this.PreviousHash.Text = this.formatHash(this.previousHash);

        this.BlockHashLabel.Text = this.formatHash(this.blockHash);
   
        //Transactions.
    
        let transactionData = this.block.Transactions as ITransaction[];
        console.log("Block Data..");
        console.log(transactionData);
        this.ListOfTransactions.changed(transactionData);

    }

}