import TruAppAPI from "../api/TruAppAPI";
 
import {LUIX,Text, Image, FlexBox, Div} from "luix";


import CloseIconBtn from "../units/CloseIconBtn";
import Paper from "../units/Paper";
import StyledButton from "../units/StyledButton";
 
export default class ShowCaseFileView extends FlexBox  {

    constructor(props: any = null ) {
        super(props);
 
        this.style = {flexDirection: 'row', backgroundColor: "#eee",  justifyContent: 'center', flexWrap: 'wrap', padding: "5px", alignItems: 'center'};
 
    }



    public changed(props: any = null)
    {
        console.log("SHOW CASE FILES CHANGED>>???");

        var { showCaseFiles = [] } = props || {};
      
        console.log(this.children.length);
        console.log(showCaseFiles);
        if(showCaseFiles.length !== this.children.length){
            this.clear();
            var fr: FileReader[] = [];
            for(var i=0; i< showCaseFiles.length; i++)  {
                var f = showCaseFiles[i];
                var file = (f as File);
                fr[i] = new FileReader();
                var caller = (index:number)=>{
                    fr[index].onload = () => {
                    
                        let imgContainer = new Div();

                        imgContainer.style={position: 'relative', margin: '5px', marginTop: "15px"};

                        var blockImage = new Image();
                    
                        blockImage.imgSource = fr[index].result as string;
                        blockImage.style = {maxWidth: '100px', minWidth: '50px', margin: '5px'};

                        var closeButton : CloseIconBtn = new CloseIconBtn();
                        closeButton.style= {position:'absolute', top: "-15px", right: '-15px'};
                    
                            console.log("-----***")
                            console.log(index)
                            closeButton.onClick = ()=>{
                                var listOfFiles = LUIX.getStateAt("showCaseFiles") || [];
                                console.log("CLick....");
                                console.log(index);
                                console.log(listOfFiles);
                                listOfFiles.splice(index,1);
                                console.log(listOfFiles);
                                LUIX.postData("showCaseFiles", listOfFiles);
                            };
                    

                        imgContainer.children.push(closeButton);
                        imgContainer.children.push(blockImage);

                        this.push(imgContainer);
                        console.log('this.children.length');
                        console.log(this.children.length);
                        super.changed(this.props);
                    }
                    fr[index].readAsDataURL(file);
                };
                
                caller(i);
            }

            
        }

        
    }
        
        
}     
