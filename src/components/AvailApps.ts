import LUIX from 'luix';
 
import Paper from '../units/Paper';
import { FlexBox, Label, ArrowDown} from 'luix';
import TokenSaleView from './TokenSaleView';
import StyledButton from '../units/StyledButton';
import AppSearchBar from '../units/AppSearchBar';
import ToggleUpDwnBtn from '../units/ToggleUpDwnBtn';
import AppCard from './AppCard';
import TruAppAPI from '../api/TruAppAPI';
import ITruApp from '../types/ITruApp';
import SocketConnection from '../SocketConnection';

export default class AvailApps extends Paper {

    constructor(props: any = null){
        super(props);
        this.style= {justifyContent:'flex-start',  
        flexDirection: 'column', alignItems: 'center', maxHeight: "800px", 
        overflowX: 'hidden', marginBottom: "30px"};
      

        const createHeader = new FlexBox();
        createHeader.style = {width: '100%', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap'};
        var title = new Label("TRU Apps: ");

        title.style={fontSize: "18px", marginLeft: "5px"};
        createHeader.push(title);

        createHeader.push(new AppSearchBar());


        const availAppsView = new ToggleUpDwnBtn();
        availAppsView.onClick = ()=>{
             this.AvailableAppsContainer.visible = !this.AvailableAppsContainer.visible;
        };
        createHeader.push(availAppsView);
        

        this.push(createHeader);

        this.AvailableAppsContainer.visible = false;
        this.AvailableAppsContainer.style = { position: 'relative', marginTop: "15", flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center'};

         
        this.push( this.AvailableAppsContainer);

        SocketConnection.onConnectedInvokes.push(async ()=>{
            var apps : {[key:string]:ITruApp} = await TruAppAPI.AllApps();
            if(apps){
                Object.values(apps).forEach(d => {
                
                    var dcard = new AppCard(d);
            
                    this.AvailableAppsContainer.push(dcard)
                });
            
                this.changed(props);
            }
        });
    }

    private AvailableAppsContainer = new FlexBox();

  
}