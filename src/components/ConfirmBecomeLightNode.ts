import {Elem, FlexBox, Image, Text, LUIX, Switch} from 'luix';
import ChainLoader from '../api/ChainLoader';
import CloseIconBtn from '../units/CloseIconBtn';
import StyledButton from '../units/StyledButton';
 
 
export default class ConfirmBecomeLightNode extends FlexBox {

    constructor(props: any = null) {
        super(props);

        this.style = {flexDirection: 'column', justifyContent: 'cetner'};

        const becomeFullNodeHeader = new FlexBox();
        becomeFullNodeHeader.style={width: "100%", justifyContent: 'space-between'};

        const headerLabel = new Text();
        headerLabel.Text = "Turn Light Wallet Mode On: "
        headerLabel.style = { "fontSize": "20px" , marginLeft: "10px"};

        const cancelLightSelect = new CloseIconBtn();
        cancelLightSelect.onClick = ()=>{
            ChainLoader.SetChainLoader(true);
            LUIX.post({'showModal': false, 'modalContent': null}, true);
        };

        becomeFullNodeHeader.push(headerLabel);
        becomeFullNodeHeader.push(cancelLightSelect);

        this.children.push(becomeFullNodeHeader);


        const notificationText: Text = new Text();
        notificationText.Text = `
        Moving from Full Node to a Light node. Would you like to clear existing chain data off your maching? 
        You can keep existing data for a faster full node re-establishment.
        `;

        this.children.push(notificationText);

        this.deleteData.LabelText = "Will Remove Local Chain Data";
        this.deleteData.style={alignSelf: 'flex-end'};
        this.deleteData.isOn = true;
        LUIX.postData("doClearTruChain", true, false);
        this.deleteData.onSwitch = ()=>{
            if(  this.deleteData.isOn ){
                this.deleteData.LabelText  = "Will Remove Local Chain Data";
                LUIX.postData("doClearTruChain", true, false);
            }else{
                this.deleteData.LabelText  = "Keep Local Data";
                LUIX.postData("doClearTruChain", false, false);
            }
        };
        

        this.children.push(this.deleteData);

        const commitBtn: StyledButton = new StyledButton();
        commitBtn.onClick = ()=>{
            ChainLoader.SetChainLoader(false);
            LUIX.post({'showModal': false, 'modalContent': null}, true);
        };
        commitBtn.style={alignSelf: 'flex-end', padding: "10px", marginTop: "20px"};
        commitBtn.innerText = "Use Light Wallet";
 
        this.children.push(commitBtn);
 
    }   

    private deleteData: Switch = new Switch();
 
}