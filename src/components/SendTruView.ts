import { Text, FlexBox, Entry, LUIX} from 'luix';

import WalletImage from '../units/WalletImage';
import StyledButton from '../units/StyledButton';

import TruTxAPI from '../api/TruTxAPI';
 
import TruTxMsg from '../types/TruTxMsg';

import TruWallet from '../types/ITruWallet';
 
import TruCrypto from '../api/TruCrypto';
 
import Paper from '../units/Paper';
import CloseIconBtn from '../units/CloseIconBtn';
import LocalWalletAPI from '../api/LocalWalletAPI';

export default class SendTruView extends Paper {

    constructor(props: any = null) {
        super(props);
        this.style = {
            flexDirection: 'column', 
            flexGrow: 1, 
            justifyContent: 'space-around', 
            alignItems: 'center',
            marginTop: "20px",
            minHeight: "350px",
            padding:"5px"
        };
        this.visible = false;
        this.WalletImage.src = "";
        this.FullName.Text = "";
        this.Handle.Text = "";


        const sendTruHeaderContainer = new FlexBox();
        sendTruHeaderContainer.style={width: "100%", justifyContent: 'space-between'};
        const SendTruHeaderLabel = new Text();
        SendTruHeaderLabel.Text = "Send TRU to: "
        SendTruHeaderLabel.style = { "fontSize": "20px" , marginLeft: "10px"};

        const clearSearch = new CloseIconBtn();
        clearSearch.onClick = ()=>{
            LUIX.postData('showSendTru', false);
        };
  
        sendTruHeaderContainer.push(SendTruHeaderLabel);

        sendTruHeaderContainer.push(clearSearch);


        this.push(sendTruHeaderContainer);



        const sendToContainer = new FlexBox();
        sendToContainer.push(this.WalletImage);

        const sendToNamesContainer = new FlexBox();
        sendToNamesContainer.style = {flexDirection: 'column'};
        sendToNamesContainer.push(this.FullName);
        sendToNamesContainer.push(this.Handle);
        sendToContainer.push(sendToNamesContainer);

 
        this.push(sendToContainer);
      
        this.TruAmountEntry.EntryLabel.Text = "TRU Amount:"
        this.TruAmountEntry.style = {flexDirection: 'column', alignItems: 'center'};
        this.TruAmountEntry.EntryInput.InputType = 'number';
        this.TruAmountEntry.EntryInput.style = {height: "40px", fontSize: "20px", textAlign: "center"}
        this.push(this.TruAmountEntry);
        
        const SendTRUBtn = new StyledButton();
        SendTRUBtn.onClick = this.SendTRUTo.bind(this);
        SendTRUBtn.innerText = "Send";
        SendTRUBtn.style = {width: "100px", fontSize: "20px"};
        this.push(SendTRUBtn);

    }
 
 
    public TruAmountEntry  = new Entry();
    private FullName  = new Text();
    private Handle  = new Text();
    private WalletImage: WalletImage = new WalletImage();
    private sendToPubKey: string = "";
    private sendToID: string = "";
 
    private async SendTRUTo(){

        const thisWallet = LUIX.getStateAt("wallet")  as TruWallet;
        const newTx : TruTxMsg = new TruTxMsg();
        newTx.TruAmount =  parseFloat(this.TruAmountEntry.EntryInput.Value);
        newTx.SignerId = thisWallet.ID;
        newTx.ReceiverId = this.sendToID;
        newTx.ReceiverPubKey = this.sendToPubKey;
        console.log("THIS WALLET SENDING TRU????");
        console.log(thisWallet);
        var localWallet = LocalWalletAPI.GetWallet(); // Get the local wallet private key! the data in LUIX is potentially null
        const signedMsg = TruCrypto.SignMsg(newTx, localWallet.Keys.PrivKey)  as TruTxMsg;
        
        try{
            const results = await  TruTxAPI.PostTx(signedMsg);
            this.TruAmountEntry.EntryInput.Value = "";
        }catch(e){

        }

    }



    public changed(props: any){
 
        const { sendTruTo  = undefined , showSendTru = false } = props || {};

        if(!sendTruTo || !showSendTru){
                this.visible = false;
                return;
        }else{
            this.visible = true;
        }

        this.sendToID = sendTruTo.ID;
        this.sendToPubKey = sendTruTo.Keys.PubKey;
        this.FullName.Text = sendTruTo.FullName;
        this.Handle.Text = "@" + sendTruTo.Handle;
        try{
            this.WalletImage.src = sendTruTo.ProfilePicBase64;
        }catch(e){
                
        }

        super.changed(props);
    }
  

}     
