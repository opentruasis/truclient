import AppLoading from './components/AppLoading';
 
import WalletPage from './pages/WalletPage';
import TruasisPage from './pages/TruasisPage';
import ExchangePage from './pages/ExchangePage';

import PurchaseFormPage from './pages/PurchaseFormPage';
import PurchaseFormBuilderPage from './pages/PurchaseFormBuilderPage';

import StyledButton from './units/StyledButton';


import {LUIX, FlexBox, Div, Elem, Text} from 'luix';
 

import NavBar from './components/NavBar';
import CreatePage from './pages/CreatePage';
import TruExchangeAPI from './api/TruExchangeAPI';
import BuyTRU from './pages/BuyTRU';
import MyTRUOrders from './pages/MyTRUOrders';
import ChainPage from './pages/ChainPage';
import Connectivity from './components/Connectivity';
import AppModal from './components/AppModal';
import SellTRU from './pages/SellTRU';


 
export default class App extends  Elem {

    constructor(props: any = null){
        super("div", props);

       // TruCrypto.TestSign();
       // TruCrypto.TestRSACompare();

       // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "space-between",
            overflowY: "auto"
        };

        setTimeout(async ()=>{ 
            var TRUPRICE  = await TruExchangeAPI.GetLastPrice();
            console.log("TRU PRICES>>>>>>")
            if(TRUPRICE){
                console.log(TRUPRICE.Price)
                LUIX.postData("currentTRUPrice", TRUPRICE.Price);
            }
         });
        
        setInterval(async ()=>{
            var TRUPRICE  = await TruExchangeAPI.GetLastPrice();
            console.log("TRU PRICES>>>>>>")
            if(TRUPRICE){
                console.log(TRUPRICE.Price)
                LUIX.postData("currentTRUPrice", TRUPRICE.Price);
            }
        }, 60000);
        
        const mainRouter: FlexBox  = new FlexBox();
         
        mainRouter.style = {flexDirection: 'column', justifyContent:'space-between', flexGrow: 1, width: "100%"};

        const navBar = new NavBar();
 
        mainRouter.push(navBar);

       
 
        this.AddRoute("/purchase", new PurchaseFormPage());

        this.AddRoute("/purchaseFormBuilder", new PurchaseFormBuilderPage());
  
        const RouteBody = new Div();
        RouteBody.style={flexGrow: 1, width: "100%"};
        RouteBody.AddRoute("/", new WalletPage());
 
        RouteBody.AddRoute("/chain", new ChainPage());
 
        RouteBody.AddRoute("/exchange", new ExchangePage());
      
        RouteBody.AddRoute("/truasis", new TruasisPage());

        RouteBody.AddRoute("/create", new CreatePage());

        //RouteBody.AddRoute("/transaction/${id}" , new TransactionDetailPage());
     
        RouteBody.AddRoute("/buytru", new BuyTRU());
        RouteBody.AddRoute("/selltru", new SellTRU());
        RouteBody.AddRoute("/mytruorders", new MyTRUOrders());

        
        mainRouter.push(RouteBody);

        mainRouter.push(new Connectivity());
        // const chainFooter = new FlexBox();
        // chainFooter.style = {justifyContent: 'space-between'};

        // const chainLinkBtn: StyledButton = new StyledButton();
        // chainLinkBtn.innerText = "TRUChain";
        // chainLinkBtn.style = {width: "150px"};
        // chainLinkBtn.onClick = ()=>{LUIX.GoTo("/chain");};
        // chainFooter.push( chainLinkBtn );
  

        // mainRouter.push(chainFooter);

       

        this.children.push(mainRouter);
 
        this.children.push(new AppLoading());

        this.children.push(new AppModal());
        window.scrollTo(0, 1);
    }   
 

}
