import {LUIX} from 'luix';

export default function BaseURL(): string{
    
    var baseUrl = LUIX.getStateAt("selectedSuper") as string;
    console.log("Base Url...");
    console.log(baseUrl);
    if(!baseUrl){
        baseUrl = "https://localhost:44380";
        //baseUrl = "http://18.144.168.127:5001";
    }

    return baseUrl;
};