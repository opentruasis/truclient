import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import axios from 'axios';
import ITruBuyOrder from '../types/ITruBuyOrder';
import ITruOrder from '../types/ITruOrder';
 import ITruPrice from '../types/ITruPrice';
import ITruSellOrder from '../types/ITruSellOrder';
import OrderStatus from '../types/OrderStatus';
import OrderType from '../types/OrderType';

//const ExchangeURL = "https://ex.truasis.com";
 const ExchangeURL = "http://localhost:5000";

let truPrice = 0.001;
function priceSim(){
    truPrice = (Math.random()*2-1)/100 + truPrice;
    if(truPrice < 0.001){
        truPrice = 0.001;
    }
}
setInterval(()=>{priceSim()}, 1000);

const TruExchangeAPI  = {

  GetLastPrice: async  ():  Promise<ITruPrice> => {
    try{
      var results = await axios.get(ExchangeURL + '/api/truprice/last');
      return results.data as ITruPrice;
    }catch(e){
      return {TimeStamp: new Date(), Price: 0};
    }
  },
  GetTruPrices: async (): Promise<ITruPrice[]> =>{

    try{
      var results = await axios.get(ExchangeURL + '/api/truprice');
      var prices = results.data.map((data: ITruPrice)=>{
        //normalilze tx dates to UTC time.
        var date = new Date(data.TimeStamp);
        var offset = date.getTimezoneOffset();
        date.setMinutes(date.getMinutes() + offset);
        data.TimeStamp = date;
        return data;
      });
      console.log(prices);
      return prices;
    }catch(e){
      return [{TimeStamp: new Date(), Price: 0}];
    }

  },
 
  GetOrderStatus: async (exchangeOrderID: number,  orderType: OrderType): Promise<ITruOrder> =>{

    if(orderType == OrderType.Buy)
    {
        return await TruExchangeAPI.GetBuyOrder(exchangeOrderID);
    }
    
    return await TruExchangeAPI.GetSellOrder(exchangeOrderID);

  },

  GetBuyOrder: async (orderId: number): Promise<ITruBuyOrder> =>{

    try{
      var results = await axios.get(ExchangeURL + '/api/trubuy/' + orderId);
      return results.data as ITruBuyOrder;
    }catch(e){
      return null;
    }

  },

  GetSellOrder: async (orderId: number): Promise<ITruBuyOrder> =>{
    try{
      var results = await axios.get(ExchangeURL + '/api/trusell/' + orderId);
      return results.data as ITruBuyOrder;
    }catch(e){
      return null;
    }

  },
 
  GetBestBid: async (): Promise<ITruBuyOrder> =>{
    try{
      var results = await axios.get(ExchangeURL + '/api/truorders/bestbid');
      return results.data as ITruBuyOrder;
    }catch(e){
      return null;
    }

  },
 
  GetBestAsk: async (): Promise<ITruSellOrder> =>{
    try{
      var results = await axios.get(ExchangeURL + '/api/truorders/bestask');
      return results.data as ITruSellOrder;
    }catch(e){
      return null;
    }

  },

  PostSellOrder: async (tx: any): Promise<ITruSellOrder> =>{
    try{
      var results = await axios.post(ExchangeURL + '/api/trusell', tx);
      return results.data as ITruSellOrder;
    }catch(e){
      return null;
    }

  },

  CancelBuyOrder: async (orderID: number, walletId: string): Promise<ITruBuyOrder> =>{
    try{
     var data = {
              "WalletId": walletId,
              "OrderStatus" : OrderStatus.Canceled
              };

      var results = await axios.put(ExchangeURL + '/api/trubuy/' + orderID, data);
      return results.data as ITruBuyOrder;
    }catch(e){
      return null;
    }

  },

  GetMasterWalletInfo: async (): Promise<any> =>{
    try{
      var results = await axios.get(ExchangeURL + '/api/masterwallet');
      return results.data;
    }catch(e){
      return null;
    }

  },

  PostBuyOrder: async (tx: any): Promise<ITruBuyOrder> =>{
  
    try{
      var results = await axios.post(ExchangeURL + '/api/trubuy', tx);
      return results.data as ITruBuyOrder;
    }catch(e){
      return null;
    }

  },

  CancelSellOrder: async (orderID: number, walletId: string): Promise<ITruBuyOrder> =>{
    try{
     var data = {
              "WalletId": walletId,
              "OrderStatus" : OrderStatus.Canceled
              };

      var results = await axios.put(ExchangeURL + '/api/trusell/' + orderID, data);
      return results.data as ITruBuyOrder;
    }catch(e){
      return null;
    }

  }
};



export default TruExchangeAPI;