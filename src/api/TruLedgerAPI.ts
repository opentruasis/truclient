﻿import axios from 'axios';
 import BaseURL from './BaseURL';
 
const TruLedgerAPI = {

    FullLedger: async (): Promise<any> => {
   
        try {
            var results = await axios.get( BaseURL() + '/api/TruLedger');
            return results.data;
        } catch (e) {
            throw new DOMException(e);
        }  
    },

    LedgerSince: async (blockId: string): Promise<any>  => {
        try {
            var results = await axios.get(BaseURL() + '/api/TruLedger/' + blockId);
            return results.data;
        } catch (e) {
            throw new DOMException(e);
        }
    },

    BlockCount: async (): Promise<number>  => {
        try {
            var results = await axios.get(BaseURL() + '/api/TruLedger/blockCount');
            return results.data as number;
        } catch (e) {
            throw new DOMException(e);
        }
    },

    Size: async (): Promise<string>  => {
        try {
            var results = await axios.get(BaseURL() + '/api/TruLedger/size');
            return results.data;
        } catch (e) {
            throw new DOMException(e);
        }
    },
};

export default TruLedgerAPI;
