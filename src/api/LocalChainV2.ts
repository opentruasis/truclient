import Dexie from 'dexie';

import {Div, LUIX} from "luix";
import ITruTx from '../types/ITruTx';
import ITruBlock from '../types/ITruBlock';
import ChainLoader from './ChainLoader';
import ConfirmBecomeFullNode from '../components/ConfirmBecomeFullNode';
import SocketConnection from '../SocketConnection';
import TruLedgerAPI from './TruLedgerAPI';
import ITruWallet from '../types/ITruWallet';


class LocalChainV2 {

    private static db = new Dexie('truChain');

    private static isFullNode: boolean = false;
 

    static Init(){
        try{
            LocalChainV2.db.version(1).stores({Chain: "ID"});

            LocalChainV2.isFullNode = ChainLoader.GetChainLoader();

            console.log("Local Chain V2 Init.")
            
            setInterval(async ()=>{ 
        
                var latestChainLoaderVal = ChainLoader.GetChainLoader();
                if(latestChainLoaderVal !== LocalChainV2.isFullNode){
                    if(latestChainLoaderVal){
                       
                        LocalChainV2.StartFullNodeManager();
                    }else{
                        var doClearTruChain = LUIX.getStateAt('doClearTruChain');
                        console.log("Do Clear TRU CHAIN???");
                        console.log(doClearTruChain);
                        clearInterval(LocalChainV2.fullNodeManagerRuntimeInterval);
                        if(doClearTruChain){
                            await LocalChainV2.RemoveData();     
                        }
                    }
                }
                LocalChainV2.isFullNode = latestChainLoaderVal
               
                
            }, 1000);

            if(LocalChainV2.isFullNode){
                LocalChainV2.StartFullNodeManager();
            }

        }catch(e){
            console.log(e);
        }
    }


    // private static Chain: {[key:string]:ITruBlock} = {};
 
    public static async GetChain(): Promise< ITruBlock[]>{
        try{
            var blocks =  await LocalChainV2.db.Chain.toArray();
            return blocks;
        }catch(e){
            return  [];
        }
    }


    public static async GetChainInRange(startId: number , endId: number): Promise< ITruBlock[]>{
        try{
            var blocks =  await LocalChainV2.db.Chain.where("ID").between(startId, endId, true, true).toArray();
            return blocks;
        }catch(e){
            return  null;
        }
    }



    public static async AddToChain(block: ITruBlock): Promise<any>{
        try{
          return  await LocalChainV2.db.Chain.add(block);
        }catch(e){
            return  null;
        }
    }

    public static async UpdateLatestBlock(block: ITruBlock): Promise<any>{
        try{
            console.log("Udate.>??");
            return  await LocalChainV2.db.Chain.put(block);
        }catch(e){
            return  null;
        }
    }

    public static async GetBlockCount(): Promise<number>{
        try{
            return await LocalChainV2.db.Chain.count(); 
        }catch(e){
            return  null;
        }   
    }

   
    private static async StartFullNodeManager(){

        var blockCount =  await TruLedgerAPI.BlockCount();
 
        LocalChainV2.fullNodeManagerRuntimeInterval = setInterval(async ()=>{

            console.log("StartFullNodeManager")
            if(!LocalChainV2.db.isOpen()){
                LocalChainV2.db = new Dexie('truChain');
                LocalChainV2.db.version(1).stores({Chain: "ID" });
            }
 
            var nextBlock = await LocalChainV2.GetBlockCount();
            console.log("NEXT BLOCK??====", nextBlock)
            console.log(blockCount);
            if(blockCount > nextBlock){
                console.log("Get Next BlocK: ", nextBlock);
                SocketConnection.GetChainData(nextBlock-1);
            }
        }, 5000);

    }

    private static fullNodeManagerRuntimeInterval: NodeJS.Timer = null;

    private static async RemoveData(){
        console.log("Deleting Tru Ledger Block Chain");
        console.log(await LocalChainV2.db.delete());
    }
  
    public static async GetMyBalance(){

        var localWallet:ITruWallet = LUIX.getStateAt("wallet");

        var latestCalculatedBalance = await LocalChainV2.BalanceAt(localWallet.ID);

        if(localWallet.Balance != latestCalculatedBalance){
            localWallet.Balance = latestCalculatedBalance;
            LUIX.post({"wallet": localWallet});
        } 
   
    }
    
    private static async EndBlockId(): Promise<number>{
        try{
            var lastBlock: ITruBlock= await LocalChainV2.db.Chain.orderBy('ID').last();
            console.log("Last Bloc Get End Block ID>>>");
            console.log(lastBlock);
            return lastBlock.ID;
        }catch(e){

        }
    };

    private static async EndBlock(): Promise<ITruBlock>{
        try{
            var lastBlock: ITruBlock = await LocalChainV2.db.Chain.orderBy('ID').last();
            console.log("Last Bloc Get End Block>>>");
            console.log(lastBlock);
            return lastBlock;
        }catch(e){

        }
    }
    public static CrawledTo :number  = -1;

    private static _balances: {[id:string]: number} = {}; 
    /// Balances here will be as of last block -1..
    // Cacheing the balances upto but not including the tip.
    //Get tip balances with GetBalance  as the last block is getting new Txs as we go..

    public Balances():  {[id:string]: number}{
        return LocalChainV2._balances;
    }


    public static async ValidChain(): Promise<boolean> {
        var chain:  ITruBlock[] = await LocalChainV2.GetChain();
        console.log("IS Valid Chain?????");
        console.log(chain);
        var lastHash = "";
        var chainComplete: boolean = true;
        var chainGoodTil: number = 0;
        for(var b of chain){
            console.log(b);
            console.log(b.ID);
            
            if (b.ID == 0)
            {
                lastHash = b.BlockHash;
            }
            else
            {
                console.log(b.PreviousHash,lastHash);
                if (b.PreviousHash == lastHash)
                {

                    lastHash = b.BlockHash;
                    chainGoodTil = b.ID;
                }
                else
                {

                    console.log("Comlete TO: " +chainGoodTil);
                    chainComplete = false;
                    //Request Block b.ID-1 and on..
                    LocalChainV2.CrawledTo = chainGoodTil;

                    SocketConnection.GetChainData(chainGoodTil);
                    break;
                }
            }
        }

        return chainComplete;
    }

    public static async CalculateBalances(): Promise<{[id:string]: number}>
    {
        var endBlock: ITruBlock = await LocalChainV2.EndBlock();
        if(!endBlock) return null;
        console.log("Calcualte Balances.. End Block..?");
        console.log(endBlock);
        console.log(LocalChainV2.CrawledTo);
        if (LocalChainV2.CrawledTo == endBlock.ID -1 )
        {
            return LocalChainV2._balances;
        }
 
        var nextTruBalances: {[id:string]: number} = LocalChainV2._balances;

        var nextBlock: ITruBlock = null;

        var nextBlockId = LocalChainV2.CrawledTo + 1;

        var nextBlock = await LocalChainV2.GetBlock(nextBlockId);
        if (nextBlock && nextBlockId != endBlock.ID)
        {
            LocalChainV2.CrawledTo++;
        }
        else
        {
            nextBlock = null;
        }
 
        while (nextBlock != null)
        {
            for (var nextTx of nextBlock.Transactions)
            {
                if (nextTx.SignerId in nextTruBalances)
                {
                    nextTruBalances[nextTx.SignerId] -= nextTx.TruAmount;
                }
                else
                {
                    if (nextBlock.ID != 0)
                    {
                        //throw new Error("Unkown Sender tried to send TRU");
                    }
                }


                if (nextTx.ReceiverId in nextTruBalances)
                {
                    nextTruBalances[nextTx.ReceiverId] += nextTx.TruAmount;
                }
                else
                {
                    nextTruBalances[nextTx.ReceiverId] = nextTx.TruAmount;
                }
            }
            var nextBlockId = LocalChainV2.CrawledTo + 1;
            nextBlock = await LocalChainV2.GetBlock(nextBlockId);
            if (nextBlock && nextBlockId != endBlock.ID)
            {
                LocalChainV2.CrawledTo++;
            }
            else
            {
                nextBlock = null;
                //request next block ID// or maybe block hash missmatch..
            }
        }

     
        LocalChainV2._balances = nextTruBalances;
        return nextTruBalances;
     
    }


    public static async GetUpToDateBalances(): Promise<{[id:string]: number}> {

        if(!ChainLoader.GetChainLoader()) return null;
        if(!(await LocalChainV2.ValidChain())) return null;
        var upToCurrentBlockBalances = await LocalChainV2.CalculateBalances();
        var tipBalances = {...upToCurrentBlockBalances};
        console.log("Get Upd To Date Balances.. TIp?");
        console.log(tipBalances);
        var nextBlock: ITruBlock = await LocalChainV2.EndBlock();

        if(nextBlock){
            for (var nextTx of nextBlock.Transactions)
            {
                if (nextTx.SignerId in tipBalances)
                {
                    tipBalances[nextTx.SignerId] -= nextTx.TruAmount;
                }
                else
                {
                    if (nextBlock.ID != 0)
                    {
                        
                    // LocalChainV2.SetChain({});
                      //  throw new Error("Unkown Sender tried to send TRU");
                    }
                }

                if (nextTx.ReceiverId in tipBalances)
                {
                    tipBalances[nextTx.ReceiverId] += nextTx.TruAmount;
                }
                else
                {
                    tipBalances[nextTx.ReceiverId] = nextTx.TruAmount;
                }
            }
            return tipBalances;
        }else{
            return null;
        }
    }



    public static async BalanceAt(walletId: string):  Promise<number> {
        var tipBalances = await LocalChainV2.GetUpToDateBalances();
        if(tipBalances){
            if(walletId in tipBalances){
                return tipBalances[walletId];
            }
            else if(walletId in LocalChainV2._balances){
                return LocalChainV2._balances[walletId];
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }


    private static async GetBlock(blockNumb: number): Promise<ITruBlock> {
        if(LocalChainV2.db.Chain){
            console.log(LocalChainV2.db.Chain);
            console.log(blockNumb);
            var blockFromDb = await LocalChainV2.db.Chain.get(parseInt(blockNumb.toString()));
            console.log("Block From Db..");
            console.log(blockFromDb);
             return blockFromDb;
        }
        return null;
    }
 
    public static async GetLastBlock(): Promise<ITruBlock>{
        var endblockId = await this.EndBlockId();
        return await this.GetBlock(endblockId);
    }


    public static async GetTxAt(blockNumb: number, txId: string): Promise<ITruTx> {
        console.log(blockNumb);
        var block: ITruBlock = await LocalChainV2.GetBlock(blockNumb);
        console.log("Get Tx at: ");
        console.log(block);
        if(block){
            return block.Transactions.find(tx => tx.ID === txId);
        }        
    }
     
 

}

export default LocalChainV2;