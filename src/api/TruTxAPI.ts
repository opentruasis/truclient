import axios from 'axios';
import LUIX from 'luix';
import ITruTx from '../types/ITruTx';
import BaseURL from './BaseURL'
 
const TruTxAPI = {
 
PostTx: async(newTx: ITruTx): Promise<ITruTx> =>{
 
    try{
        var results = await axios.post(BaseURL() + "/api/Tx", newTx);
        return  results.data as ITruTx;
    }catch(e){
 
        return null;
    }
  },
 
};

 
export default TruTxAPI;
