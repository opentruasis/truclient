// import {LUIX} from "luix";
// import ITruTx from '../types/ITruTx';
// import ITruBlock from '../types/ITruBlock';

// export default class LocalChain {
 
//     private static Chain: {[key:string]:ITruBlock} = {};

//     public static GetChain(): any{
    
//         if (typeof(Storage) !== "undefined") {
//                 const storageChain =  JSON.parse(localStorage.getItem('truChain'));
//                 if(storageChain){
//                     LocalChain.Chain = storageChain;
//                 }else{
//                     LocalChain.Chain = {};
//                 }
//                 return LocalChain.Chain 
//             } else {
//             throw new Error("No Local Storage");
//         }
//     }
 
//     public static  SetChain(chain: any) : void{
//         try{
//             LocalChain.Chain = chain;
//             LUIX.postData("truChain", chain);
//             localStorage.setItem("truChain", JSON.stringify(chain));
        
//         }catch(e){
             
//         }
//     } 
 
  
//     private static get EndBlockId(): number{
//         return Object.keys(LocalChain.Chain).length-1;
//     };

//     private static get EndBlock(): ITruBlock{
//         return LocalChain.Chain[ LocalChain.EndBlockId];
//     }
//     public static CrawledTo :number  = -1;

//     private static _balances: {[id:string]: number} = {}; 
//     /// Balances here will be as of last block -1..
//     // Cacheing the balances upto but not including the tip.
//     //Get tip balances with GetBalance  as the last block is getting new Txs as we go..

//     public Balances():  {[id:string]: number}{
//         return LocalChain._balances;
//     }


//     public static CalculateBalances(): {[id:string]: number}
//     {
//         if (LocalChain.CrawledTo == LocalChain.EndBlockId-1)
//         {
//             return LocalChain._balances;
//         }
 
//         var nextTruBalances: {[id:string]: number} = LocalChain._balances;

//         var nextBlock: ITruBlock = null;

//         var nextBlockId = LocalChain.CrawledTo + 1;
//         if (nextBlockId in LocalChain.Chain && nextBlockId != LocalChain.EndBlockId)
//         {
//             LocalChain.CrawledTo++;
//             nextBlock =  LocalChain.Chain[ LocalChain.CrawledTo];
//         }
//         else
//         {
//             nextBlock = null;
//         }
 
//         while (nextBlock != null)
//         {
//             for (var nextTx of nextBlock.Transactions)
//             {
//                 if (nextTx.SenderId in nextTruBalances)
//                 {
//                     nextTruBalances[nextTx.SenderId] -= nextTx.TruAmount;
//                 }
//                 else
//                 {
//                     if (nextBlock.ID != 0)
//                     {
//                         throw new Error("Unkown Sender tried to send TRU");
//                     }
//                 }


//                 if (nextTx.ReceiverId in nextTruBalances)
//                 {
//                     nextTruBalances[nextTx.ReceiverId] += nextTx.TruAmount;
//                 }
//                 else
//                 {
//                     nextTruBalances[nextTx.ReceiverId] = nextTx.TruAmount;
//                 }
//             }
//             var nextBlockId = LocalChain.CrawledTo + 1;
//             if (nextBlockId in LocalChain.Chain && nextBlockId != LocalChain.EndBlockId)
//             {
//                 LocalChain.CrawledTo++;
//                 nextBlock =  LocalChain.Chain[ LocalChain.CrawledTo];
//             }
//             else
//             {
//                 nextBlock = null;
//             }
//         }

     
//         LocalChain._balances = nextTruBalances;
//         return nextTruBalances;
     
//     }


//     public static GetUpToDateBalances():{[id:string]: number}{
//         var tipBalances = {... LocalChain.CalculateBalances()};

//         var nextBlock: ITruBlock =  LocalChain.EndBlock;
       
//         for (var nextTx of nextBlock.Transactions)
//         {
//             if (nextTx.SenderId in tipBalances)
//             {
//                 tipBalances[nextTx.SenderId] -= nextTx.TruAmount;
//             }
//             else
//             {
//                 if (nextBlock.ID != 0)
//                 {
//                     ///Clear localChain..
//                     LocalChain.SetChain({});
//                     throw new Error("Unkown Sender tried to send TRU");
//                 }
//             }

//             if (nextTx.ReceiverId in tipBalances)
//             {
//                 tipBalances[nextTx.ReceiverId] += nextTx.TruAmount;
//             }
//             else
//             {
//                 tipBalances[nextTx.ReceiverId] = nextTx.TruAmount;
//             }
//         }
//         return tipBalances;
//     }

//     public static BalanceAt(walletId: string):  number {
//         var tipBalances = LocalChain.GetUpToDateBalances();
//         if(walletId in tipBalances){
//             return tipBalances[walletId];
//         }
//         else if(walletId in LocalChain._balances){
//             return LocalChain._balances[walletId];
//         }else{
//             return 0;
//         }
//     }
 
//     public static GetTxAt(blockNumb: number, txId: string): ITruTx{
//         var block: ITruBlock =  LocalChain.Chain[ blockNumb ];
//         return block.Transactions.find(tx => tx.ID === txId);
//     }
     
// }
 
 