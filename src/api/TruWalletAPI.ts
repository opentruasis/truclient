import axios from 'axios';
import { LUIX } from 'luix';
import AppLoading from '../components/AppLoading';
import TruWallet from '../types/ITruWallet';
import BaseURL from './BaseURL';
 
const TruWalletAPI = {

Wallets: async  ():  Promise<any> => {
 
  try{
     var results = await axios.get(BaseURL()+'/api/TruWallet');
     return results;
  }catch(e){
    return null;
  }
},

WalletById: async  (id: string) =>{
    try{
 
        var results = await axios.get(BaseURL() + '/api/TruWallet/'+ id);
        return results.data;
    }catch(e){
        return null;
    }
},

NewWallet: async(newWallet: TruWallet): Promise<TruWallet> =>{
    LUIX.postData("showLoader", true);
    try{
        var copiedWallet: TruWallet = {
          FullName: newWallet.FullName,
          Handle: newWallet.Handle,
          Keys: {PubKey: newWallet.Keys.PubKey, PrivKey: null},
          PushNotifToken: newWallet.PushNotifToken,
          ProfilePicBase64: newWallet.ProfilePicBase64,
          Balance: newWallet.Balance
        };

        delete copiedWallet.Keys["PrivKey"];
        console.log("Base URL: ");
        console.log(BaseURL());
        var results = await axios.post( BaseURL() + '/api/TruWallet', copiedWallet);
        console.log("REsults..");
        console.log(results);
        LUIX.postData("showLoader", false);
        return  results.data as TruWallet;
    }catch(e){
        LUIX.postData("showLoader", true);
         return null;
    }

  },
  GetWalletByPubKey: async(pubKey: string): Promise<TruWallet> =>{

    try{
      var pubKeyPayload: any= {
        PubKey: pubKey
      };
 
      var results = await axios.post( BaseURL() + '/api/TruWallet/getWalletByPubKey', pubKeyPayload);
 
      return  results.data as TruWallet;
  }catch(e){

      return null;
  }


  },

  UpdateWallet: async(wallet: TruWallet): Promise<TruWallet> =>{

    if(wallet.ID){
      var copiedWallet: TruWallet = {
        ID: wallet.ID,
        FullName: wallet.FullName,
        Handle: wallet.Handle,
        Keys: {PubKey: wallet.Keys.PubKey, PrivKey: null},
        PushNotifToken: wallet.PushNotifToken,
        ProfilePicBase64: wallet.ProfilePicBase64,
        Balance: wallet.Balance
      };

      delete copiedWallet.Keys["PrivKey"];
 
      var results = await axios.put(BaseURL() + '/api/TruWallet', copiedWallet);
 
      return  results.data as TruWallet;
    }else{
     return await TruWalletAPI.NewWallet(wallet);
    }
  },

  SearchWallet: async(searchString: string): Promise<TruWallet[]> =>{
 
    var results = await axios.post( BaseURL() + '/api/TruWallet/search', { searchString} );

    return  results.data as TruWallet[];
  
  },
 

};



export default TruWalletAPI;
