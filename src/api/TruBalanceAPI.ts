import axios from 'axios';
 import BaseURL from './BaseURL';
 
const TruBalanceAPI = {

Balances: async  ():  Promise<any> => {
  try{
    
     var results = await axios.get(BaseURL() + '/api/TruBalance');
     return results;
  }catch(e){
     
     
    return null;
  }
},

MyBalance: async  (myId: string) =>{
  try{
 
    var results = await axios.get(BaseURL() + '/api/TruBalance/'+ myId);
    var n = JSON.parse(results);

    return results;
  }catch(e){
    return 0;
  }
  }
};



export default TruBalanceAPI;
