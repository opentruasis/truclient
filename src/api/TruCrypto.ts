import SignedMsg from "../types/ISignedMsg";
import TruWallet from "../types/ITruWallet";
 import WalletKeys from "../types/IWalletKeys";
import TruWalletAPI from "./TruWalletAPI";
var forge = require('node-forge');
 
function _arrayBufferToBase64( buffer: any ) {
    var binary = '';
    var bytes = new Uint8Array( buffer );
   
    var len = bytes.byteLength;
 
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode( bytes[ i ] );
  
    }

    return window.btoa( binary );
}


function _base64ToArrayBuffer( base64: any ) {
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

function buf2hex(buffer:any) { // buffer is an ArrayBuffer
  // create a byte array (Uint8Array) that we can use to read the array buffer
  const byteArray = new Uint8Array(buffer);
  
  // for each element, we want to get its two-digit hexadecimal representation
  const hexParts = [];
  for(let i = 0; i < byteArray.length; i++) {
    // convert value to hexadecimal
    const hex = byteArray[i].toString(16);
    
    // pad with zeros to length 2
    const paddedHex = ('00' + hex).slice(-2);
    
    // push to array
    hexParts.push(paddedHex);
  }
  
  // join all the hex values of the elements into a single string
  return hexParts.join('');
}

function forgeBuf2Hex(buffer: any){
  return forge.util.createBuffer(buffer).toHex();
}


function base64ToHex(str:any) {
    const raw = atob(str);
    let result = '';
    for (let i = 0; i < raw.length; i++) {
      const hex = raw.charCodeAt(i).toString(16);
      result += (hex.length === 2 ? hex : '0' + hex);
    }
    return result.toUpperCase();
  }

  
  function concatArrays(a:any, b:any): any { // a, b TypedArray of same type
    var c = new (a.constructor)(a.length + b.length);
    c.set(a, 0);
    c.set(b, a.length);
    return c;
}


function blockCopy(src: any, srcOffset: number, len:number ): any{
    return src.slice(srcOffset, srcOffset+len);
}

interface ForgeKeys{
  publicKey: any,
  privateKey: any
}

const TruCrypto = {


  SimpletTestSig: async () : Promise<void> =>{
     

   
    forge.options.usePureJavaScript = true;

    var rsa = forge.pki.rsa;

     
    var thisWalletKeys = {
      "PrivKey": "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAFBNVTXi72f4FHVELc2U+vn4Ad45O1iKlJiHfm9jC2V/5amQaCBg/JNB7a6EtCcQFspazlDWiHU5Cx9cJdx7tmuONh9ocbyNnpw6efdg01MuFU04WtBwg+ZQ+Yci6pkPcUUYADyniexHOX2oZNISzXEyNG3kReR2j4Pd+03kwyeidhqc4jlhiO1lepIy7/4xc9BNTDCO2nVtr7yRADNm6QHB/KRlyV5+5+CHrs9dURZfT/HIB5H0DTH6H3b0mXkdMe5jALTvVFs6sHWFKE8AkxuABuOmhzzsXmeN/smnlNEnPrP8ryyQO2/ULy7wmAI1KuXSagBOgNL34t2oZueYFUdPEPYjISuB/+nEajX5EuYehCEiWFtPhmfXCkGutqGvvSeR/Q3DN6dLVwExstkfnYVJM+EqCoDVs9cdL/kJS+rPbNKPmUZ17d2NK1Hq/EPi4jzY61s77Lu+ETRdUcA0BdJ1PXhaUKZiOJIAscvcJq33cLHcqcUxqCnNt0AFh54mWUHM3fjKRUjChWvXci/arPLSsFWIoual4SmvxLBAynteXVKqAKFbpZ5l/Jv8dzENbpPoKB0Z1r/lRlFSWwE9t75",
      "PubKey": "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAE="
   };
 
   var forgeKeys = TruCrypto.DeserializePrivateKey(thisWalletKeys.PrivKey);

    
   var md = forge.md.sha1.create();
   
   md.update("asdf", 'utf8');
 
  
   var signature =  forgeKeys.privateKey.sign(md);

   
   

},
  TestRSACompare: async () : Promise<void> =>{
     

   
    forge.options.usePureJavaScript = true;

    var rsa = forge.pki.rsa;

     

   var keysGenedandSerialized = {
      "PrivKey": "2m1KGMvPHbnBmnWPHGJ9PeUdc8hEeBWbsOgs4GDHuXNpPkySObf2wFsgbVSNQTyYY4trDuk819P6XW2sPRD3JEw5CRMywVXdI1vn37d1PaZrar3YeLfbU+P8yEMGUKTpNZwHJWSzixIIvx8ZC84vMHVFrRwgR44myn2OLy2+E/0BAAEKuu62bCC3GZ6Aq4DFyLLVXRGG1BJFAe69kJJPzYFSQ3CqnlFao4ZsKa3coRg1Wbg0v68ZMu0Jv0Cf4VjiQRia6SUzjPnum0xWGIFizClcOQ7bV8Nt1WDtfnA9Xft1zTtKrxOtc26ale6YkdYc2NZFC8q879UPIqWe4omRiXAcIfA9xdam8nZXFOkR1+VreZOMJD8w2nTBXn0r7VmxNoORLarKtxEJy+rOAHMQDt+R1aCS0WIwtnkNdXwy+f4Nou3owTNmGCVyeQY7N2mzQMy/Vbi1id8pegX9weHFmp2iMPS1xP8doMKRzjTCAbKg/5DRIzw2dRNkUNTQVrWQP8NRvgkPMwGyb9vohYkAz9CZRhaoIXhChfLkDH2v7bAZoC86AY0DDK5EKMO1ZU9BhKY2ZyNcD6Da5py333vvdoBQ7cWepGFexwy2rQ/CMMC9mBosUjPw58VzkXqvD16TtJttLZjXtjQVtp3NkwXWIzCHTRIE9K7YIKrf5pEtzeK4AMFEwWIS+rGuBnFKT/uCNYdh3uvgCR6xfTGfnC/MkCyjlEDcWHJpVVnhABv67mv+6LRkB0rHtRG3BOwM7b59AoL2",
      "PubKey": "2m1KGMvPHbnBmnWPHGJ9PeUdc8hEeBWbsOgs4GDHuXNpPkySObf2wFsgbVSNQTyYY4trDuk819P6XW2sPRD3JEw5CRMywVXdI1vn37d1PaZrar3YeLfbU+P8yEMGUKTpNZwHJWSzixIIvx8ZC84vMHVFrRwgR44myn2OLy2+E/0BAAE="
   };

    var forgeKeys =  TruCrypto.DeserializePrivateKey(keysGenedandSerialized.PrivKey);
     
     
    
     
    
    var privateKey = TruCrypto.SerializePrivateKey(forgeKeys.privateKey);
    var publicKey = TruCrypto.SerializePublicKey(forgeKeys.publicKey);

    if(keysGenedandSerialized.PubKey !== publicKey){
      alert("Bad Serial and Deserial");
    }

    if(privateKey !== keysGenedandSerialized.PrivKey){
      alert("Bad Serial and Deserial");
    }

     
    

    var encoded = forge.util.encodeUtf8("asdfasdf");

    var bytes = forge.util.createBuffer("asdfasdf", 'utf8');

        // encrypt data with a public key (defaults to RSAES PKCS#1 v1.5)
    var encrypted = forgeKeys.publicKey.encrypt(encoded);
     
     
     
    
    
    // decrypt data with a private key (defaults to RSAES PKCS#1 v1.5)
    var decrypted =  forgeKeys.privateKey.decrypt(encrypted);
     
     
    
    

  },

  TestSign: async () : Promise<void> =>{
     
    //"RSA-PKCS1-KeyEx"
    var bufTest =   new Uint8Array(5);
     
     
    
    bufTest[0] = 1;
    bufTest[1] = 2;

     

    var bufTestHex= base64ToHex( _arrayBufferToBase64(bufTest));

     
    
    

    var msg =  {msg: '{"TruAmount":"222.000000","SenderId":"4c141955-b201-41b9-bb4e-a4b7d1ace5e5","ReceiverId":"100703b9-ef4e-43ea-80bb-0b644da8a23f","ReceiverPubKey":"4X+Jp5tZAWnL2lC4c6z6N0oeNkU4CLol+QpFBnBArHdSkOZCOd8Bx5NkiVxUdI4Vzc0a48RbAAltzqwXQgPvB5ecvox7svvbkIvipqv4xMrrknJWU1XgaW9ue1rZkyvVLt4/RXOlUFW9lEjzwe09sxYFlRN6vMzeJCcCjLfrnasBAAE="}', stringifyMsg: function(){return this.msg;} };

    var thisWalletKeys = {
      "PrivKey": "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAFBNVTXi72f4FHVELc2U+vn4Ad45O1iKlJiHfm9jC2V/5amQaCBg/JNB7a6EtCcQFspazlDWiHU5Cx9cJdx7tmuONh9ocbyNnpw6efdg01MuFU04WtBwg+ZQ+Yci6pkPcUUYADyniexHOX2oZNISzXEyNG3kReR2j4Pd+03kwyeidhqc4jlhiO1lepIy7/4xc9BNTDCO2nVtr7yRADNm6QHB/KRlyV5+5+CHrs9dURZfT/HIB5H0DTH6H3b0mXkdMe5jALTvVFs6sHWFKE8AkxuABuOmhzzsXmeN/smnlNEnPrP8ryyQO2/ULy7wmAI1KuXSagBOgNL34t2oZueYFUdPEPYjISuB/+nEajX5EuYehCEiWFtPhmfXCkGutqGvvSeR/Q3DN6dLVwExstkfnYVJM+EqCoDVs9cdL/kJS+rPbNKPmUZ17d2NK1Hq/EPi4jzY61s77Lu+ETRdUcA0BdJ1PXhaUKZiOJIAscvcJq33cLHcqcUxqCnNt0AFh54mWUHM3fjKRUjChWvXci/arPLSsFWIoual4SmvxLBAynteXVKqAKFbpZ5l/Jv8dzENbpPoKB0Z1r/lRlFSWwE9t75",
      "PubKey": "nNtKGPFrLdf4KGPdmdE/IZwTEHAP2i/U+C0jWn6Fdy7r4Zivs4TexlXl4GMGFuEljfiaKOfMDGv+jKJMwNT9adhPPgi26Ys9MpLnacNR3UTzMBeVR04HFXxrntCXLuOd4FxuFUY0UpqtpNJNNbiBKAJlb5cQ36s8qYXT5gJHTYsBAAE="
  };
 
    
    const signedMsg = TruCrypto.SignMsg(msg, thisWalletKeys.PrivKey);

     
     
    
     
     

    
    var isValid =  TruCrypto.VerifyMsgSig(signedMsg, thisWalletKeys.PubKey);
     
    
     
    
  },

  TestBase64Encod: () : void => {
 
    const buffer = new ArrayBuffer(8);
     
    const view = new Int32Array(buffer);
     

    var arr = new Uint8Array(256);
     
    
    for(var i:number=0; i<256; i++){
      arr[i] = i;
       
    }


    var encodedString = _arrayBufferToBase64(arr);
     
 
  },
  Test2Base64Encod: () : void => {
 
   //  "ynxK5Qi06/79sr4yBjckfI4UPXsradasJ0jfWZK/cnE4XTqKT8OqmgJLzA8vJpchY68kJcbHBwlHZGrsMBloslNiSyuqxKVnw1ep+gxXn8oOOEFr56N59iqMGjZTfAEFi30RmL39WRT/IbaMWpRcLEogA/SzhocqzmSoumeQliEBAAE="
    var arr = new Uint8Array(131);
     
    arr[0] = 202
    arr[1] = 124
    arr[2] = 74
    arr[3] = 229
    arr[4] = 8
    arr[5] = 180
    arr[6] = 235
    arr[7] = 254
    arr[8] =  253
    arr[9] = 178
    arr[10] = 190
    arr[11] = 50
    arr[12] = 6
    arr[13] = 55
    arr[14] = 36;
   
       
    var encodedString = _arrayBufferToBase64(arr);
     
    ///good: ynxK5Qi06/79sr4yBjckAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=


  },

  Test3Base64ToArry: (): void =>{

    var pubKey = "ynxK5Qi06/79sr4yBjckfI4UPXsradasJ0jfWZK/cnE4XTqKT8OqmgJLzA8vJpchY68kJcbHBwlHZGrsMBloslNiSyuqxKVnw1ep+gxXn8oOOEFr56N59iqMGjZTfAEFi30RmL39WRT/IbaMWpRcLEogA/SzhocqzmSoumeQliEBAAE=";

    var deserialized = TruCrypto.DeserializePublicKey(pubKey);
     
     

  },

  Test4Base64ToArry: (): void =>{

    var privKey = "rKr9sStpNfi4fx6qF1c9USzbAit5NaflVm1tqlQ9efytgAN2DbVfJSz32W+yRYSq6oYqx6KHRJl7hbBlWWKa+Gev3hyerrVgzSL1+eKjK6rZr6nzmmQrvC7A33X82Mkgr8Egl/wXTAZue5F/7kUAvvCjuapkKeFbQ8HNEpBxy6EBAAE+9ealqLend3cG206YTjZMmDePtfxvpTYJ6sL3Pdpg1dMJ/7kXTJBSBPCnN4Rgtvx/ocaNnuUGDZUt7WE3U64/1fgb+J5cmktMrMKsTShrnxQfkI4mXSWXaElTKPIXNHYHrMj/dmYV8SNmlkqcnAurHyteKImgiqanKoDPeb79pcI7lwI73CZH5UIZBajBVRcsDjR6OaJHLPu/353rrOsRH1QtDJzfuxfc+78YJwXsaSvP+y+4f5CRoAHYMbukMg/jk9D/xbKjlLZMtBslnelkxTPMCQvK0SGkcWRC7Ek/DPAn3nhSE9X49LkFHe0pgE5TR1G5t32/fXFNZVQ+SRdPpiJCL+7zhurH9Zlhdc6n8YIFRG2nDl7C4zJjGriS1x12JpHB0AyuAuNJkVIfTrm/23Gx8EdnFUj6C5nYMB9pkcxsOf+WoWmpKrvpyLzYhQ78yRNNSOdtRdAF/y7SOtVFFPBrOR9ZLApOIWiEQkXWOAU8eh/dwDnjP4RrMeolE6d1vpe4AaiqZRxFPxme7sIWg7+zK7QMXjBD2XKNDdnfP9U0CMrGNDdE9P3FtJPc/Xf3yW/SotIWpuW1Xkkdd2gc";

    var deserialized = TruCrypto.DeserializePrivateKey(privKey);
    
     
     
    
    

  },

  TestBlockCopy: (): void =>{

    var arr = new Uint8Array(256);
     
    
    for(var i:number=0; i<256; i++){
      arr[i] = i;
       
    }


    let offset = 0;
    //let len = 129;
    let len = 128;
    let nbytes = blockCopy(arr, offset, len);
     
    

  },

  TestToHex: ():void =>{

    var arr = new Uint8Array(256);
     
    
    for(var i:number=0; i<256; i++){
      arr[i] = i;
       
    }

    let hex = buf2hex(arr);

     

  },
  TestUintArrToBase16: (): void =>{
 
    var arr = new Uint8Array(256);
      
    for(var i:number=0; i<256; i++){
      arr[i] = i;
       
    }
 
    let offset = 0;
    //let len = 129;
    let len = 128;
    let nbytes = blockCopy(arr, offset, len);
     
    
    var bufferToBase64 = _arrayBufferToBase64(nbytes)
     
    
    let nhex = base64ToHex(bufferToBase64);

     
    

    var BigInteger = forge.jsbn.BigInteger;

    let n = new BigInteger(nhex, 16);

     
    
  },

  ImportKey: (privateBase64Enc: string): WalletKeys =>{
 
    let keyPair = TruCrypto.DeserializePrivateKey(privateBase64Enc);

    var walletKeys: WalletKeys = {
        PrivKey: privateBase64Enc,
        PubKey: TruCrypto.SerializePublicKey(keyPair.publicKey)
    };
        
    return walletKeys;
  },

  DeserializePublicKey: (publicBase64Enc: string) : any =>{
    forge.options.usePureJavaScript = true;
    var rsa = forge.pki.rsa;
 
    var arry = _base64ToArrayBuffer( publicBase64Enc );
    var BigInteger = forge.jsbn.BigInteger;
 
 
    let offset = 0;
    //let len = 129;
    let len = 128;
    let nbytes = blockCopy(arry, offset, len);
 
    let nhex = buf2hex(  nbytes );
    let n = new BigInteger(nhex, 16);

    offset += len;

    len = 3;
    let expbytes = blockCopy(arry, offset, len);
    let exphex = buf2hex(expbytes);
    offset += len;
    let e = new BigInteger(exphex, 16);
  
 
    return  rsa.setPublicKey(n , e);

  },
  DeserializePrivateKey: (privateBase64Enc: string) : ForgeKeys =>{
    forge.options.usePureJavaScript = true;
    var rsa = forge.pki.rsa;
     
      
 
    var arry = _base64ToArrayBuffer( privateBase64Enc );
    var BigInteger = forge.jsbn.BigInteger;
 
    let offset = 0;
    //let len = 129;
    let len = 128;
    let nbytes = blockCopy(arry, offset, len);
 
     
 
    let nhex = buf2hex(nbytes);
    let nhexforg = forgeBuf2Hex(nbytes);


    let nhexforg1 = forge.util.bytesToHex(nbytes);

      

    let n = new BigInteger(nhex, 16);
    let ncomp = new BigInteger(nbytes, 16);
    let ncomp1 = new BigInteger(nhexforg1, 16);

     
    
    offset += len;

    len = 3;
    let expbytes = blockCopy(arry, offset, len);
    let exphex = forgeBuf2Hex(expbytes);// buf2hex(expbytes);
    offset += len;
    let e = new BigInteger(exphex, 16);

    len = 128;

    let dbytes = blockCopy(arry, offset, len);
    let dhex = buf2hex(dbytes);
    offset += len;
    let d = new BigInteger(dhex, 16);
    //len = 65;
    len = 64;
    let pbytes = blockCopy(arry, offset, len);
    let phex = buf2hex(pbytes);
    offset += len;
    let p = new BigInteger(phex, 16);

    len = 64;
    let qbytes = blockCopy(arry, offset, len);
    let qhex = buf2hex(qbytes);
    offset += len;
    let q = new BigInteger(qhex, 16);


    len = 64;
    let dpbytes = blockCopy(arry, offset, len);
    let dphex = buf2hex(dpbytes);
    offset += len;
    let dP = new BigInteger(dphex, 16);

    //len = 65;
    len = 64;
    let dqbytes = blockCopy(arry, offset, len);
    let dqhex = buf2hex( dqbytes );
    offset += len;
    let dQ = new BigInteger(dqhex, 16);

    //len = 65;
    len = 64;
    let Invqbytes = blockCopy(arry, offset, len);
    let Invqhex = buf2hex( Invqbytes );
    let qInv = new BigInteger(Invqhex, 16);
 
    
    var pubKey = rsa.setPublicKey(n , e);
                             //    n, e, d, p, q, dP, dQ, qInv
    var pvkey = rsa.setPrivateKey(n, e, d, p, q, dP, dQ, qInv);
  
    return {privateKey: pvkey, publicKey: pubKey};

  },
 
  SerializePublicKey:  (rsaPubKey: any): string => {

    var mod = rsaPubKey.n.toByteArray();
 
    if(mod.length > 128){
      mod = blockCopy(mod, mod.length-128, 128);
    }
 
    
    var exp = rsaPubKey.e.toByteArray();
    if(exp.length > 3){
      exp = blockCopy(exp, exp.length-3, 3);
    }
 
    var pubkeyEncodedBytes =  concatArrays(new Uint8Array(mod), exp);
    var pubKeyb64encoded = _arrayBufferToBase64(pubkeyEncodedBytes);
    return pubKeyb64encoded;

  },

  SerializePrivateKey: (privKey: any): string =>{
 
    var mod = privKey.n.toByteArray();
    if(mod.length > 128){
      mod = blockCopy(mod, mod.length-128, 128);
    }
     
    
    var exp = privKey.e.toByteArray();
    if(exp.length > 3){
      exp = blockCopy(exp, exp.length-3, 3);
    }
     
    
    var d = privKey.d.toByteArray();
    if(d.length > 128){
      d = blockCopy(d, d.length-128, 128);
    }
     
    

    var p = privKey.p.toByteArray();
    if(p.length > 64){
      p = blockCopy(p, p.length-64, 64);
    }
     
    
    var q = privKey.q.toByteArray();
    if(q.length > 64){
      q = blockCopy(q, q.length-64, 64);
    }
     
    

    var dp = privKey.dP.toByteArray();
    if(dp.length > 64){
      dp = blockCopy(dp, dp.length-64, 64);
    }
     
    
    var dq = privKey.dQ.toByteArray();
    if(dq.length > 64){
      dq = blockCopy(dq, dq.length-64, 64);
    }
     
    
    var InvQ = privKey.qInv.toByteArray();
    if(InvQ.length > 64){
      InvQ = blockCopy(InvQ, InvQ.length-64, 64);
    }
     
    
    var privateKeyEncodedBytes =  concatArrays(concatArrays(concatArrays(concatArrays(concatArrays(concatArrays(concatArrays(new Uint8Array(mod), exp), d), p), q), dp),dq),InvQ);
    var privKeyb64encoded = _arrayBufferToBase64(privateKeyEncodedBytes);

    return privKeyb64encoded;
  },

 
  CreateNewWalletKeys:  async (): Promise<WalletKeys> => {
     //RSA
    forge.options.usePureJavaScript = true;

    var rsa = forge.pki.rsa;
   
    return new Promise((resolve, reject)=>{
    
      var keypair = rsa.generateKeyPair({bits: 1024, e: 0x10001});
      //  rsa.generateKeyPair({bits: 2048, workers: -1}, (err:any, keypair:any) => {
 
            try{
                let privateKey: any  = keypair.privateKey;
                let publicKey:any  = keypair.publicKey;
         
                var walletKeys: WalletKeys = {
                    PrivKey: TruCrypto.SerializePrivateKey(privateKey),
                    PubKey: TruCrypto.SerializePublicKey(publicKey)
                };
    
                resolve( walletKeys );
                
            }catch(e){
  
                reject(e);
            }
        //},
        //  (errrrrr:any)=>{
        //    alert("rsa Erroor.");
        //    alert(errrrrr)});
    });
 },

 SignMsg:  (msg: SignedMsg, PrivKeyBase64Enc: string): SignedMsg =>{

    const forgeKeys = TruCrypto.DeserializePrivateKey(PrivKeyBase64Enc);
 
    var privKeyEncoded =  TruCrypto.SerializePrivateKey(forgeKeys.privateKey);

    var pubKeyEncoded =   TruCrypto.SerializePublicKey(forgeKeys.publicKey);


    console.log("Private KEY:: ");
    console.log(PrivKeyBase64Enc);
    console.log("Pub Key------");
    console.log(pubKeyEncoded);

    console.log(privKeyEncoded === PrivKeyBase64Enc);

    console.log("-----------------")
    var md = forge.md.sha1.create();
     
    var stringifyedMsg = msg.stringifyMsg();
    
    console.log('Stringified msg: ');
    console.log(stringifyedMsg);
 
    md.update(msg.stringifyMsg(), 'utf8');
     
    var hashedMsg = md.digest().toHex();

    console.log("HAshed Msg: ");
    console.log(hashedMsg);
  
    //var signature =  rsa.encrypt(hashedMsg, forgeKeys.privateKey, "asdf");

    var signature = forgeKeys.privateKey.sign(md);
 
    var buf = forge.util.createBuffer(signature, 'utf8');

    var sigByteArr = buf.getBytes();
     
    var hexSig = forge.util.bytesToHex(signature);
    console.log("hex Sig: ");
    console.log(hexSig);

    msg.Signature = hexSig;
    msg.Hash =  hashedMsg;

    return msg;
 
 },

 VerifyMsgSig: (msg: SignedMsg,  pubKeyBase64Enc: string): boolean =>{
 
  const publicKey =  TruCrypto.DeserializePublicKey(pubKeyBase64Enc);

  var md = forge.md.sha1.create();
   
   
   
  var stringifiedMsg = msg.stringifyMsg();
   

  
  md.update(stringifiedMsg, 'utf8');

   
    var hashedMsg = md.digest().toHex();
     
    hashedMsg = md.digest().toHex();
     
    hashedMsg = md.digest().toHex();
     
    hashedMsg = md.digest().toHex();
     
  
  
    var sigString = forge.util.hexToBytes(msg.Signature);
     
    
      
 
   //  var rsa = forge.pki.rsa;

    //var decryptedMsg =  rsa.decrypt(sigString, publicKey, true, false);

    //  
    //  
    //  
    
    //  
 
   //return decryptedMsg.includes(hashedMsg);

    return publicKey.verify(md.digest().bytes(), sigString);

 
 },

 HashBase64String: (data: string): string =>{

  var md = forge.md.sha1.create();
  md.update(data, 'utf8');
  return md.digest().toHex();
  
 }
  
 };
 
 
 
 export default TruCrypto;