import {LUIX} from 'luix';
 
const ChainLoader = {
 
SetChainLoader: (isLoader: boolean):boolean =>{
    try{
        localStorage.setItem("isFullNode", isLoader.toString());
        return isLoader;
    }catch(e){
         
    }
 },

GetChainLoader: ():boolean=>{
    try{
        var isChainLoader: boolean = localStorage.getItem("isFullNode") == 'true';
        return isChainLoader;
    }catch(e){
         return false;
    }
}
 
};

 
export default ChainLoader;
