import axios from 'axios';
 
import AppLoading from '../components/AppLoading';
import {LUIX} from 'luix';
import ITruApp from '../types/ITruApp';
import TruWallet from '../types/ITruWallet';
import CreateAppBtn from '../units/CreateAppBtn';
import BaseURL from './BaseURL';
import TruCrypto from './TruCrypto';
 
function _base64ToArrayBuffer( base64: any ) {
    try{
        var binary_string =  window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array( len );
        for (var i = 0; i < len; i++)        {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }catch(e){
        
        
    }
  
}


const TruAppAPI = {

AllApps: async  ():  Promise<any> => {
 
  try{
     var results = await axios.get(BaseURL()+'/api/TruApp');
     
     
     return results.data;
  }catch(e){
    return null;
  }
},

OpenApp: async  (app: ITruApp) => {
    try{
 
        var results = await axios.get(BaseURL() + '/api/TruApp/' + app.ID);
        console.log("OPEN DAP??? Results");
        console.log(results);
        var appProgram = results.data;
      
       
        try{
            
            var programBuffer = _base64ToArrayBuffer(appProgram);
 
            if( app.Hash == TruCrypto.HashBase64String(appProgram).toUpperCase()){
         
                var stringCode = String.fromCharCode.apply(null, new Uint8Array(programBuffer));
      
                var win = window.open(null, null, "scrollbars=yes,status=no,location=yes,toolbar=yes,menubar=yes,resizable=yes,width=500,height=700,top="+100+",left="+100);

                 win.document.write(stringCode);
                 win.document.close();
            }
        }catch(e){
            console.error(e);
        }
        return results;
    }catch(e){
        return null;
    }
},

NewApp: async(appData: ITruApp):Promise<ITruApp> =>{
    try{
        var newAppFormData = new FormData();
        newAppFormData.set("Program", appData.Program);
        newAppFormData.set("Name", appData.Name);
        newAppFormData.set("Description", appData.Description);
        newAppFormData.set("GitRepoURL", appData.GitRepoURL);
        newAppFormData.set("Hash", appData.Hash);
        newAppFormData.set("CreatedBy", appData.CreatedBy);

     
        for(var i=0;i<appData.ShowcaseFiles.length; i++){
            newAppFormData.append('ShowcaseFiles', appData.ShowcaseFiles[i]);
        }
        
 
         var results = await axios({
             url: BaseURL() + '/api/TruApp',
             method: 'POST',
             headers: {'Content-Type': 'multipart/form-data'},
             data: newAppFormData,
             onUploadProgress: (progressEvent: any) => {

                const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
                console.log("Upload Progress.........");
                console.log()
                if (totalLength !== null) {
                    console.log("PERCENT:");
                   
                    var percentComplete =  Math.round( (progressEvent.loaded * 100) / totalLength );
                    console.log( percentComplete);
                    LUIX.postData("newAppUploadProgress",  percentComplete);
                    console.log("lllllllllllllllllllllllllll")
                }
            }
        });

        
        console.log("NEW DAP RESULTS>>>");
        console.log(results);
        
         return results;

    }catch(e){

        return null;
    }
  },
 
  UpdateApp: async(appData: ITruApp): Promise<ITruApp> =>{
    try{
         var results = await axios.put( BaseURL() + '/api/TruApp/' + appData.ID, appData);

         
         
    }catch(e){

        return null;
    }
  },

  DeleteApp: async(appId: string): Promise<void> =>{
  
  },
 

};



export default TruAppAPI;
