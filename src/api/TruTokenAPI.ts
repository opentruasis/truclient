import axios from 'axios';
import AppLoading from '../components/AppLoading';
import ITruToken from '../types/ITruToken';
import TruWallet from '../types/ITruWallet';
import BaseURL from './BaseURL';


const TruTokenAPI = {

AllTokens: async  ():  Promise<any> => {
  try{
     var results = await axios.get(BaseURL()+'/api/TruToken');
     
     
     return results.data;
  }catch(e){
    return null;
  }
},
DownloadToken: async (tkn: ITruToken): Promise<any> =>{
  try{

    var a = document.createElement('a');
    a.href = BaseURL()+'/api/TruToken/download/' + tkn.ID;
    a.download = tkn.ID + "." + tkn.FileExt;
    a.click();
    a.remove();
    return true;
 }catch(e){
   return null;
 }

},
WalletTokens: async  (walletId: string):  Promise<any> => {
    try{
       var results = await axios.get(BaseURL()+'/api/TruToken/walletTokens/' + walletId);
       
       
       return results.data;
    }catch(e){
      return null;
    }
  },

TokensForSale: async (): Promise<any> => {
    try{
       var results = await axios.get(BaseURL()+'/api/TruToken/forsale');
       
       
       return results.data;
    }catch(e){
      return null;
    }
 },

TokenById: async  (id: string): Promise<ITruToken> =>{
    try{
        var results = await axios.get(BaseURL() + '/api/TruToken/'+ id);
        var tkn = results.data;
        
        return tkn;
    }catch(e){
        return null;
    }
},

NewToken: async(newTokenData: any): Promise<ITruToken> =>{
    //TODO: Sign msg.
    try{

        var newTokenFormData = new FormData();
        newTokenFormData.set("tknFile", newTokenData.tknFile);
        newTokenFormData.set("Name", newTokenData.Name);
        newTokenFormData.set("CreatedBy", newTokenData.CreatedBy);
       
         var results = await axios({
             url: BaseURL() + '/api/TruToken/',
             method: 'post',
             headers: {'Content-Type': 'multipart/form-data'},
             data: newTokenFormData,
            });
 
          console.log("New Token Results..////");
          console.log(results);
         return results.data as ITruToken;

    }catch(e){

        return null;
    }
  },
 
  SellToken: async(tknData: ITruToken): Promise<ITruToken> =>{
    //TODO: Sign msg.
    try{
        var results = await axios.put(BaseURL()+'/api/TruToken/putforsale', tknData);
        console.log("Sell Token: " + results.data);
        console.log(results.data);
        return results.data;
     }catch(e){
       return null;
     }

  },

  BuyToken: async(tknData: ITruToken): Promise<boolean> =>{
    //TODO: Sign msg.
    try{
        var results = await axios.put(BaseURL()+'/api/TruToken/buytoken', tknData);
        
        return true;
     }catch(e){
       return false;
     }
  }

};



export default TruTokenAPI;
