 
import {LUIX} from "luix";
import TruWallet from "../types/ITruWallet";



 function hasLocalStorage(){
    if (!typeof(Storage)) {
        throw new Error("No Local Storage");
    }
 }

 const LocalSuperNodeURL = {
 
    GetUrls: (): string[] => {
        hasLocalStorage();
        var localSuperList =  JSON.parse(localStorage.getItem('superNodeUrls'));   
        localSuperList = localSuperList ? localSuperList : [];
       
        if(!localSuperList.includes("https://localhost:44380")){
            localSuperList.push("https://localhost:44380");
        }

        // if(!localSuperList.includes("http://18.144.168.127:5001")){
        //     localSuperList.push("http://18.144.168.127:5001");
        // }

        return localSuperList;
    },
    AddUrl:  (newUrl: string) : void => {
        hasLocalStorage();
        try{
            var list = LocalSuperNodeURL.GetUrls();
            if(list){
                if(!list.find((uri)=>uri===newUrl)){
                    list.push(newUrl);
                }else{
                    return;
                }
            }else{
                list = [newUrl];
            }
           
            localStorage.setItem("superNodeUrls", JSON.stringify(list));
             
        }catch(e){
             
        }
    },
    SaveList: (listOfUrls: string[]) : void => {
        hasLocalStorage();
        try{
          localStorage.setItem("superNodeUrls", JSON.stringify(listOfUrls));
          LUIX.postData('superNodeUrls', listOfUrls);
        }catch(e){
             
        }
    },
    DeleteUrl: (url: string):any =>{
        hasLocalStorage();
        try{
            var list = LocalSuperNodeURL.GetUrls();
   
            let index = list.findIndex((uri)=> uri===url);
 
            list.splice(index, 1);
  
            LocalSuperNodeURL.SaveList(list);
        }catch(e){
             
            
            return null;
        }
    },
    ClearUrls: (): void =>{
        hasLocalStorage();
        try{
            localStorage.removeItem("superNodeUrls");
        }catch(e){
             
            
            return null;
        }
    }
   
 };
 
 
 
 export default LocalSuperNodeURL;
 