 
 import TruWallet from "../types/ITruWallet";


const LocalWalletAPI = {

GetWallet: (): any => {
 
    if (typeof(Storage) !== "undefined") {
        return JSON.parse(localStorage.getItem('wallet'));
        } else {
        throw new Error("No Local Storage");
    }
 
},

SetWallet:  (wallet: TruWallet) : void => {
    try{
        if (typeof(Storage) !== "undefined") {
          localStorage.setItem("wallet", JSON.stringify(wallet));
        }else{
            alert("DANG!!! No Local Storage");
        }
    }catch(e){
         
    }
  },

  DeleteKey: (key: string):any =>{

    try{
        localStorage.removeItem("wallet");
    }catch(e){
         
        
        return null;
    }
  }
 
};



export default LocalWalletAPI;
