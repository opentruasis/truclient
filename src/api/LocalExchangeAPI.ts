import ITruOrder from "../types/ITruOrder";

const LocalExchangeAPI = {

    GetOrders: (): ITruOrder[] =>  {
     
        if (typeof(Storage) !== "undefined") {
            return JSON.parse(localStorage.getItem('myExchangeOrders'));
            } else {
            throw new Error("No Local Storage");
        }
     
    },
    
    SaveOrder:  (order: ITruOrder) : void => {
        if(order){
            try{
                if (typeof(Storage) !== "undefined") {
                    var allOrders = LocalExchangeAPI.GetOrders() || [];
                    
                    var isIn: boolean = false;
                    for(var odr in allOrders){
                        var o = allOrders[odr];
                        if(o.ID == order.ID){
                        isIn = true;
                        allOrders[odr] = order;
                        break;
                        }
                    } 
                    if(!isIn){
                        allOrders.push(order);
                    }
                    
                    localStorage.setItem("myExchangeOrders", JSON.stringify(allOrders));

                }else{
                    alert("DANG!!! No Local Storage");
                }
            }catch(e){
                
            }
        }
      },
    
      DeleteOrder: (orderId: number):any =>{
    
        try{
            var allOrders = LocalExchangeAPI.GetOrders() || [];
            allOrders =  allOrders.filter((o: ITruOrder)=>{
               return o.ID != orderId
            });
            localStorage.setItem("myExchangeOrders", JSON.stringify(allOrders));
        }catch(e){
            console.error("Delete Order... failure.");
            console.log(e);
        }
      }
     
};
    
    
export default LocalExchangeAPI;
    